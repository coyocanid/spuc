package App::SPUC::Part::Panel;
use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub can_kudo {
    my( $self, $artist ) = @_;
    my $givers = $self->get__kudo_givers({});
    return $artist ne $self->get_artist && !$givers->{$artist};
}

sub kudo {
    my( $self, $artist ) = @_;
    if( $self->can_kudo($artist) ) {
        my $givers = $self->get__kudo_givers;
        $givers->{$artist->[0]} = $artist;
        $artist->get__kudos->{$self->[0]} = $self;
        $self->set_kudos( scalar(keys %$givers));
    }
}

1;
