package App::SPUC::Part::Comic;
use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';
use File::Path qw(make_path);

my $max = 30;
my $m1 = $max-1;
my $m2 = $max-2;
sub splitext {
    my $txt = shift;
    my( @words ) = split( /\s+/, $txt );
    my $lcount = 0;
    my( $line, @lines ) = ('');
    while( @words ) {
        my $w = shift @words;
        if( 1 + length($line) + length($w) > $max ) {
            if( length( $w ) > $m2 ) {
                # break word apart
                my $space_left = $m2 - length($line);
                if( $space_left > 4 ) {
                    my $first_bit = substr( $w, 0, $space_left );
                    $first_bit .= '-' unless $first_bit =~ /-$/;
                    $w = substr( $w, $space_left );
                    push @lines, "$line $first_bit";
                } else {
                    push @lines, $line;
                }
                while( length( $w ) > $m1 ) {
                    my $first_bit = substr( $w, 0, $m1 );
                    $first_bit .= '-' unless $first_bit =~ /-$/;
                    push @lines, $first_bit;
                    $w = substr( $w, $m1 );
                }
                $line = $w;
            } else {
                push @lines, $line;
                $line = $w;
            }
        } else{
            $line = $line eq '' ? $w : "$line $w";
        }
    }
    if( $line ne '' ) { push @lines, $line; }

    return @lines;
}

sub beep {
    return "BEEP";
}

sub make_comic_image {
    my( $self ) = @_;
    
    if( $self->get_finished && ! $self->get_full_pic ) {

        my $full_pic = $self->store->create_container( 'App::SPUC::Part::Picture' );
        my $path = $self->get__app->get__image_path;
        my $app_path = $self->get__app->get__app_path;
        my $dir = "$path/comics/$self->[0]";
        make_path( $dir );
        my $filename = "$dir/finished.png";
        print STDERR ")$filename(\n";

        my $cmd = 'convert ';
        my( @cmd ) = ('convert');
        my( @panels ) = @{$self->get_panels};
        my $pani = 0;
        while( @panels ) {
            my $cap = shift @panels;
            print STDERR $cap->get_caption."\n";
            my( @caplines ) = splitext( $cap->get_caption );
            for my $cap (@caplines) {
                my $q = $cap =~ /'/;
                $cap =~ s/"/\\"/g;
                $cap =~ s/'/\`/g;
                my $txt = qq~text 0,$pani "$cap"~;
                push @cmd, '(', '-font', 'Courier', '-weight', 800, '-pointsize', 60, '-size', '1200x100', '-gravity',
                             'center', '-draw', $txt, 'xc:white', ')';
                $cmd = "$cmd \\( -font Courier -weight 800 -pointsize 60 -size 1200x100 -gravity center -draw 'text 0,$pani \"$cap\"' xc:white \\)";
            }
            $pani = -20;
            if( @panels ) {
                my $pic = shift @panels;
                my $fn = $pic->get_picture->get__origin_file;
                push @cmd, '(', '-frame 30x30', '-mattecolor black', $fn, '-border 1x1', '-bordercolor white',
                             '-resize 900x', 'xs:white', ')';
                $cmd = "$cmd \\(  -frame 30x30 -mattecolor black $fn -border 1x1 -bordercolor white -background white -flatten -resize 900x xc:white \\)";
            }
        }
        push @cmd, '-bordercolor', 'grey', '-append', $filename;
        $cmd = "$cmd -bordercolor grey -append $filename";
        print STDERR Data::Dumper->Dump(["GO $cmd","SYS @cmd"]);
#        system @cmd;
        `$cmd`;
        $full_pic->set__origin_file( $filename );
        $full_pic->set_path( "$app_path/images/comics/$self->[0]/finished.png" );
        $full_pic->set__original_name( "finished_$self->[0].png" );
        $self->set_full_pic( $full_pic );
    } # if finished
	
} #make_comic_image
1;
