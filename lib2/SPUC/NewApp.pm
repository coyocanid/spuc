package SPUC::NewApp;

use strict;
use warnings;

use Data::ObjectStore;

my $singleton;
sub fetch {
    my $cls = shift;
    unless( $singleton ) {
        my $store = Data::ObjectStore->open_store( '/var/www/data/spuc' );
        my $root  = $store->load_root_container;
        my $app   = $root->get( 'SPUC' );

        $singleton = bless {
            store => $store,
            app   => $app,
        }, $cls;
    }
    return $singleton;
} #fetch

sub store {
    return shift->{app}->store;
}

sub act {
    my( $self, $action, $args ) = @_;
    if( $action =~ /^(comics)$/ ) {
        return $self->$action( $args );
    }
}

sub comics {
    my( $self, $args ) = @_;
    my $start  = $args->{start} || 0;
    my $len    = $args->{length} || 10;
    my $comics = $self->{app}->get_finished_comics;
    if( @$comics > $len ) {
        $comics = [@$comics[0..$len]];
    }
    return 1, $comics;
}

1;
