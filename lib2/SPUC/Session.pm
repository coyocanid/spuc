package SPUC::Session;

use strict;
use warnings;

use Encode qw/ decode encode /;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

sub note {}

sub err {}

#
# May return a new session if a login has just occurred.
#
sub validate {
    my( $self, $sess_id ) = @_;
    my $app  = $self->get_app;
    my $user = $self->get_user;
    unless( $user ) {
        $self->note( "invalid sessions (no user) $sess_id", $user );
        return 0;
    }
    my $t = time;

    #
    # Check if the user hasnt been active in a long time,
    # Check that the session itself isnt too old.
    #
    if( ( $t - $self->get__active_time($t) ) > 3600*24*30 ||
        ( $t - $user->get__login_time($user->get__active_time) ) > 3600*24*30*6 ) {
        $self->err( 'session expired' );
        $self->note( "session expired", $user );
        $self->lock( "SESSIONS" ) ;
        delete $app->get__sessions->{$sess_id};
        return 0;
    }
    
    $user->set__active_time( $t );

    1;
} #validate

sub login {
    my( $self, $params ) = @_;
    my( $un, $pw ) = ( encode( 'UTF-8', $params->{un} ), $params->{pw} );
    my $sess_id;
    
    # check if the username is a handle or email address
    my $app = $self->get_app;
    my $emails = $app->get__emails({});
    my $unames = $app->get__users({});
    
    my $user = $unames->{lc($un)} || 
        $emails->{lc($un)} || $app->get_dummy_user;

    if( $user->_checkpw( $pw ) ) {
        my $found;
        $self->lock( "SESSIONS" );
        my $sessions = $self->{app}->get__sessions({});
        until( $found ) {
            $sess_id = int(rand(2**64));
            $found = ! $sessions->{$sess_id};
        }
        $self->note( "logged in", $user );
        $self->msg( 'logged in' );
        my $sess = $user->get__session;
        delete $sessions->{$sess->get_last_id};
        $sessions->{$sess_id} = $sess;
        $sess->set_last_id( $sess_id );
        $user->set__login_time( time );
    } else {
        $self->err( 'login failed' );
        undef $user;
    }
} #login

#
# A user gets one permanent session object
#
# Fields :
#   ids  - id -> object known to the session
#   user - owner of the session
#   last_id - last session id used
#

# -- the following methods are just for the
# -- RPC infrastructure
sub fetch {
    my( $self, $id ) = @_;
    my $ids = $self->get_ids({});
    return $ids->{$id};
}

sub stow {
    my( $self, $item ) = @_;
    my $id = $self->store->_get_id( $item );
    my $ids = $self->get_ids({});
    $ids->{$id} = $item;
    return $id;
}

1;
