package SPUC::Panel;

use strict;
use warnings;

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

#
# Fields :
#   type - 'caption' or 'picture'
#   artist - artist obj who created this panel
#   caption - caption text ( if type is caption )
#   comic   - SPUC::Comic
#   created - when this was created
#   picture - SPUC::Image
#

1;
