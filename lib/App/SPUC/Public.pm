package App::SPUC::Public;
use strict;
use warnings;

no warnings 'numeric';

use Data::ObjectStore;
use base 'Data::ObjectStore::Container';

use Digest::MD5;
use File::Path qw(make_path);
use File::Copy;
use File::Type;
use Image::Magick;
use MIME::Lite;
use MIME::Base64;

my $singleton;
sub fetch {
    my $cls = shift;
    unless( $singleton ) {
        my $store = Data::ObjectStore->open_store( '/var/www/data/spuc' );
        my $root  = $store->load_root_container;
        $singleton = $root->get_apps->{SPUC}{Public};
        unless( $singleton ) {
            $singleton = $store->create_container( $cls );
            $root->get_apps->{SPUC}{Public} = $singleton;
        }
    }
    return $singleton;
} #fetch

sub act {
    my( $self, $action, $args, $sess, $files ) = @_;
#    print STDERR ")act)$action\n";
    if( $action =~ /^(add_avatar|add_chat|add_comment|activate_reset|artists|autosave_avatar|autosave_panel|bookmark|caption_comic|comics|chats|check_login_and_email|login|logout|logs|kudo|play|rate_comic|recent_chat_length|recent_log_length|logs|reset_password|reset_playing|save_panel|select_autosaved_picture|select_saved_picture|send_reset_request|session_login|signup|start_comic|unbookmark|update_account|use_panel)$/ ) {
        # if the following regex matches, then the
        # action needs a session. Note that the match is !~
        if( $action !~ /^(activate_reset|artists|comics|check_login_and_email|login|logout|send_reset_request|session_login|signup)$/ ) {
            if( $sess ) {
                $self->vol( 'notes', [] );
                my @ret = $self->$action( $args, $sess, $files );
                $self->_write_notes;
                return @ret;
            }
            return 0, 'session expired';
        }
        $self->vol( 'notes', [] );
        my @ret = $self->$action( $args, $sess, $files );
        $self->_write_notes;
        return @ret;
    } else {
        print STDERR "($$) Request for unknown action '$action'\n";
    }
    return 0, 'unknown';
} #act

# --------------------- PUBLIC ACTIONS --------------------------------------

sub activate_reset {
    my( $self, $tok ) = @_;
    if( $tok ) {
        my $user = $self->get__resets->{$tok};
        if( $user && $user->get__reset_token eq $tok && $user->get__reset_token_good_until > time ) {
            $self->_note( "actiated reset token '$tok'", $user );
            delete $self->get__resets->{$tok};
            return 1, $self->_session( $user );
        }
    }
    $self->_note( "bad or expired token '$tok'" );
    return 0, "bad or expired token";
} #activate_reset


sub artists {
    my $self = shift;
    return 1, $self->get__artists;
}

sub comics {
    my( $self, $args, $sess ) = @_;
    my $start  = $args->{start} || 0;
    my $len    = $args->{length} || 2;
    my $logged_in = $sess ? $sess->get_account : undef;
    my( $comics, $artist );
    if( $args->{artist} ) {
        ( $artist ) = grep { $_->get_display_name eq $args->{artist} } @{$self->get__artists};
        if( $artist ) {
            if( $args->{unfinished} && $logged_in eq $artist ) {
                $comics = $artist->get__unfinished_comics;
            } elsif( $args->{bookmarks} && $logged_in eq $artist ) {
                $comics = $artist->get__bookmarks;
            } else {
                $comics = $artist->get__finished_comics;
            }
        }
    }
    $comics //= $self->get__finished_comics;

    # if logged in, attach a boolean indicating
    # the comic is bookmarked by the logged in artist
    if( $logged_in ) {
        my $bmh = $logged_in->get__bookmark_hash;
        my $ratings = $logged_in->get__ratings;
        for my $comic (@$comics) {
            $comic->vol( 'is_bookmarked', $bmh->{$comic} ? 1 : 0 );
            $comic->vol( 'my_rating', $ratings->{$comic} || 0 );
        }
    }

    if( $args->{sort} eq 'kudos' ) {
        if( $artist ) {
            my( @ku );
            for( my $idx=0; $idx<@$comics; $idx++ ) {
                my $comic = $comics->[$idx];
                my( $pan ) = grep { $_->get_artist eq $artist } @{$comic->get_panels};
                $ku[$idx] = $pan->get_kudos;
            }
            $comics = [map { $comics->[$_] } sort { $ku[$b] <=> $ku[$a] } (0..$#$comics)];
        } else {
            $comics = [sort { $b->get_kudo_count <=> $a->get_kudo_count } @$comics];
        }
    }

    elsif( $args->{sort} eq 'rating' ) {
        $comics = [sort { $b->get_rating <=> $a->get_rating } @$comics];
    }


    elsif( $args->{sort} eq 'comments' ) {
        $comics = [sort { $b->get_comment_count <=> $a->get_comment_count } @$comics];
    }
    elsif( $args->{unfinished} ) {
        # recently updated
        $comics = [sort { $b->get_last_updated <=> $a->get_last_updated } @$comics];
    }
    my $til = $start+$len;
    $til = $til > $#$comics ? $#$comics : $til;
    if( $start > $til ) { $start = $til };
    if( $start < 0 ) { $start = 0; }
    if( $til < 0 ) { $til = 0; }
    my $ret_comics = $start > $til ? [] : [@$comics[$start..$til]];

    return 1, { segment => $ret_comics, list_size => scalar(@$comics) };
} #comics

sub check_login_and_email {
    my( $self, $args ) = @_;
    if( $self->get__login->{lc($args->{login})} ||
        $self->get__email->{lc($args->{email})} ) {
        return 0, 'login or email already taken';
    } elsif( $args->{email} !~ /^\S+\@\S+\.\S+$/ ) {
        return 0, 'invalid looking email';
    } elsif( $args->{login} !~ /^\S+/ ) {
        return 0, 'login too short';
    } else {
        return 1, 1;
    }
} #check_login_and_email

sub login {
    my( $self, $args ) = @_;
    my( $unORem, $pw ) = ( lc( $args->{user} ), $args->{password} );
    my $acct;
    $self->lock( 'SESSIONS EMLOGIN' );
    if( $unORem =~ /\@/ ) {
        $acct = $self->get__email->{$unORem};
    } else {
        $acct = $self->get__login->{$unORem};
    }
    if( $acct ) {
        my $un = $acct->get__login_name;
        my $enc_pw = crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($un) );
	#        if( $enc_pw eq $acct->get__enc_pw ) {
	my $saved_pw = $acct->get__enc_pw;
        if( $enc_pw =~ /$saved_pw/ ) {
            my $avas = $acct->get__avatars;
            $acct->vol( 'avatars', $avas );
            $acct->vol( 'ratings', $acct->get__ratings );
            $acct->vol( 'kudos', $acct->get__kudos );
            $acct->vol( 'last_autosave_avatar', $acct->get__avatar_autosave );
            $acct->vol( 'autosave_avatar_is_dirty', $acct->get__autosave_avatar_is_dirty );
            $acct->vol( 'admin', $acct->get__is_admin );
            $self->_note( "logged in", $acct );
            return 1, $self->_session( $acct );
        }
#        print STDERR Data::Dumper->Dump([$acct->get__enc_pw,$enc_pw,"PW FAIL($pw)"]);
        $self->_note( "$un failed to log in" );
    } else {
#        print STDERR Data::Dumper->Dump(["NO ACT"]);
        # calculate to prevent a timing oracle.
        $self->_note( "unknown login attempt for '$unORem'" );
        if( crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($unORem) ) ) {
            return 0, "login failed";
        }
    }
    return 0, "login failed";

} #login

sub logout {
    my( $self, $args, $sess ) = @_;
    $self->lock( 'SESSIONS' );
    if( $sess && (my $acct = $sess->get_account) ) {
        $self->_note( " logout", $acct );
    }
    if( my $sess_id = $args->{sess_id} ) {
        delete $self->get__session({})->{$sess_id};
    }
    return 1, "logged out";
}

sub send_reset_request {
    my( $self, $email ) = @_;

    my $user = $self->get__email->{lc($email)};

    my $resets = $self->get__resets({});

    my $restok;
    my $found;
    until( $found ) {
        $restok  = int( rand( 10_000_000 ) );
        $found = ! $resets->{$restok};
    }
    if( $user ) {
        $resets->{$restok} = $user;
    }
    my $gooduntil = time + 3600;

    my $site = $self->get__site('madyote.com' );

    $self->_note( "RESET REQUEST FOR '$email'" );

    my $path = $self->get__app_path;
    my $link = "https://$site$path\#recover;tok=$restok";

    my $body_html = <<"END";
<body>
<h1>SPUC Password Reset Request</h1>

<p>
To reset your password, please visit <a href="$link">$link</a>.
This link will work for an hour. If you did not request this, please let coyo know.
</p>

<p>Thanks</p>

<p style="font-style:italic">Scarf Poutine You Clone</p>

</body>
END

    my $body_txt = <<"END";
SPUC Password Reset Request

To reset your password, please visit
$link.
This link will work for an hour.
If you did not request this, please let coyo know.

Thanks
  Scarf Poutine You Clone

END

    my $msg = MIME::Lite->new(
        From => "noreply\@$site",
        To   => $email,
        Subject => 'SPUC Password Reset',
        Type => 'multipart/alternative',
        );

    $msg->attach(Type => 'text/plain', Data => $body_txt);
    $msg->attach(Type => 'text/html',
                 Data => $body_html,
                 Encoding => 'quoted-printable');
    if( $user ) {
        $user->set__reset_token( $restok );
        $user->set__reset_token_good_until( $gooduntil );
        $msg->send;
    }
    return 1, "sent";
} #send_reset_request

sub session_login {
    my( $self, $args ) = @_;
    my $sess_id = $args->{sess_id};
    my $sess = $self->get__session({})->{$sess_id};
    if( $sess ) {
        my $acct = $sess->get_account;
        my $avas = $acct->get__avatars();
        $acct->vol( 'avatars', $avas );
        $acct->vol( 'ratings', $acct->get__ratings );
        $acct->vol( 'kudos', $acct->get__kudos );
        $acct->vol( 'last_autosave_avatar', $acct->get__avatar_autosave );
        $acct->vol( 'autosave_avatar_is_dirty', $acct->get__autosave_avatar_is_dirty );
        $acct->vol( 'admin', $acct->get__is_admin );
        $self->_note( "logged in vis session", $acct );

        return 1, $sess;
    }
    $self->_note( "invalid session login" );
    return 0, 'no session';
} #session_login

sub signup {
    my( $self, $args ) = @_;

    $self->lock( 'SESSIONS EMLOGIN' );
    my( $ok, $msg ) = $self->check_login_and_email( $args );
    unless( $ok) {
        return 0, $msg;
    }
    my $pw = $args->{password};
    my $un = $args->{login};
    my $em = $args->{email};
    if( length($pw) < 7 ) {
        return 0, "Password too short";
    }

    my $enc_pw = crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($un) );
    my $time = time;

    my $acct = $self->store->
        create_container( 'App::SPUC::Part::Artist',
                          {
                              _active_time   => $time,
                              _all_comics    => [],
                              avatar         => $self->get__default_avatar,
                              _avatars       => [],
                              _bookmarks     => [],
                              _created       => $time,
                              display_name   => $un,
                              _login_name    => $un,
                              _email         => $em,
                              _enc_pw        => $enc_pw,
                              _finished_comics => [],
                              _kudos         => {},
                              _login_time    => $time,
                              _unfinished_comics => [],
                              _updates       => [],
                          } );
    $self->get__email->{lc($em)} = $acct;
    $self->get__login->{lc($un)} = $acct;
    $acct->vol( 'avatars', $acct->get__avatars );
    $acct->vol( 'ratings', $acct->get__ratings );
    $acct->vol( 'kudos', $acct->get__kudos );
    $self->add_to__artists( $acct );
    
    $self->_note( "created account for '$un' at '$em'" );
    
    return 1, { session => $self->_session( $acct ), artists => $self->get__artists };

} #signup

# --------------------- LOGGED IN  ACTIONS --------------------------------------


sub add_avatar {
    my( $self, $imgdata, $sess, $files ) = @_;

    my $acct = $sess->get_account;

    my $pic = $self->_make_picture( {
        destdir  => $self->get__image_path."/avatars/".$acct->get__login_name,
        destpath => $self->get__app_path."/images/avatars/".$acct->get__login_name,
        imgdata => $imgdata,
        file    => $files->[0],
                                    } );

    $self->_note( "added avatar", $acct );
    
    $acct->add_to__avatars( $pic );
    $acct->set__autosave_avatar_is_dirty( 0 );
    return 1, $acct->get__avatars;
} #add_avatar

sub add_chat {
    my( $self, $args, $sess ) = @_;
    my $artist = $sess->get_account;
    $self->lock( 'CHATS' );
    my $chats = $self->get__chat;
    push @$chats,
        $self->store->create_container( {
            artist => $artist,
            comment => $args->{message},
            time => time,
                                        } );
    return 1, $#$chats;
} #add_chat

sub add_comment {
    my( $self, $args, $sess ) = @_;
    my $acct = $sess->get_account;
    my $comic = $args->{comic};
    my $comment = $args->{comment};
    if( $comic && $comment ) {
        $self->_note( "added comment", $acct );
        $comic->add_to_comments( $self->store->create_container({ time => time,
                                                                  artist => $acct,
                                                                  comment => $comment } ));

        return 1, $comic;
    }
    return 0, "whaat?";
} #add_comment

sub autosave_avatar {
    my( $self, $imgdata, $sess, $files ) = @_;
    my $acct = $sess->get_account;

    my $autosave = $self->_save_image( {
        field    => '_autosave_avatar', # acct field
        acct     => $acct,
        destdir  => $self->get__image_path."/avatars/".$acct->get__login_name,
        destpath => $self->get__app_path."/images/avatars/".$acct->get__login_name,
        imgdata  => $imgdata,
        file     => $files->[0],
                                       } );

    $acct->set__autosave_avatar_is_dirty( 1 );

    return 1, avatars => $acct->get__avatars;

} #autosave_avatar

sub autosave_panel {
    my( $self, $imgdata, $sess, $files ) = @_;
    my $acct = $sess->get_account;

    my $autosave = $self->_save_image( {
        field    => '_autosave_panel',
        acct     => $acct,
        destdir  => $self->get__image_path."/scratch/".$acct->get__login_name,
        destpath => $self->get__app_path."/images/scratch/".$acct->get__login_name,
        imgdata  => $imgdata,
        file     => $files->[0],
                                       } );
    $acct->set__use_autosave_panel(1);

    return 1, 'autosave panel';

} #upload_panel_autosave

sub bookmark {
    my( $self, $comic, $sess ) = @_;
    my $acct = $sess->get_account;
    if( ! $acct->get__bookmark_hash->{$comic} ) {
        $self->_note( "added bookmark", $acct );
        $acct->get__bookmark_hash->{$comic} = $comic;
        unshift @{$acct->get__bookmarks}, $comic;
        $comic->vol( 'is_bookmarked', 1 );
        $comic->vol( 'my_rating', $acct->get__ratings->{$comic} );
    }
    return 1, $comic;
} #bookmark

sub caption_comic {
    my( $self, $args, $sess ) = @_;
    my $comic = $args->{comic};
    my $caption = $args->{caption};

    if( $caption !~ /\S/ ) {
        return 0, "no caption given";
    }
    if( $comic->get_finished ) {
        return 0, "Comic is already finished";
    }
    my $acct = $sess->get_account;
#    print STDERR Data::Dumper->Dump([$comic->get__player,$comic->get_panels,"CCC"]);
    my $played_by = $comic->get__player;
    if( $played_by && $played_by ne $acct ) {
        return 0, "Someone else is playing this comic";
    }

    my $pan = $self->store->create_container( 'App::SPUC::Part::Panel',
                                              {
                                                  created => time,
                                                  kudos   => 0,
                                                  type    => 'caption',
                                                  artist  => $acct,
                                                  _comic  => $comic,
                                                  caption => $caption,
                                              } );
    $comic->add_to_panels( $pan );
    $comic->set_last_updated( time );
    my $comic_artists = $comic->get__artists;
    map { $_->add_to__updates( {
        comic => $comic,
        msg   => 'a panel was added',
        type  => 'comic',
                               } ) } values %$comic_artists;
    $comic_artists->{$acct} = $acct;

    $acct->add_once_to__all_comics( $comic );

    $acct->set__playing( undef );
    $comic->set__player( undef );

    # check if it is finished now
    if( scalar(@{$comic->get_panels}) >= $comic->get_needs ) {
        $comic->set_finished( 1 );
        $comic->set__player( undef );
        $acct->set__playing( undef );
        $self->remove_from__unfinished_comics( $comic );
        $self->_note( "added caption, finished comic", $acct );
	my $fin = $self->get__finished_comics;
	my $arts = $comic->get__artists;
	for my $art (values %$arts) {
	    $art->remove_all_from__unfinished_comics( $comic );
	    $art->add_to__finished_comics( $comic );
	}
	unshift @$fin, $comic;
        return 1, 1;
    } else {
        $acct->add_once_to__unfinished_comics( $comic );
    }
    $self->_note( "added caption", $acct );
    return 1, 0;

} #caption_comic

sub chats {
    my( $self, $args, $sess ) = @_;
    return $self->_pag_list( $args, $self->get__chat );
} #chats

sub kudo {
    my( $self, $args, $sess ) = @_;
    my $panel = $args->{panel};
    if( $panel ) {
        my $acct = $sess->get_account;
        $panel->kudo( $acct );
        $self->_note( "added kudo", $acct );
        return 1, $panel->get__comic;
    }
    return 0, "whaat?";
} #kudo

sub logs {
    my( $self, $args, $sess ) = @_;
    my $acct = $sess->get_account;
    return 0, 'must be logged in' unless $acct && $acct->get__is_admin;
    return $self->_pag_list( $args, $self->get__logs );
} #logs

sub reset_playing {
    my( $self, $skip, $sess ) = @_;
    my $artist = $sess->get_account;
    if( my $autosave = $artist->get__use_autosave_panel ) {
	$artist->add_to__gallery( $autosave );
	$artist->set__use_autosave_panel( undef );
    }
    if( my $savepan = $artist->get__use_save_panel ) {
	$artist->add_to__gallery( $savepan );
	$artist->set__use_save_panel( undef );
    }
    return 1, "reset";
} #reset_playing

sub play {
    my( $self, $skip, $sess ) = @_;
    my $artist = $sess->get_account;
    
    $self->_note( "played", $artist );
    
    my $playing = $artist->get__playing;
    if( $playing ) {
        if( $artist->get__use_autosave_panel ) {
            if( $artist->get__use_save_panel ) {
                return 1, { comic => $playing, image => $artist->get__save_panel, autosave => $artist->get__autosave_panel, };
            }
            else {
                return 1, { comic => $playing, image => $artist->get__autosave_panel };
            }
        }
        if( $artist->get__use_save_panel ) {
            return 1, { comic => $playing, image => $artist->get__save_panel };
        }

        if( ! $skip ) {
            return 1, { comic => $playing };
        }
	$playing->set__player(undef);
    }

    $self->lock("PLAY");
    
    # find a comic to play
    my( $cand ) = sort { $a*0+rand() <=> $b*0+rand() } grep { ! $_->get__artists->{$artist} } @{$self->get__unfinished_comics};
    if( $cand ) {
        $artist->set__playing( $cand );
        $cand->set__player( $artist );
        return 1, { comic => $cand };
    } else {
        $artist->set__playing( undef );
    }
    return 0, "no comics found. Maybe start one?";

} #play

sub recent_chat_length {
    my( $self, $args, $sess ) = @_;
    my $acct = $sess->get_account;
    return 0, 'must be logged in' unless $acct;

    my $chats = $self->get__chat;
    return 1, $#$chats;
} #recent_chat_length

sub recent_log_length {
    my( $self, $args, $sess ) = @_;
    my $acct = $sess->get_account;
    return 0, 'must be logged in' unless $acct && $acct->get__is_admin;

    my $logs = $self->get__logs;
    return 1, $#$logs;
} #recent_log_length

sub rate_comic {
    my( $self, $args, $sess ) =  @_;
    my $acct = $sess->get_account;

    $self->_note( "rated comic", $acct );
    my $comic  = $args->{comic};
    my $rating = $args->{rating};
    $acct->get__ratings->{$comic} = $rating;
    my $crats = $comic->get__ratings;
    $crats->{$acct} = $rating;
    my $tot = 0;
    my( @ratings ) = map{ $tot += $_; $_ } values %$crats;
    my $avg_rat = @ratings ? $tot/@ratings : 0;
    $comic->set_rating( $avg_rat );
    $acct->vol( 'ratings', $acct->get__ratings );
    $comic->vol( 'my_rating', $rating );
    return 1, { comic => $comic, acct => $acct };
} #rate_comic

sub reset_password {
    my( $self, $args, $sess ) = @_;
    my $acct = $sess->get_account;

    my $pw = $args->{newpassword};
    if( length($pw) < 7 ) {
        return 0, "Password too short";
    }
    my $oldpw = $args->{oldpassword};
    my $reset_token = $args->{token};

    if( $reset_token ) {
        if( $reset_token ne $acct->get__reset_token ) {
            return 0, 'bad or expired token';
        }
        delete $self->get__resets->{$reset_token};
    }
    else {
        my $un = $acct->get__login_name;
        my $enc_old_pw = crypt( $oldpw, length( $oldpw ) . Digest::MD5::md5_hex($un) );
        if( $enc_old_pw ne $acct->get__enc_pw ) {
            return 0, 'wrong password';
        }
    }
    $self->_note( "reset password", $acct );
    $acct->set__reset_token(undef);
    $acct->set__reset_token_good_until(undef);
    my $un = $acct->get__login_name;
    my $enc_pw = crypt( $pw, length( $pw ) . Digest::MD5::md5_hex($un) );
    $acct->set__enc_pw( $enc_pw );
    return 1, "Reset Password";
} #reset_password

sub save_panel {
    my( $self, $imgdata, $sess, $files ) = @_;

    my $acct = $sess->get_account;

    $self->_note( "saved panel", $acct );
    
    my $saved = $self->_save_image( {
        field    => '_save_panel', # acct field
        acct     => $acct,
        destdir  => $self->get__image_path."/scratch/".$acct->get__login_name,
        destpath => $self->get__app_path."/images/scratch/".$acct->get__login_name,
        imgdata  => $imgdata,
        file     => $files->[0],
                                    } );
    $acct->set__use_save_panel(1);
    $acct->set__use_autosave_panel(0);

    return 1, 'saved panel';
} #save_panel

sub select_autosaved_picture {
    my( $self, $caption, $sess ) = @_;
    my $artist = $sess->get_account;

    #copy autosaved to saved
    my $autopan = $artist->get__autosave_panel->get__origin_file;
    my $savepan = $artist->get__save_panel->get__origin_file;

    copy( $autopan, $savepan );
    
    $artist->set__use_autosave_panel(0);
    return 1, "selected auto1saved picture";
} #select_autosaved_picture

sub select_saved_picture {
    my( $self, $caption, $sess ) = @_;
    my $artist = $sess->get_account;

    $artist->set__use_autosave_panel(0);
    return 1, "selected saved picture";
} #select_saved_picture

sub start_comic {
    my( $self, $caption, $sess ) = @_;
    my $artist = $sess->get_account;

    $self->_note( "started comic", $artist );
    
    my $panel = $self->store->create_container( 'App::SPUC::Part::Panel',
                                                {
                                                    artist  => $artist,
                                                    caption => $caption,
                                                    created => time,
                                                    kudos   => 0,
                                                    type    => 'caption',
                                                } );

    my $comic = $self->store->create_container( 'App::SPUC::Part::Comic',
                                                {
                                                    _artists      => { $artist => $artist },
                                                    _creator      => $artist,
                                                    comment_count => 0,
                                                    comments      => [],
                                                    finished      => 0,
                                                    last_updated  => time,
                                                    needs         => 9,
                                                    panels        => [$panel],
                                                    started       => time,
                                                } );
    $panel->set__comic( $comic );
    $artist->add_to__unfinished_comics( $comic );
    $self->add_to__unfinished_comics( $comic );
    $self->add_to__all_comics( $comic );
    return 1, $comic;
} #start_comic

sub unbookmark {
    my( $self, $comic, $sess ) = @_;
    my $acct = $sess->get_account;
    if( $acct->get__bookmark_hash->{$comic} ) {
        delete $acct->get__bookmark_hash->{$comic};
        $self->_note( "removed bookmark", $acct );
        $acct->remove_from__bookmarks( $comic );
        $comic->vol( 'is_bookmarked', 0 );
        $comic->vol( 'my_rating', $acct->get__ratings->{$comic} || 0 );
    }
    return 1, $comic;
} #unbookmark


sub update_account {
    my( $self, $fields, $sess ) = @_;
    my $acct = $sess->get_account;

    
    if( exists $fields->{bio} ) {
        $self->_note( "updated bio", $acct );
        $acct->set_bio( $fields->{bio} );
    }
    if( exists $fields->{avatar} ) {
        $self->_note( "set avatar", $acct );
        $acct->set_avatar( $acct->get__avatars->[$fields->{avatar}] );
    }
    if( exists $fields->{delete_avatar} ) {
        $self->_note( "deleted avatar", $acct );
                
        my $avas = $acct->get__avatars;
        my( $deled ) = splice @$avas, $fields->{delete_avatar}, 1;
        $acct->add_to__deleted_avatars( $deled );
    }
    if( exists $fields->{recover_avatar} ) {
        $self->_note( "recovered avatar", $acct );
        my $avas = $acct->get__avatars;
        my( $ava ) = pop @{$acct->get__deleted_avatars};
        push @$avas, $ava;
    }
    $acct->vol( 'avatars', $acct->get__avatars );
    $acct->vol( 'ratings', $acct->get__ratings );
    $acct->vol( 'kudos', $acct->get__kudos );
    $acct->vol( 'last_autosave_avatar', $acct->get__avatar_autosave );
    $acct->vol( 'autosave_avatar_is_dirty', $acct->get__autosave_avatar_is_dirty );
    return 1, {artist => $acct, avatars => $acct->get__avatars};
} #update_account


sub use_panel {
    my( $self, $data, $sess, $files ) = @_;
    my $acct = $sess->get_account;

    my $comic = $data->{comic};


    # TODO make sure comic is available for uploading the panel
    if( $comic->get_finished ) {
        return 0, "Comic is already finished";
    }
    #    print STDERR Data::Dumper->Dump([$comic->get__player,$comic->get_panels,"CCC"]);
    my $played_by = $comic->get__player;
    if( $played_by && $played_by ne $acct && $played_by->get__playing eq $comic ) {
        return 0, "Someone else is playing this comic";
    }

    my $pic = $self->_make_picture( {
        destdir  => $self->get__image_path."/comics/$comic",
        destpath => $self->get__app_path."/images/comics/$comic",
        imgdata  => $data->{imgdata},
        file     => $files->[0],
                                    } );

    if( $pic ) { #can add the panel
        $self->_note( "added picture to comic", $acct );
	my $comic_artists = $comic->get__artists;
	
        my $pan = $self->store->create_container( 'App::SPUC::Part::Panel',
                                                  {
                                                      created => time,
                                                      kudos   => 0,
                                                      type    => 'picture',
                                                      artist  => $acct,
                                                      _comic  => $comic,
                                                      picture => $pic,
                                                  } );
	if ($comic_artists->{$acct}) {
	    $comic->add_to__extra_panels( $pan );
	} else {
	    $comic->add_to_panels( $pan );
	}
        $comic->set_last_updated( time );

        map { $_->add_to__updates( {
            comic => $comic,
            msg   => 'a panel was added',
            type  => 'comic',
                                   } ) } values %$comic_artists;
        $comic_artists->{$acct} = $acct;
        $acct->set__use_save_panel(0);
        $acct->set__use_autosave_panel(0);
        $acct->set__playing( undef );
        $comic->set__player( undef );
        $acct->add_once_to__unfinished_comics( $comic );
        $acct->add_once_to__all_comics( $comic );

        return 1, $comic;
    } #add the panel

    return 0, "no files uploaded";
} #use_panel

# ----------------- util -------------------

sub _note {
    my( $self, $msg, $acct ) = @_;
    my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    my $tdis = sprintf( "[%02d/%02d/%02d %02d:%02d]", $year%100,$mon+1,$mday,$hour,$min );
    push @{$self->vol( 'notes' )||[]}, "$tdis $msg ".($acct ? "- ".$acct->get_display_name : '' );
}
sub _write_notes {
    my( $self ) = @_;
    $self->unlock;
    $self->lock( "LOG" );
    $self->add_to__logs( @{$self->vol('notes')} );
}

sub _make_picture {
    my( $self, $args ) = @_;

    make_path( $args->{destdir} );
    my $pic = $self->store->create_container( 'App::SPUC::Part::Picture',
                                              {
                                                  extension      => 'png',
                                                  _original_name => $args->{original_name},
                                              } );
    my $origin_file = $pic->set__origin_file( "$args->{destdir}/$pic.png" );
    $pic->set_path( "$args->{destpath}/$pic.png" );
    $pic->set__origin_file( $origin_file );

    _write_image( $args->{imgdata}, $args->{file}, $origin_file );
    return $pic;
} #_make_picture


sub _pag_list {
    my( $self, $args, $list ) = @_;
    my $start = $args->{start};
    my $til   = $start + $args->{length};
    if( $start > $#$list ) { $start = $#$list }
    if( $til > $#$list ) { $til = $#$list }
    if( $start < 0 ) { $start = 0 }
    if( $til < 0 ) { $til = 0 }
    my $ret = { segment => [ map { $list->[$_] } ($start..$til) ], list_size => scalar(@$list) };
    return 1, $ret;
} #_pag_list


sub _write_image {
    my( $imgdata, $file, $origin_file ) = @_;

    if( $imgdata ) {
        my $data = decode_base64($imgdata);
        open my $out, '>', $origin_file;
        print $out $data;
        close $out;
    }
    else {
        my $ft = File::Type->new();
        my $mag = new Image::Magick;
        my( $tmpFn, $fh, $fn ) = @$file;
        my $type = $ft->checktype_filename( $tmpFn );
        if( $type =~ /^image\/(gif|x-png|png|jpeg)/ ) {
            $mag->Read($tmpFn);
            $mag->Write( $origin_file );
        } else {
            die "unrecognizable file type '$type'";
        }
    }
} #_write_image


sub _save_image {
    my( $self, $args ) = @_;
    my $savefield = $args->{field};
    my $image = $args->{acct}->get( $savefield );
    if( $image ) {
        my $origin_file = $image->get__origin_file;
        _write_image( $args->{imgdata}, $args->{file}, $origin_file );
    }
    else {
        $image = $self->_make_picture( {
            destdir   => $args->{destdir},
            destpath  => $args->{destpath},
            imgdata  => $args->{imgdata},
            file     => $args->{file},
                                          } );
        $args->{acct}->set( $savefield, $image );
    }
    return $image;
} #_save_image

sub _session {
    my( $self, $user ) = @_;
    my $sesss = $self->get__session({});
    my $sess_id;
    while(1) {
        $sess_id = int(rand(2**64));
        last unless $sesss->{$sess_id};
    }
    my $session = $self->store->create_container( 'App::SPUC::Session', {
        sess_id => $sess_id,
        created => time,
        account => $user,
                                                  } );
    $sesss->{$sess_id} = $session;
    return $session;
}


1;
