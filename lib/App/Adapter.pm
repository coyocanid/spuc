package App::Adapter;

use strict;
use warnings;

use Apache2::Const;
use Apache2::RequestRec;
use Apache2::RequestIO;
use Apache2::Response;
use Apache2::Upload;

use Data::Dumper;

sub handler {
    my $req = Apache2::Request->new( shift );

    print STDERR Data::Dumper->Dump([$r->args,"ARGS"]);
    $r->content_type( 'text/plain' );
    $r->print( "FOOOOOSDFSDF\n" );
    
    return Apache2::Const::OK;
} #handler

sub handle {
    my $vars = shift;
    my $inputs = from_json( $vars->{payload} || '{}' );

    # --------- find files that are inputted
    my $file_handles = [];
    if( my $files = $vars->{files} ) {
	for( my $i=0; $i<$files; $i++ ) {
	    my $fh = $q->upload("file_$i");
	    my $fn = "$fh";
	    my $tmpfn = $q->tmpFileName( $fh );
	    push @$file_handles, [$tmpfn,$fh,$fn];
	}
    }
    #print STDERR Data::Dumper->Dump([$inputs,"ADA ($$)"]);

    my $appname = $inputs->{app};
    if( $appname =~ /^App::(Yote::DB|SPUC::(Public|LoggedIn))$/ ) {

	my $action  = $inputs->{action};
	my $sess_id = $inputs->{sess_id};
	print STDERR Data::Dumper->Dump([$inputs,"INPUTS, ACTION '$action"]);
	if( $action ) {
	    my $app   = $appname->fetch;
	    my $store = $app->store;
	    
	    my $args    = transform_input_args($inputs->{args},$store);
	    #        print STDERR Data::Dumper->Dump([$inputs,"INPUT ARGS"]);
	    
	    my $sess = $sess_id ? $app->get__session->{$sess_id} : undef;
	    


	    eval {
		my( $succ, $ret ) = $app->act( $action, $args, $sess, $file_handles );
		#            print STDERR Data::Dumper->Dump(["$succ,$ret,$action"]);
		# inthis case, the $ret is a list of $comics, SPUC::Comic objects
		# something to transform that list into JSON, excluding fields starting with '_'
		my $json;
		if( $succ ) {
		    my $pair = datafy( $ret, $sess );
		    $app->store->save;
		    $json = to_json( {
			succ   => 1,
			data   => $pair->{data},
			ret    => $pair->{ret},
				     } );
		    print STDERR Data::Dumper->Dump([$inputs,$json,"RESULT ACTION '$action"]);
		} else {
		    $json = to_json( {
			succ => 0,
			error => $ret,
				     } );
		    print STDERR Data::Dumper->Dump(["$ret","FAILED AAPA action '$action' ($$)"]);
		}


		print "Content-Type: application/json; charset=utf-8\n\n$json";
	    };
	    $app->store->unlock;
	    if( $@ ) {
		print STDERR Data::Dumper->Dump(["ERROR $@","ERROR action '$action' ($$)"]);
		print "Content-Type: text/json\n\n".to_json( { succ => 0, error => 'failed' } );
	    }
	}
	else {
	    print STDERR Data::Dumper->Dump(["NO ACTION GIVEN ($$)"]);
	    print "Content-Type: text/json\n\n".to_json( { succ => 0, error => 'failed' } );
	}
    }
    else {
	print STDERR Data::Dumper->Dump(["WRONG APP"]);
	print "Content-Type: text/json\n\n".to_json( { succ => 0, error => 'failed' } );
    }


    
} #handle

sub datafy {
    my( $obj, $sess ) = @_;
    my $seen = {};
    my $ret = _datafy( $obj, $seen );
    return { ret => $ret, data => $seen };
}
sub _datafy {
    my( $obj, $seen ) = @_;

    my $r = ref( $obj );
    if( $r ) {

	# TODO - scramble ids with the session
	# uses existing ids? okey, not the best
	# but we can update this later at this point
	my $id = $store->existing_id( $obj );

	if( $id && $seen->{$id} ) {
	    return "r$id";
	}

	my $thing = $store->_knot( $obj );
	if( $thing ) {
	    if( $r eq 'ARRAY' ) {
		if( $id ) {
		    my $x = [];
		    $seen->{$id} = $x;
		    @$x = map { _datafy( $_, $seen ) } @$obj;
		    return "r$id";
		}
		return [ map { _datafy( $_, $seen ) } @$obj ];
	    }
	    elsif( $r eq 'HASH' ) {
		if( $id ) {
		    my $x = {};
		    $seen->{$id} = $x;
		    %$x = map { $_ => _datafy( $obj->{$_}, $seen ) } keys %$obj;
		    return "r$id";
		}
		return { map { $_ => _datafy( $obj->{$_}, $seen ) } keys %$obj };
	    }
	    elsif( $r ) {
		my $fields = $obj->fields;
		my $vol_fields = $obj->vol_fields;
		my $x = {};
		$seen->{$id} = $x;
		%$x = map { $_ => _datafy( $obj->get($_), $seen ) } grep { $_ !~ /^_/ } @$fields;
		for my $vf (@$vol_fields) {
		    $x->{$vf} = _datafy( $obj->vol($vf), $seen );
		}
		#                        $obj->clearvols;
		return "r$id";
	    }
	} else {  #not a yote object
	    if( $r eq 'ARRAY' ) {
		return [ map { _datafy( $_, $seen ) } @$obj ];
	    } elsif( $r eq 'HASH' ) {
		return { map { $_ => _datafy( $obj->{$_}, $seen ) } keys %$obj };
	    }
	    die "Got blessed object of type $r in adapter. Cannot send";
	}
    }
    return "v$obj";
} #_datafy


sub transform_input_args {
    my( $arg, $store ) = @_;
    my $r = ref($arg);
    if( $r eq 'ARRAY' ) {
        return [ map { transform_input_args($_,$store) } @$arg ];
    }
    elsif( $r eq 'HASH' ) {
        return { map { $_ => transform_input_args($arg->{$_},$store) } keys %$arg };
    }
    elsif( $arg =~ /^r(.*)/ ) {
        return $store->_fetch( $1 );
    }
    elsif( $arg =~ /^v(.*)/ ) {
        return $1;
    }
    return undef;
}

1;
