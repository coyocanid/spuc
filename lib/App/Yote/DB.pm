package App::Yote::DB;

use strict;
use warnings;

my $singleton;
sub fetch {
    my $cls = shift;
    unless( $singleton ) {
        my $store = Data::ObjectStore->open_store( '/var/www/data/spuc' );
        my $root  = $store->load_root_container;

        unless( $singleton ) {
            $singleton = bless {
                store => $store,
            }, $cls;
        }
    }
    return $singleton;
} #fetch

sub store {
    shift->{store};
}

sub act {
    my( $self, $action, $args ) = @_;
    if( $action =~ /^(load)$/ ) {
        return $self->$action( $args );
    }
}

sub load {
    my( $self, $arg ) = @_;
    if( $arg > 0 ) {
        my $res = $self->{store}->_fetch($arg);
        my $thing = $self->{store}->_knot( $res );
        return 1, {id => $arg, class => ref( $res ), data => $thing->[1]};
    }
    elsif( $arg =~ m~/~ ) {
        my @parts = split( m~/~, $arg );
        my $start = $self->{store}->load_root_container;
        for my $part (@parts) {
            $start = ref($start) eq 'ARRAY' ? $start->[$part] : ref($start) eq 'HASH' ? $start->{$part} : $start->get($part);
        }
        my $thing = $self->{store}->_knot( $start );
        return 1, {id => $thing->[0], path => $arg, class => ref( $start ), data => $thing->[1] };
    }
    return 0, "'$arg' not found";
} #load


1;
