#!/usr/bin/perl

use strict;
use warnings;
no warnings 'uninitialized';

use Data::ObjectStore;
use Yote;

my $store = Data::ObjectStore
    ->new( directory => 'spuc' )
    ->open;


my $spuc = $store->fetch_by_path( '/yote/public_apps/spuc' );

my $all_comics = $spuc->get__all_comics;
my $finished_comics = $spuc->get__finished_comics;
my( %finished ) = map { $_ => $_ } @$finished_comics;

for my $comic (@$all_comics) {
    my $panels = $comic->get_panels;
    next unless @$panels;
    my $should_be_finished = @$panels == 9;
    my $is_in_finished = defined $finished{$comic};
    if( $should_be_finished != $is_in_finished ||
        $should_be_finished != $comic->get_is_finished )
    {
        printf "Comic %s with %d panels. Should be finished %s. Is Finished %s. Is in finished %s\n",
            $panels->[0]->get_caption,
            scalar(@$panels),
            $should_be_finished ? "has enough panels" : "no, has ".scalar(@$panels)."panels",
            $comic->get_is_finished,
            $is_in_finished ? "yes" : "no";
    }
        
}

