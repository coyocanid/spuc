#!/usr/bin/perl

use strict;
use warnings;

use Data::ObjectStore;
use Yote;
`rm -rf spuc; cp -R spuc.converted.backup/RECORDSTORE spuc;`;
my $store = Data::ObjectStore->new( directory => 'spuc' );

$store->open;
my $root = $store->fetch_root;

my $old_app = $root->get_apps->{SPUC}{Public};

my $yote = $root->get_yote;
unless ($yote) {
    $yote = $root->set_yote( $store->create_container( 'Yote' ) );
}

my $apps = $yote->get_public_apps;
my $spuc = $apps->{spuc};
unless ($spuc) {
    $spuc = $apps->{spuc} = $store->create_container( 'Spuc' ) ;
}

$spuc->disallow_methods( 'upload_avatar' );
$spuc->remove_field( 'avatar' );

$spuc->set__image_file_path( '/var/www/html/spuc/images' );
$spuc->set_url_path( '/spuc' );
$spuc->set_image_url_path( '/spuc/images' );


# copy one comic
# my $old_finished_comics = $old_app->get__finished_comics;
# my $old_comic = $old_finished_comics->[0];

# my $new_comic = translate_comic( $old_comic );
# print STDERR Data::Dumper->Dump([$new_comic->{DATA},"NOLDC"]);
# exit;

# my $old_creator = $old_comic->get__creator;
# my $old_comments = $old_comic->get_comments;

# old app
# $VAR1 = {
#           '_finished_comics' => 'r29539',
#           '_artists' => 'r20692',
#           '_app_path' => 'v/spuc',
#           '_image_path' => 'v/var/www/html/spuc/images',
#           '_site' => 'vmadyote.com',
#           '_all_comics' => 'r20822',
#           '_chat' => 'r20738',
#           '_session' => 'r20687',
#           '_file_path' => 'v/var/www',
#           '_login' => 'r20686',
#           '_resets' => 'r29636',
#           '_logs' => 'r29541',
#           '_unfinished_comics' => 'r29540',
#           '_email' => 'r20685',
#           '_default_avatar' => 'r20688'
#         };


$spuc->set__finished_comics( [map { translate_comic($_) }
                              @{$old_app->get__finished_comics} ] );

$spuc->set__artist_handles( [
                                sort { lc($a->get_name) cmp lc($b->get_name) }
                                map { translate_account($_,1) }
                                @{$old_app->get__artists} ] );
$spuc->set__handle2artist( {
    map { $_->get_name => $_ }
    map { translate_account($_,1) }
    @{$old_app->get__artists} } );

$spuc->set__all_comics( [map { translate_comic($_) }
                         @{$old_app->get__all_comics} ] );

$spuc->set__default_avatar( translate_image( $old_app->get__default_avatar ) );

my $old_email = $old_app->get__email;
$spuc->set__email( { map { $_ => translate_account($old_email->{$_}) } keys %$old_email } );

my $old_login = $old_app->get__login;
$spuc->set__login( { map { $_ => translate_account($old_login->{$_}) } keys %$old_login } );

$spuc->set__chat( [ map { $store->create_container( {
    time => $_->get_time,
    comment => $_->get_comment,
    artist_handle => translate_account($_->get_artist,1) } ) }
                    @{$old_app->get__chat} ] );

$spuc->set__session({});
$spuc->set__resets({});
$spuc->set__logs( [@{$old_app->get__logs}] );

#$spuc->allow_methods( 'upload_avatar' );
#print STDERR Data::Dumper->Dump([$spuc->{DATA},$spuc->allowed,"DA"]);

print STDERR Data::Dumper->Dump([$spuc->{DATA},"FUD"]);

print STDERR Data::Dumper->Dump([[ map { ref($_) } @{$spuc->get__artist_handles}],'hands']);
$store->save;

`sudo chown -R apache spuc`;
exit;


my %translated_image;
sub translate_image {
    my( $old_image ) = @_;

    my $new_image = $translated_image{$old_image};
    if ($new_image) {
        return $new_image;
    }

    $new_image = $store->create_container( 'Yote::Image', {
        extension      => $old_image->get_extension,
        file_path      => $old_image->get__origin_file,
        _original_name => $old_image->get__original_name,
        url_path       => $old_image->get_path,
                                           });
    $translated_image{$old_image} = $new_image;

    return $new_image;
    #      extension => vpng
	#      _origin_file => v/var/www/html/spuc/images/avatars/159/162.png
	#      path => v/spuc/images/avatars/159/162.png
	#     _original_name => vArvxolotl.png

} #translate_image


my %translated_account;
sub translate_account {
    my( $old_account, $return_handle ) = @_;
    return unless $old_account;
    my $new_account = $translated_account{$old_account};
    if ($new_account) {
        return $new_account->get_handle if $return_handle;
        return $new_account;
    }
    use Carp 'longmess'; print STDERR Data::Dumper->Dump([longmess,$old_account]) unless ref($old_account);
    my $old_ratings = $old_account->get__ratings({});
    $new_account = $store->create_container
        ( 'Yote::Account',
          {
              created     => $old_account->get__created,
              last_login  => $old_account->get__login_time,
              email       => $old_account->get__email,
              _digest     => $old_account->get_display_name,
              _enc_pw     => $old_account->get__enc_pw,
              _last_active => $old_account->get__active_time,
          } );
    $translated_account{$old_account} = $new_account;
    $new_account->set__all_comics (  [ map { translate_comic($_) } @{$old_account->get__all_comics} ] );
    $new_account->set__bookmarks  (  { map { $_ => $_ } map { translate_comic($_) } @{$old_account->get__bookmarks} } );
    if (my $playing = $old_account->get__playing) {
        $new_account->set_playing     (  translate_comic( $playing  ) );
    }
    $new_account->set__kudos      (  { map { $_ => $_ } map { translate_panel($_) } values %{$old_account->get__kudos}} );
    $new_account->set__unfinished_comics (  [ map { translate_comic($_) } @{$old_account->get__unfinished_comics} ] );
    $new_account->set__finished_comics (  [ map { translate_comic($_) } @{$old_account->get__finished_comics} ] );
    $new_account->set__avatars    (  [ map { translate_image( $_ ) } @{$old_account->get__avatars} ] );
    $new_account->set__ratings    (  { map { map { $_ => $old_ratings->{$_} } translate_comic( $_ ) } keys %$old_ratings } );

    my $handle = $new_account->set_handle( $store->create_container( 'Spuc::Account::Handle' ) );
    $handle->set_name( $old_account->get_display_name );
    $handle->set_bio( $old_account->get_bio );
    $handle->set_avatar( translate_image( $old_account->get_avatar ) );

    return $new_account->get_handle if $return_handle;
    return $new_account;
    
	#                _created => v1523971291
	#                _login_time => v1532618049
	#                avatar => r28873 (App::SPUC::Part::Picture)
    #                extension => vpng
	#                _origin_file => v/var/www/html/spuc/images/avatars/159/162.png
	#                path => v/spuc/images/avatars/159/162.png
	#                _original_name => vArvxolotl.png
	#                _all_comics => r28871  [App::SPUC::Part::Comic]
	#                _enc_pw => v12sKKeOWNqjDI
	#                _bookmarks => r29143 [App::SPUC::Part::Comic => App::SPUC::Part::Comic]
	#                _playing => r21830 (App::SPUC::Part::Comic)
	#                _kudos => r29120
    #                   20830 => r20830 (App::SPUC::Part::Panel)
    #                   20832 => r20832
	#                   20834 => r20834
	# ????   _saved_comic => r21643  (App::SPUC::Part::Comic)
	#                _unfinished_comics => r28870 [ (App::SPUC::Part::Comic) }
	#                bio => vblub
	# ????  _saved_pic => u
	#                _avatars => r28874
	# _updates => r28875
	#                _email => vmogmail@gmx.de
	#                _finished_comics => r28872
	# xxx_login_name => varvor
	#                display_name => vArvor
	#                 _active_time => v1532618147
	# xxxx  _reset_token_good_until => u
	# xxxx  _bookmark_hash => r29144  (just use bookmarks)
    #          25804 is hash with 10 keys
    #          20912 => r20912 App::SPUC::Part::Comic
	#       _ratings => r29145
    #            {Comic --> v4}

    #
    # handle
    #   name
    #   bio
    #   avatar
    #  
    
} #translate_account

my %translated_panel;
sub translate_panel {
    my ( $old_panel ) = @_;
    my $new_panel = $translated_panel{$old_panel};
    return $new_panel if $new_panel;

    $new_panel = $store->create_container( 'Spuc::Panel',
                                          {
                                              map { $_ => $old_panel->get( $_ ) }
                                              qw(created kudos type )
                                          } );
    $translated_panel{$old_panel} = $new_panel;
    $new_panel->set_creator_handle( translate_account( $old_panel->get__creator, 1 ) );
    if ($old_panel->get_type eq 'picture') {
        $new_panel->set_picture( translate_image( $old_panel->get_picture ) );
    } else {
        $new_panel->set_caption( $old_panel->get_caption );
    }
    $new_panel->set_artist_handle( translate_account( $old_panel->get_artist, 1 ) );
    $new_panel->set__comic( translate_comic( $old_panel->get__comic ) );

    return $new_panel;
    
    # 22289 is App::SPUC::Part::Panel
    #    _comic => r22274
	#    _kudo_givers => r25427
	#    artist => r20728
	#    kudos => v4
	#    picture => r22290
	#    type => vpicture
    #    created => u

    # 29765 is App::SPUC::Part::Panel
	#        _comic  => r22274
	#       artist  => r20691
	#       caption => vThey were all going to miss Jeremiah and his wine. 
	#       kudos   => v1
	#       type    => vcaption
    #       created => v1563684177
    
    
}

my %translated_comic;
sub translate_comic {
    my ( $old_comic ) = @_;
    my $new_comic = $translated_comic{$old_comic};
    return $new_comic if $new_comic;
    
    $new_comic = $store->create_container( 'Spuc::Comic',
                                          {
                                              map { $_ => $old_comic->get( $_ ) }
                                              qw( started is_finished rating kudo_count last_updated )
                                          } );
    $translated_comic{$old_comic} = $new_comic;
    $new_comic->set_creator_handle( translate_account( $old_comic->get__creator, 1 ) );
    $new_comic->set_comments( [map {
         {artist_handle=>translate_account($_->get_artist, 1),
          comment => $_->get_comment,
          time    => $_->get_time } } @{$old_comic->get_comments}] );
    if (my $full_pic = $old_comic->get_full_pic) {
        $new_comic->set_full_pic( translate_image( $full_pic ) );
    }
    $new_comic->set__player( translate_account( $old_comic->get__player ) );
    $new_comic->set__artist_handles( { map { $_ => $_ }
                                map { translate_account( $_, 1 ) }
                                values %{$old_comic->get__artists} } );

    my $ratings = $old_comic->get__ratings;
    $new_comic->get__ratings( { map { translate_account( $_ ) => $ratings->{$_} } keys %$ratings } );
    $new_comic->set_panels( [ map { translate_panel($_) } @{$old_comic->get_panels} ] );

    return $new_comic;
    
	#               _creator => r20731
	#              kudo_count => v18
	# ???? needs => v9
	# ???? _hold_expires => v0
	#      comments => r22192
    #       22192 is array with 11 elements
    #            	0) r22189
    #            	1) r22190
    #            	2) r22191
    #            	3) r30494
    #            	4) r30495
    #            	5) r30496
    #            	6) r30546
    #            	7) r30552
    #            	8) r30553
    #            	9) r30587
    #            	10) r30588
    #            >22189
    #            22189 is Data::ObjectStore::Container
    #            	artist => r20709
    #            	comment => vI honestly don't understand the turn this strip took.  Maya?
    #            	time => v1544122180
	#        !!! YES !!! full_pic => r31361
	#    ???? _player => u ????
	#              rating => v4.5
	#              last_updated => v1575903720
	#        _artists => r22188
	#        is_finished => v1
	# xxxx _app => r20684
	#        _ratings => r22187
	# panels => r22194
	#              started => v1528661286
	# xxxx finished => v1 xxxx
    
} #translate_comic

__END__

New structure
  /yote/public_apps/spuc --> Spuc instance

  Spuc instance
    
