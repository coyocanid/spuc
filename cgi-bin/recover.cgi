#!/usr/bin/perl

use strict;
use warnings;
no warnings 'uninitialized';

use lib '/home/wolf/open_source/spuc/lib';

use Data::Dumper;

my $q = new CGI;
my $vars = $q->Vars;

my $inputs = from_json( $vars->{POSTDATA} || '[]' );

print STDERR Data::Dumper->Dump([$inputs,"ADA"]);

