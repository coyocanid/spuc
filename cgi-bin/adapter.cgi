#!/usr/bin/perl

use strict;
use warnings;
no warnings 'uninitialized';

use lib '/var/www/lib';

#
# For security, all the apps that adapter will
# proxy for must be included here
# 
use App::SPUC::Public;

use CGI;
use Data::Dumper;
use JSON;

my $q = CGI->new;

my $DOWN = 0;

if( $DOWN ) {
    my $content = to_json( { succ => 0, content => "vSPUC is down" } );
    print "Content-Type: text/plain\nContent-Length: ".length($content)."\n\n$content";
    exit;
}


my $vars = $q->Vars;

my $inputs = from_json( $vars->{payload} || '{}' );

# --------- find files that are inputted
my $file_handles = [];
if( my $files = $vars->{files} ) {
    for( my $i=0; $i<$files; $i++ ) {
        my $fh = $q->upload("file_$i");
        my $fn = "$fh";
        my $tmpfn = $q->tmpFileName( $fh );
        push @$file_handles, [$tmpfn,$fh,$fn];
    }
}
$vars->{file_handles} = $file_handles;

#my( $content_type, $content ) = App::Adapter::handle( $vars );
#print STDERR Data::Dumper->Dump([$content_type,$content,"RETU"]);

#print "Content-Type: $content_type\nContent-Length: ".length($content)."\n\n$content";

sub transform_input_args {
    my( $arg, $store ) = @_;
    my $r = ref($arg);
    if( $r eq 'ARRAY' ) {
        return [ map { transform_input_args($_,$store) } @$arg ];
    }
    elsif( $r eq 'HASH' ) {
        return { map { $_ => transform_input_args($arg->{$_},$store) } keys %$arg };
    }
    elsif( $arg =~ /^r(.*)/ ) {
        return $store->_fetch( $1 );
    }
    elsif( $arg =~ /^v(.*)/ ) {
        return $1;
    }
    return undef;
}
my $json;
#print STDERR Data::Dumper->Dump([$inputs,"ADA ($$)"]);

my $appname = $inputs->{app};
if( $appname =~ /^App::SPUC::Public$/ ) {

    my $action  = $inputs->{action};
    my $sess_id = $inputs->{sess_id};
#    print STDERR Data::Dumper->Dump([$inputs,"INPUTS, ACTION '$action"]);
    if( $action ) {
        my $app   = $appname->fetch;
        my $store = $app->store;
        
        my $args    = transform_input_args($inputs->{args},$store);
#        print STDERR Data::Dumper->Dump([$inputs,"INPUT ARGS"]);
        
        my $sess = $sess_id ? $app->get__session->{$sess_id} : undef;
        

        sub datafy {
            my( $obj, $sess ) = @_;
            my $seen = {};
            my $ret = _datafy( $obj, $seen );
            return { ret => $ret, data => $seen };
        }
        sub _datafy {
            my( $obj, $seen ) = @_;

            my $r = ref( $obj );
            if( $r ) {

                # TODO - scramble ids with the session
                # uses existing ids? okey, not the best
                # but we can update this later at this point
                my $id = $store->existing_id( $obj );

                if( $id && $seen->{$id} ) {
                    return "r$id";
                }

                my $thing = $store->_knot( $obj );
                if( $thing ) {
                    if( $r eq 'ARRAY' ) {
                        if( $id ) {
                            my $x = [];
                            $seen->{$id} = $x;
                            @$x = map { _datafy( $_, $seen ) } @$obj;
                            return "r$id";
                        }
                        return [ map { _datafy( $_, $seen ) } @$obj ];
                    }
                    elsif( $r eq 'HASH' ) {
                        if( $id ) {
                            my $x = {};
                            $seen->{$id} = $x;
                            %$x = map { $_ => _datafy( $obj->{$_}, $seen ) } keys %$obj;
                            return "r$id";
                        }
                        return { map { $_ => _datafy( $obj->{$_}, $seen ) } keys %$obj };
                    }
                    elsif( $r ) {
                        my $fields = $obj->fields;
                        my $vol_fields = $obj->vol_fields;
                        my $x = {};
                        $seen->{$id} = $x;
                        %$x = map { $_ => _datafy( $obj->get($_), $seen ) } grep { $_ !~ /^_/ } @$fields;
                        for my $vf (@$vol_fields) {
                            $x->{$vf} = _datafy( $obj->vol($vf), $seen );
                        }
#                        $obj->clearvols;
                        return "r$id";
                    }
                } else {  #not a yote object
                    if( $r eq 'ARRAY' ) {
                        return [ map { _datafy( $_, $seen ) } @$obj ];
                    } elsif( $r eq 'HASH' ) {
                        return { map { $_ => _datafy( $obj->{$_}, $seen ) } keys %$obj };
                    }
                    die "Got blessed object of type $r in adapter. Cannot send";
                }
            }
            return "v$obj";
        } #_datafy

        eval {
            my( $succ, $ret ) = $app->act( $action, $args, $sess, $file_handles );
#            print STDERR Data::Dumper->Dump(["$succ,$ret,$action"]);
            # inthis case, the $ret is a list of $comics, SPUC::Comic objects
            # something to transform that list into JSON, excluding fields starting with '_'
            if( $succ ) {
                my $pair = datafy( $ret, $sess );
                $app->store->save;
                $json = to_json( {
                    succ   => 1,
                    data   => $pair->{data},
                    ret    => $pair->{ret},
                                 } );
#                print STDERR Data::Dumper->Dump([$inputs,substr($json,0,500),"RESULT ACTION '$action"]);# if $action =~ /logs/;
            } else {
                $json = to_json( {
                    succ => 0,
                    error => $ret,
                                 } );
#                print STDERR Data::Dumper->Dump(["$ret","FAILED AAPA action '$action' ($$)"]);
            }

        };
        $app->store->unlock;
        if( $@ ) {
            print STDERR Data::Dumper->Dump(["ERROR $@","ERROR action '$action' ($$)"]);
            print "Content-Type: text/json\n\n".to_json( { succ => 0, error => 'failed' } );
            $json = to_json( { succ => 0, error => 'failed' } );
        }
    }
    else {
	#        print STDERR Data::Dumper->Dump(["NO ACTION GIVEN ($$)"]);
	$json = to_json( { succ => 0, error => 'failed' } );
#        print "Content-Type: text/json\n\n".to_json( { succ => 0, error => 'failed' } );
    }
}
else {
#    print STDERR Data::Dumper->Dump(["WRONG APP"]);
    $json = to_json( { succ => 0, error => 'failed' } );
}

#print STDERR "Output $json\n";
print "Content-Type: application/json; charset=utf-8\nContent-Length: ".length($json)."\n\n$json";


__END__

#
# The adapter takes incomming ajax (json) requests directed at web apps
# and returns a json response. 
#
# The incomming requests are contained in the POSTDATA of the request, that is
# the POSTDATA is treated as a JSON string rather than key/value pairs.
#
# The protocol is as follows
# INCOMMING JSON
#   {
#     app    : <string> 'name of the app',
#     sess   : <string> session id
#     action : <string> app method to invoke
#     args   : <any> argument(s) to pass to the action method.
#   }
#
# OUTGOING JSON
#  if unsuccessfull
#   {
#     succ   : '0',
#     error  : <string>, error message if not successful
#   }
#  successfull, array response
#   {
#     succ   : '1',
#     error  : <string>, error message if not successful
#     data   : { id => { field1 => 'vStringVal', field2 => 'rSomeotherIDindata', ... }, id2 => { ... } ... },
#     ret    : [ 'vsomval', 'rsomeIDinData', ... ]
#   }
#  successfull, hash response
#   {
#     succ   : '1',
#     error  : <string>, error message if not successful
#     data   : { id => { field1 => 'vStringVal', field2 => 'rSomeotherIDindata', .. }, id2 => { ... } ... },
#     ret    : { field1 => 'vStringVal', field2 => 'rSomeIDindata', ... },
#   }
#  successfull, scalar string value
#   {
#     succ   : '1',
#     error  : <string>, error message if not successful
#     data   : {},
#     ret    : "vsomething"
#   }
#  successfull, scalar object
#   {
#     succ   : '1',
#     error  : <string>, error message if not successful
#     data   : { id => { field1 => 'vStringVal', field2 => 'rSomeotherIDindata', .. }, id2 => { ... } ... },
#     ret    : "rsomeIDinData"
#   }
#

