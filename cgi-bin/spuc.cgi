#!/usr/bin/perl

#print "Content-type: text/html\n\nTESTING\n";

print "Location: /spuc\n\n";

__END__
#!/usr/bin/perl

use strict;
use warnings;
no warnings 'uninitialized';

use lib "/var/www/lib";
use lib "/home/wolf/open_source/spuc/lib";
use lib '/home/wolf/open_source/recordstore/lib';
use lib '/home/wolf/open_source/objectstore/lib';
use lib '/home/wolf/open_source/yoteserver/lib';



use Encode qw/ decode encode /;
use CGI;

use Data::Dumper;
use File::Path qw/make_path/;

use SPUC::RequestHandler;
use SPUC::Uploader;

# ---------------------------------------
#     config
# ---------------------------------------

our $basedir = "/var/www";
our $datadir = "$basedir/data/spuc";
our $lockdir = "$basedir/lock/spuc";
our $template_dir = "$basedir/templates/spuc";
our $config = {
    app          => 'SPUC',
    app_class    => 'SPUC::App',
    app_path     => '/cgi-bin/spuc.cgi',
    basedir      => $basedir,
    datadir      => $datadir,
    imagedir     => "$basedir/html/spuc/images",
    lockdir      => $lockdir,
    logdir       => '/tmp/log',
    user_idle_timeout => 3600*24*30*6, # 6 'months'
    session_timeout   => 3600*24*30*3, # 3 'months'
    site         => $ENV{SERVER_NAME},
    template_dir => $template_dir,
};

umask(0);

my $group = getgrnam( 'www-data' );
make_path( $datadir, $lockdir, $template_dir,
           { group => $group, mode => 0775, error => \my $err } );


# ---------------------------------------
#     request
# ---------------------------------------

my $q = new CGI;

my $params = $q->Vars;

# grab session
my $sess_id = $q->cookie('session');
my( $path ) = ( $ENV{QUERY_STRING} =~ /path=([^\&\#]*)/ );
$path ||= '/';

# ---------------------------------------
#     processing
# ---------------------------------------

#
# uploads..hmmm
#
my $uploader = SPUC::Uploader::from_cgi( $q );

my( $content_ref, $status, $new_sess_id, $content_type );
if( $path eq '/RPC' ) {
    print STDERR Data::Dumper->Dump([$params, $sess_id, "RPC CALL"]);
    ( $content_ref, $status, $new_sess_id )
        = SPUC::RequestHandler::handle_RPC( $params, $sess_id, $uploader );
    $content_type = 'text/json';
}
else {
    ( $content_ref, $status, $new_sess_id )
        = SPUC::RequestHandler::handle( $path, $params, $sess_id, $uploader, $config );
    $content_type = 'text/html;charset=UTF-8';
}
# ---------------------------------------
#     result
# ---------------------------------------
my $sesscook;

if( $sess_id ne $new_sess_id ) {
    if( $new_sess_id ) {
        $sesscook = $q->cookie(
            -name  => 'session',
            -value => $new_sess_id,
            );
    } else {
        # no new session id, so remove the old one
        $sesscook = $q->cookie(
            -name  => 'session',
            -expired => '+1s',
            -value => 0,
            );
    }
}

print $q->header( 
    -type => $content_type,
    -cookie => [$sesscook],
    -status => $status,
    -charset => 'utf-8',
 );

my $out = $$content_ref;
print $out;
