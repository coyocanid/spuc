import { act, Paginator, match, matches, byId } from "/spuc/js/tools.js";
import  "/spuc/js/imed.js";

if( true || window.location.hash == '#check' ) { 

const IO = {
    app     : 'App::SPUC::Public',
    sess_id : window.localStorage.getItem( 'sess_id' ),
    cache   : {},
    loading : 0,
    rpc     : function( action, args, files ) {
        this.loading++;
        if( !Array.isArray(files) ) files = [files];
        return act( {
            action, args, files,
            cache   : this.cache,
            sess_id : this.sess_id,
            app     : this.app,
        } ).then( r => {
            this.loading--;
            return r;
        } );
    },
};

class TailingPaginator extends Paginator {
    constructor( args ) {
        super( args );
        this.IO        = args.IO;
        this.period    = args.period || 10000;
        this.load_fun  = args.load_fun;
        this.check_fun = args.check_fun;
        this.autojump  = args.autojump || false;
        this.reverse   = args.reverse  || false;

        this.interval  = undefined;
        this.polling   = false;
        this.has_more  = false;
        
        this.pagefun = (start,chunksize) => {
            return new Promise( (resolve,reject) => {
                this.IO.rpc( this.load_fun, {
                    start,
                    length : chunksize,
                } ).then( res => {
                    resolve( { segment : res.segment, list_size : res.list_size } );
                } ).catch( e => reject(e) );
            } );
        };

    } //constructor
    refresh() {
        let that = this;
        return this.IO.rpc( this.check_fun )
            .then( item_count => {
                let needsInit = isNaN( this.list_last_idx);
                if( item_count > this.list_last_idx || needsInit ) {
                    let start_point = item_count - (this.window_size-2);
                    if( start_point < 1 ) start_point = 1;
                    this.list_last_idx = item_count;
                    if( this.autojump || needsInit )
                        return this.jump_to_last();
                    else
                        this.has_more = true;
                }
                return new Promise( (res,rej) => {
                    return this.window;
                } );
            } );
    }
    jump_to_last() {
        let jump_to = (this.list_last_idx - this.window_size) + 1;
        if( jump_to < 0 ) jump_to = 0;
        return this.set_position(jump_to).then( e => {
            this.restart_polling();
            if( this.reverse ) {
                e.reverse();
            }
            return e;
        } );
    }
    restart_polling() {
        if( this.polling ) {
            this.stop_polling();
            this.polling = true;
            this.start_polling();
        }
    }
    start_polling() {
        this.interval = setInterval( this.refresh.bind(this), this.period );
        this.polling = true;
    }
    stop_polling() {
        if( this.interval ) clearInterval( this.interval );
        this.polling = false;
    }
} //TailingPagininator
// ---------- MODEL -----------
class ComicsPaginator extends Paginator {
    constructor( args ) {
        super( args );
        this.spuc    = args.spuc;
        this.sort    = 'recent';
        this.artist  = undefined;
        this.detail  = 0;
        this.unfinished = 0;        
        this.bookmarks = 0;
        this.pagefun = (start,chunksize) => {
            return this.spuc.IO.rpc( 'comics',
                        {
                            start, length : chunksize,
                            artist : this.artist ? this.artist.display_name : undefined,
                            sort   : this.sort,
                            unfinished : this.unfinished,
                            bookmarks  : this.bookmarks,
                        },
                      );
        };
    }
    set_unfinished( unf ) {
        if( this.unfinished !== unf ) {
            this.unfinished = unf;
            if( unf ) {
                this.bookmarks = 0;
            }
            this.reset();
        }
    }
    set_bookmarks( bm ) {
        if( this.bookmarks !== bm ) {
            this.bookmarks = bm;
            if( bm ) {
                this.unfinished = 0;
            }
            this.reset();
        }
    }
    set_sort( sort ) {
        if( this.sort !== sort ) {
            this.sort = sort;
            this.reset();
        }
    }
    set_artist( art ) {
        if( this.artist !== art ) {
            this.artist = art;
            this.reset();
        }
        if( ! art ) {
            this.bookmarks = undefined;
            this.unfinished = undefined;
        }
    }
    url( args ) {
        let start  = (args.window_start !== undefined ? args.window_start : this.window_start) || 0;
        let artist = args.artist !== undefined ? args.artist : this.artist ? this.artist.display_name : undefined;
        let unfinished = args.unfinished ? 1 : this.unfinished ? 1 : 0;
        let bookmarks  = args.bookmarks ? 1 : this.bookmarks ? 1 : 0;
        let sort   = ( args.sort !== undefined ? args.sort : this.sort ) || 'recent';
        let amt    = ( args.amt !== undefined ? args.amt : this.window_size ) || 4;
        let detail = ( args.detail !== undefined ? args.detail : this.detail ) || 0;
        let dest   = args.dest || 'comics'
        return '#' + dest +
            ';start='+start+
            ( artist ? ';artist=' + artist : '' ) +
            ( unfinished ? ';unfinished=1' : '' ) +
            ( bookmarks ? ';bookmarks=1' : '' ) +
            ';sort=' + sort +
            ';detail=' + detail +
            ';amt=' + amt;
    }
    next_url() {
        return this.url({ window_start : this.next_idx() });
    }
    prev_url() {
        return this.url({ window_start : this.prev_idx() });
    }
    sort_url(sort) {
        return this.url({ sort, window_start: 0 });
    }
    back_url() {
        return this.url({ detail : 0, amt : 4 });
    }
    detail_url(idx) {
        return this.url({ window_start : idx, detail : 1, amt : 1 });
    }
    artist_url(name) {
        return this.url({window_start:0,detail:0, amt: 4, dest:'artist', artist:name } );
    }

} //ComicsPaginator

class SPUC {
    constructor(IO) {
        this.IO  = IO;

        this.login          = undefined;
        this.showing        = undefined;

        this.artists = {};
        this.idxartists = [];
        this.chats = [];

        this.last_play = undefined;

        this.show_chat = true;

        this.err = '';

        this.comics_paginator = new ComicsPaginator( {
            fetch_chunk_size : 20,
            max_cached       : 400,
            window_size      : 4,
            spuc             : this,
        } );
        
        this.artist_paginator = new Paginator( {
            fetch_chunk_size : 100,
            max_cached       : 400,
            window_size      : 1,
            pagefun : (start,chunksize) => {
                return new Promise( (resolve,reject) => {
                    resolve( { segment : this.idxartists.slice(start,start + chunksize), list_size : this.idxartists.length } );
                } );
            },
        } );

        this.chat_paginator = new TailingPaginator( {
            IO               : this.IO,
            fetch_chunk_size : 100,
            max_cached       : 400,
            window_size      : 25,
            autojump         : true,
            reverse          : true,
            load_fun         : 'chats',
            check_fun        : 'recent_chat_length',
        } );

        
        this.log_paginator = new TailingPaginator( {
            IO               : this.IO,
            fetch_chunk_size : 100,
            max_cached       : 1000,
            window_size      : 20,
            reverse          : true,
            load_fun         : 'logs',
            check_fun        : 'recent_log_length',
        } );


        this.public_nav_list = [ ['comics'],
                                 ['artists'],
                               ];
        this.logged_in_nav_list = [
                                    ['play', () => {
                                        if( window.location.hash == '#play' ) {
                                            this.show_play(true);
                                        } else {
                                            window.location.hash = '#play';
                                        }
                                    } ],
                                    ['start a comic', 'start'],
                                    ['my comics', () =>  {
                                        spuc.artist = spuc.login;
                                        window.location.hash = 'comics;artist=' + spuc.login.display_name;
                                    }],
                                    ['unfinished comics', () => {
                                        spuc.artist = spuc.login;
                                        window.location.hash = '#comics;unfinished=1;artist=' + spuc.login.display_name;
                                    }],
                                    ['bookmarked comics', () => {
                                        spuc.artist = spuc.login;
                                        window.location.hash = '#comics;bookmarks=1;artist=' + spuc.login.display_name;
                                    }],
                                    ['edit profile','profile'],
                                    ['log out',()=>{this.logout().then( r => {
                                        spuc.msg = 'logged out';
                                        window.location.hash = '#comics';
                                    } )}],
                                  ];
        this.admin_nav_list = [ [ 'logs' ] ];
    } //constructor

    init() {
        // check if there is a sess_id to login with
        if( this.IO.sess_id ) {
            this.IO.rpc( 'session_login', { sess_id : this.IO.sess_id } )
                .then( sess => {
                    this.login = sess.account;
                    this.artist = sess.account;
                    this.chat_paginator.refresh().then( e => this.chat_paginator.start_polling() );
                    this._start_nav();
                } )
                .catch(e => {
                    this.IO.sess_id = undefined;
                    spuc.msg = 'logged out';
                    window.location.hash = '#comics';
                    window.localStorage.removeItem( 'sess_id' );
                    this._start_nav();
                } );
        } else {
            this._start_nav();
        }

    } //init
    _start_nav() {
        this.comics_paginator.start().then( e => {
            this.nav( match( window.location.href, /\#(.*)/ ) ? matches[1] : '' );
        } );
        window.addEventListener( 'hashchange', ev => {
            let hash = match( ev.newURL, /\#(.*)/ ) ? matches[1] : '';
            this.nav( hash );
        } );
    }

    // -------------------- main view changes ------------------------

    nav( hash ) {
        this.err = '';
        let parts = hash.split( ';' );
        let page = parts[0];
        let args = {};
        for( let i=1; i<parts.length; i++ ) {
            if( match( parts[i], /(.*)=(.*)/ ) ) {
                args[matches[1]] = matches[2];
            }
        }
        let shower = (this['show_'+page] || this.show_comics ).bind(this);
                    
        shower( args );
        this.msg = undefined;
    } //nav

    show_artists() {
        this.load_artists()
            .then( arts => this.showing = 'artists' );
    }
    show_artist(args) {
        this.load_artists()
            .then( arts => {
                let artist = this.artists[args.artist];
                if( artist ) {
                    this.artist_paginator.set_position( artist.idx );
                    this.artist = artist;
                    this.comics_paginator.detail = args.detail || 0;
                    this.comics_paginator.window_size = 4;
                    this.comics_paginator.set_artist( artist );
                    this.comics_paginator.start().then( e => this.showing = 'artist' );
                } else {
                    this.showing = 'comics';
                }
            } );
    } //show_artist
    show_comics(args) {
        this.comics_paginator.window_size = parseInt(args.amt)||4;
        this.comics_paginator.set_sort( args.sort || 'recent' );
        this.comics_paginator.detail = args.detail || 0;
        if( args.artist ) {
            this.load_artists()
                .then( artists => {
                    this.comics_paginator.set_artist(this.artists[args.artist]);
                    this.comics_paginator.set_unfinished(args.unfinished ? 1 : 0 );
                    this.comics_paginator.set_bookmarks(args.bookmarks ? 1 : 0 );
                    let start = args && args.start ? parseInt(args.start) : 0;
                    return this.comics_paginator.set_position(start).then( e => this.showing = 'artist' );
                } );
        } else {
            this.comics_paginator.set_artist( undefined );
            let start = args.start ? parseInt(args.start) : 0;
            return this.comics_paginator.set_position(start).then( e => {
                this.showing = 'comics';
            });
        }
    } //show_comics
    show_logs() {
        this.log_paginator.refresh();
        this.showing = 'logs';
    }
    show_reset_request(recover_idx) {
        this.showing = 'reset-request';
    }
    show_profile() {
        if( this.login )
            this.showing = 'profile';
        else {
            this.err = 'need to be logged in to edit profile';
            this.showing = 'comics';
        }
    }
    show_recover( args ) {
        if( args.tok ) {
            this.IO.rpc( 'activate_reset', args.tok )
                .then( sess => {
                    this.login    = sess.account;
                    this.chat_paginator.refresh();
                    this.token    = args.tok;
                    this.IO.sess_id       = sess.sess_id;
                    window.localStorage.setItem( 'sess_id', this.IO.sess_id );
                    this.msg = 'You are now logged in and may now reset your password';
                    this.showing  = 'recover';
                } )
                .catch(e => {
                    this.showing = 'recover-err';
                } );
        } else {
            this.showing = 'recover-err';
        }
    } //show_recover
    show_signup() {
        this.showing = 'signup';
    }

    show_start() {
        if( this.login )
            this.showing = 'start';
        else {
            this.err = 'need to be logged in to start a comic';
            this.showing = 'comics';
        }
    }
    
    show_play(skip_last) {
        if( this.login ) {

            this.IO.rpc( 'play', skip_last ? 1 : 0 )
                .then( args => {
                    this.last_play = args.comic;
                    this.last_picture_autosave = args.autosave;
                    this.last_picture = args.image;
                    this.showing = 'play';
                } )
                .catch( msg => {
                    if( msg.match( /no comics found/ ) ) {
                        this.showing = 'start';
                        spuc.msg = msg;
                        Vue.nextTick( () => spuc.msg = msg  );
                    }
                } );
        } else {
            this.err = 'need to be logged in to play';
            this.showing = 'comics';
        }
    }
    // ---------- actions ----------------

    // calls the other actions

    add_chat(message) {
        return this.IO.rpc( 'add_chat', { message } );
    }
    load_artists() {
        if( Object.keys(this.artists).length ) {
            return new Promise( (res,rej) => {
                res(this.artists);
            } );
        }
        return this.IO.rpc('artists')
            .then( arts => {
                this.idxartists = arts;
                arts.forEach( (a,idx) => {
                    a.idx = idx;
                    this.artists[a.display_name] = a;
                } );
            } );
    } //load_artists
    logout() {
        window.localStorage.removeItem( 'sess_id' );
        return this.IO.rpc('logout')
            .finally( r => {
                this.login   = undefined;
                this.chat_paginator.stop_polling();
                
                this.IO.sess_id      = undefined;
                this.msg     = 'logged out';
                this.showing = 'comics';
            } );
    } //logout
    reset_password(newpassword,token,oldpassword) {
        return this.IO.rpc( 'reset_password', {newpassword,token,oldpassword} );
    }
    send_reset_request( email ) {
        return this.IO.rpc( 'send_reset_request', email );
    }
    try_login(user,password) {
        return this.IO.rpc( 'login', { user, password } )
            .then( sess => {
                this.login = sess.account;
                this.chat_paginator.refresh();
                this.IO.sess_id    = sess.sess_id;
                window.localStorage.setItem( 'sess_id', this.IO.sess_id );
            });
    } //try_login
    signup(email,login,password) {
        return this.IO.rpc( 'signup', { email, login, password } )
            .then( args => {
                let sess = args.session;
                this.artists = args.artists;
                this.login   = sess.account;
                this.chat_paginator.refresh();

                this.IO.sess_id      = sess.sess_id;
                Vue.nextTick( () => this.msg = "Created and logged into account." );
                window.localStorage.setItem( 'sess_id', sess.sess_id );
                window.location.hash = '#profile';
            } );
    }

    update_account(fields) {
        return this.IO.rpc( 'update_account', fields ).then( res => {
            this.login = res.artist;
            this.artists[res.artist.display_name] = res.artist;
        } );
    }
    // --------------- utility ------------------------
    format_time(time) {
        let d = new Date(1000*time);
        return d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
    }
    dump() {
        console.log( this,"SPUCDUMP" );
    }
} //class SPC

// --------- THE ACTIVE STUFF

let spuc = new SPUC(IO);
spuc.init();


// ---------- UI -----------


Vue.component('artist', {
    data : function() {
        return { spuc };
    },
    template : `<div class="col" v-if="spuc.comics_paginator.artist">
                  <div class="row squish" v-if="! (spuc.login && spuc.comics_paginator.artist.display_name === spuc.login.display_name && (spuc.comics_paginator.unfinished || spuc.comics_paginator.bookmarked ))">
                       <a v-if="spuc.artist_paginator.has_prev()"
                          v-bind:href="'#artist;artist='+spuc.idxartists[spuc.comics_paginator.artist.idx-1].display_name"
                          >&lt; prev artist</a>
                       <div v-else>&lt; prev artist</div>

                       <div class="col squish">
                          <img class="big-avatar" style="margin:0 auto" v-bind:title="spuc.comics_paginator.artist.display_name" v-bind:src="spuc.comics_paginator.artist.avatar.path">
                          <span class="x-large center">{{ spuc.comics_paginator.artist.display_name }}</span>
                       </div>

                       <a v-if="spuc.artist_paginator.has_next()"
                          v-bind:href="'#artist;artist='+spuc.idxartists[spuc.comics_paginator.artist.idx+1].display_name"
                          >next artist &gt;</a>
                       <div v-else>next artist &gt;</div>
                  </div>

                  <div v-if="spuc.comics_paginator.artist.bio && ! (spuc.login && spuc.comics_paginator.artist.display_name === spuc.login.display_name && (spuc.comics_paginator.unfinished || spuc.comics_paginator.bookmarked ))" class="large italic center">&ldquo;{{ spuc.comics_paginator.artist.bio }}&rdquo;</div>
                  <comics v-if="spuc.comics_paginator.window.length > 0"/>
                  <div class="request" v-else>No Comics Yet</div>
                </div>`,
} );

Vue.component('artist-blurb', {
    data : function() { return { spuc, artist : this.$attrs.artist } },
    template : `<a v-bind:href="'#artist;artist='+artist.display_name" style="text-decoration:none;border:solid 2px black; margin: 5px 0;font-size:larger; cursor:pointer">
                 <div class="row">
                   <div class="col"><img class="avatar" v-bind:src="artist.avatar.path"></div>
                   <div class="col">
                    <span>{{ artist.display_name }}</span>
                    <span>{{ artist.bio }}</span>
                   </div>
                 </div>
               </a>
               `,
} ); //artist-blurb

Vue.component('artists', {
    data : function() { return { spuc } },
    template : `<div class="row">
                  <div class="col">
                    <h2>The artists of SPUC</h2>
                    <artist-blurb v-for="art in Object.keys(spuc.artists).sort().map( aname => spuc.artists[aname] )" 
                                  v-bind:key="art.display_name"
                                  v-bind:artist="art"/>
                  </div>
                  <chat></chat>
               </div>`,
} );

Vue.component('chat', {
    data : function() {
        return { spuc }
    },
    methods : {
        add_chat : function( ev ) {
            let msg = ev.target.value;
            if( msg.match(/\S/) ) {
                ev.target.value = '';
                this.spuc.add_chat( msg )
                    .then( () => spuc.chat_paginator.refresh() );
            }
        },
    },
    template : `<div v-if="spuc.login" class="col shrink">
                    <h2>Chatter</h2>
                    <div v-if="spuc.login">
                      <input type="text"
                             v-on:change="add_chat($event)">
                    </div>
                    <div v-for="c in spuc.chat_paginator.window" class="chat-entry">
                      <img class="tiny-avatar" v-bind:title="c.artist.display_name" v-bind:src="c.artist.avatar.path">
                      {{ c.comment }}
                    </div>
                </div>`,
} ); //chat


Vue.component('comic', {
    data : function() { return { spuc,
				 IO,
                                 comic       : this.$attrs.comic,
                                 comic_idx   : this.$attrs.comic_idx,
                                 bookmarked  : this.$attrs.comic.is_bookmarked,
                                 new_comment : '' } },
    methods : {
        add_comment : function() {
            if( this.new_comment.match(/\S/) ) {
                this.IO.rpc( 'add_comment', { comic : this.comic, comment : this.new_comment } )
                    .then( comic => {
                        this.new_comment = '';
                        this.comic = comic;
                    } )
                    .catch( e => {
                        spuc.err = e;
                    } );
            }
        },
        kudo : function(panel) {
            this.IO.rpc( 'kudo', { panel } )
                .then( comic => {
                    spuc.login.kudos[panel.id] = panel;
                    this.comic = comic;
                } );
        },
        rate : function(rating) {
            this.IO.rpc( 'rate_comic', { comic : this.comic, rating } )
                .then( ret => this.comic = ret.comic );
        },
        bookmark : function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            this.IO.rpc( 'bookmark', this.comic )
                .then( comic => this.comic = comic );
        },
        unbookmark : function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            this.IO.rpc( 'unbookmark', this.comic )
                .then( comic => this.comic = comic );
        },
    },
    template : `<div v-if="spuc.comics_paginator.detail==1" class="col strip" style="border:solid 1px black; margin:3px; max-width: 660px">
                   <div v-if="spuc.login">
                      <div class="row spread standout">
                         <a href="#" v-if="comic.is_bookmarked==1" 
                            v-on:click="unbookmark($event)"> unbookmark </a>
                         <a href="#" v-else v-on:click="bookmark($event)"> bookmark </a>
			 <a v-if="comic.is_finished==1" target="_blank" v-bind:href="comic.full_pic.path">image</a>
                         <div class="row">
                            <span class="row" 
                                  v-bind:style="'cursor:pointer;' + (comic.my_rating == (1+r) ? 'color:red;' : '' ) "
                                  v-bind:title="(1+r) == comic.my_rating ? 'my rating '+(1+r) : comic.rating > 0 ? 'average rating '+comic.rating : 'comic not yet rated'"
                                  v-on:click="rate(r+1)"
                                  v-for="r in [0,1,2,3,4]">
                              {{ comic.rating > r ? '&starf;' : '&star;' }}
                            </span>
                         </div>
                      </div>
                   </div>
                   <div v-else class="row standout">
                      <span class="row" 
                            v-bind:title="comic.rating > 0 ? 'average rating '+comic.rating : 'comic not yet rated'"
                            v-for="r in [1,2,3,4,5]">
                       {{ comic.rating < r ? '&star;' : '&starf;' }}
                      </span>
                   </div>

                   <div v-for="p in comic.panels" class="row" style="margin-bottom:3px">
                      <div class="col">
                        <a style="margin-bottom:4px" v-bind:href="spuc.comics_paginator.artist_url(p.artist.display_name)">
                          <img class="avatar" v-bind:title="p.artist.display_name"  v-bind:src="p.artist.avatar.path">
                        </a>
                        <div class="row squish" style="cursor:pointer"
                             v-if="spuc.login && p.artist.id != spuc.login.id && undefined === spuc.login.kudos[p.id]"
                             v-on:click="kudo(p);">
                          <span class="x-large" style="color:red">&#x2764;</span>
                          <span>{{ p.kudos }}</span>
                        </div>
                        <div v-else class="row squish">
                          <span>&#x2764;</span>
                          <span>{{ p.kudos }}</span>
                        </div>
                      </div>
                      
                      <div v-if="p.type === 'caption'" class="detail panel caption" style="flex-grow:1">{{p.caption}}</div>
                      <div class="detail panel" v-else><img class="detail picture" v-bind:src="p.picture.path"></div>
                   </div>
                   <div class="col">
                      <h3 style="text-align:center">Comments</h3>
                      <table>
                        <tr v-if="spuc.login">
                           <td><img class="tiny-avatar" v-bind:src="spuc.login.avatar.path" style="margin:0 auto"></td>
                           <td></td>
                           <td><div class="row"><input class="grow" type="text"
						       v-bind:value="new_comment"
						       v-on:input="new_comment=$event.target.value"
						       @keyup.enter="add_comment()"
						       placeholder="say something">
                               <button type="button" v-on:click="add_comment()">add comment</button></div></td>
                        </tr>
                        <tr v-for="c in comic.comments">
                           <td v-bind:title="c.artist.display_name"><img class="tiny-avatar" v-bind:src="c.artist.avatar.path" style="margin:0 auto"></td>
                           <td class="x-small">{{ c.artist.display_name }}</td>
                           <td>{{ c.comment }}</td>
                        </tr>
                      </table>
                   </div>
                </div>
                <div v-else class="col strip" style="border:solid 1px black; margin:3px;">
                   <a class="button" v-bind:href="spuc.comics_paginator.detail_url(comic_idx)">
                     <div style="cursor:pointer;text-size:larger; margin-bottom:10px;">&#x1F50D;</div>
                     <div v-for="p in comic.panels" class="col">
                        <div class="row" style="margin-bottom:3px">
                           <div v-if="p.type === 'caption'" class="panel caption" style="flex-grow:1">{{p.caption}}</div>
                           <div class="panel" v-else><img class="picture" v-bind:src="p.picture.path"></div>
                        </div>
                     </div>
                   </a>
                </div>`,
} ); //comic

Vue.component('comics', { //sort most kudos, etc
    data : function() { return { spuc } },
    computed : {
        blurb : function() {
            let bl = 'Showing ';
            let comics = 1 + spuc.comics_paginator.list_last_idx;
            let comic = (1+spuc.comics_paginator.window_start);
            if( spuc.comics_paginator.window_size > 1 ) {
                let comic_to = comic + spuc.comics_paginator.window_size - 1;
                let winend = spuc.comics_paginator.list_last_idx + 1;
                if( comic_to > winend ) comic_to = winend;
                bl = 'comics ' + comic + " to " + comic_to + ' of ' + comics;
            }
            else {
                bl = 'comic ' + comic + ' of ' + comics;
            }
            return bl;
        }
    },
    template : `<div class="row">
                 <div class="col grow">
                  <div class="row squish standout">
                      <div class="col x-large bold">
                           {{ spuc.comics_paginator.artist ? spuc.comics_paginator.artist.display_name + "'s" : '' }}
                            {{ spuc.comics_paginator.unfinished ? 'unfinished' : '' }}
                            {{ spuc.comics_paginator.bookmarks ? 'bookmarked' : '' }}
                            Comics
                      </div>
                       <span v-if="spuc.comics_paginator.sort == 'recent'" class="bold">Showing Most Recent</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('recent')">Most Recent</a>

                       <span v-if="spuc.comics_paginator.sort == 'comments'" class="bold">Showing Most Commented</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('comments')">Most Commented</a>

                       <span v-if="spuc.comics_paginator.sort == 'kudos'" class="bold">Showing Most Kudoed</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('kudos')">Most Kudoed</a>

                       <span v-if="spuc.comics_paginator.sort == 'rated'" class="bold">Showing Top Rated</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('rating')">Top Rated</a>
                  </div>

                  <div class="row squish center standout">
                      <a v-if="spuc.comics_paginator.has_prev()" v-bind:href="spuc.comics_paginator.prev_url()">&lt; prev</a>
                      <span v-else >&lt; prev</span>

                      <a v-if="spuc.comics_paginator.detail==1" v-bind:href="spuc.comics_paginator.back_url()"> ^back^ </a>

                      <a v-if="spuc.comics_paginator.has_next()" v-bind:href="spuc.comics_paginator.next_url()">next &gt;</a>
                      <span v-else>next &gt;</span>
                  </div>

                  <div class="row even">
                     <comic v-for="(c,idx) in spuc.comics_paginator.window"
                            v-bind:comic="c"
                            v-bind:key="c.id"
                            v-bind:comic_idx="idx+spuc.comics_paginator.window_start"
                           ></comic>
                  </div>
                 </div>
                <chat></chat>
              </div>`,
} );


Vue.component('err', {
    data : function() { return { err : this.$attrs.err, thing : this.$attrs.thing } },
    template : `<div v-if="thing.err" class="col" style="border:4px solid red;background:pink">
                     {{ thing.err }}
                </div>`
} ); //err

Vue.component('logged-in', {
    data : function() { return { spuc } },
    template : `<aside class="col center pad">
                  <span style="font-size:large"> {{ spuc.login.display_name }}</span>
                  <a href="#profile"><img class="big-avatar" v-bind:src="spuc.login.avatar.path"></a>
               </aside>`,
} );

Vue.component('file', {
    data : function() { return { spuc,
                                 selected_file : '',
                               } },
    methods: {
        ready_file(ev) {
            this.selected_file = ev.target.files[0];
        },
    },
    template : `<div class="row">
                   <input  type="file"   v-on:change="ready_file($event)">
                   <button type="button" v-bind:disabled="selected_file?null:'true'"
                            v-on:click="$emit( 'upload', selected_file)">upload</button>
                </div>`,

} );


Vue.component('login', {
    data : function() { return { spuc, ready : false, login : '', password : '', err : '' } },
    methods : {
        update : function( fld, val ) {
            this[fld] = val;
            this.ready = this.login.match(/\S/) && this.password.match(/\S/);
        },
        try_login : function( fld, val ) {
            if( this.ready )
                spuc.try_login(this.login,this.password)
                .then( li => {
                    this.err = '';
                    this.spuc.msg = 'logged in';
                } )
                .catch( err => this.err = err );
            return this.ready;
        },
        blur : function(ev) {
            ev.target.blur();
        },
    },
    template : `<aside class="col squish center pad">
                  <div class="small"><a href="#reset_request">recover password</a></div>
                  <div v-if="this.err" style="border:solid 3px red">
                      {{ this.err }}
                  </div>
                  <div class="col">
                    <input type="text" required="1"
                             v-on:input="update('login',$event.target.value)"
                             @keyup.enter="ready ? try_login() : blur($event)"
                             placeholder="username or email" />
                    <input type="password" required="1"
                             @keyup.enter="ready ? try_login() : blur($event)"
                             v-on:input="update('password',$event.target.value)"
                             placeholder="password" />
                    <button v-bind:disabled="ready?null:'true'" 
                            v-on:click="try_login()" type="button">Log In</button>
                  </div>
                  <div class="x-large"><a href="#signup">Sign Up</a></div>
                </aside>`,
} ); //login

Vue.component('msg', {
    props : [ 'thing' ],
    data : function() {
        return { spuc };
    },
    template : `<div v-if="thing.msg" 
                    class="col msg" 
                    style="border:solid 1px black" 
            v-bind:styleo="thing.msg ? 'border:4px solid brown' : 'display:none'">
                      {{ thing.msg }}
                </div>`,
} ); //msg

Vue.component('panel-artist-detail', {
    data : function() { return { spuc, panel : this.$attrs.panel, artist : this.$attrs.panel.artist } },
    template : `
`,
} ); //panel-artist-detail

Vue.component('profile', {
    data : function() {
        let start_dirty =  spuc.login.autosave_avatar_is_dirty == 1 && spuc.login.last_autosave_avatar;
        return { spuc, bio : spuc.login.bio,
                 opw: '', pw : '', pw2 : '',
                 deleted_avatars : 0,
                 passwords_ready : false,
                 edit_avatar_src : start_dirty ? spuc.login.last_autosave_avatar.path : undefined,
                 start_dirty,
                 msg : '',
                 err : '' } },
    methods : {
        check_passwords : function(fld,val) {
            this[fld] = val;
            this.err = '';
            this.passwords_ready = false;
            if( (this.pw || this.pw2) && this.pw !== this.pw2 ) {
                this.err = 'passwords do not match';
            } else if( !this.pw || this.pw.length < 7 ) {
                this.err = 'password too short';
            } else if( this.pw === this.pw2 ) {
                this.passwords_ready = true;
            }
        },
        delete_avatar( ev, idx ) {
            ev.preventDefault();
            ev.stopPropagation();
            if( confirm('really delete this avatar?') ) {
                spuc.update_account( { delete_avatar : idx } )
                    .then( e => this.deleted_avatars++ );
            }
        },
        edit_avatar(ev,path) {
            ev.preventDefault();
            ev.stopPropagation();
            this.edit_avatar_src = path;
        },
        recover_avatar(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            spuc.update_account( { recover_avatar : 1 } )
                .then( e => this.deleted_avatars-- );
        },
        reset_password : function() {
            if( this.passwords_ready )
                spuc.reset_password( this.pw, undefined, this.opw )
                .then( m => this.msg = 'updated password' );
        },
        update_account( ev, args ) {
            ev.preventDefault();
            ev.stopPropagation();
            spuc.update_account( args );
        },
        upload_avatar(file) {
            spuc.IO.rpc( 'add_avatar', undefined, file )
                .then( avas => spuc.login.avatars = avas );
        },
        ready_avatar_image(target,imgdata) {
            if( target === 'save avatar' )
                spuc.IO.rpc( 'add_avatar', imgdata )
                   .then( avas => spuc.login.avatars = avas );
            else if( target === 'auto-save' )
                spuc.IO.rpc( 'autosave_avatar', imgdata );
            
        },
    },
    template : `<div>

                  <div class="request standout">set your quote</div>

                  <div class="col" style="margin-bottom:3em;">
                     <textarea cols="50" rows="4"
                            v-bind:value="bio"
                            placeholder="say something about yourself"
                            v-on:input="bio = $event.target.value"
                            v-bind:style="bio===spuc.login.bio ? '' : 'background-color:pink'">{{ bio }}</textarea>
                     <div class="row squish">
                       <button type="button" class="large"
                               v-on:click="spuc.update_account( { bio } )"
                               v-bind:disabled="bio===spuc.login.bio?'true':null">update</button>
                       <button type="button" class="large" 
                               v-on:click="bio=spuc.login.bio"
                               v-bind:disabled="bio===spuc.login.bio?'true':null"
                               >reset</button>
                     </div>
                  </div>

                  <div class="request standout">choose your avatars</div>
                  <div class="col" style="margin:0 auto 3em auto;">
                      <div v-if="deleted_avatars > 0">
                        Avatar was deleted. 
                        <a href="#" class="click" v-on:click="recover_avatar($event)">Recover Avatar</a>
                      </div>
                      <div class="row center">
                        <div class="col" style="margin-right:1em;border:solid gold 3px;padding:3px;">
                           <img class="big-avatar" style="border:solid black 1px;" title="default avatar" v-bind:src="spuc.login.avatar.path">
                           Default Avatar
                        </div>
                        <div v-bind:class="'col ' + (spuc.login.avatars.length > 0 ? '' : 'hide')"
                             style="text-align:center;border:solid black 3px;padding: 3px;">
                           <div class="row">
                              <div class="col" v-for="(a,idx) in spuc.login.avatars">
                                <img class="big-avatar"  v-bind:src="a.path" 
                                     v-bind:style="a.path == spuc.login.avatar.path ? 'border:solid black 5px;margin:0 5px;' : 'border:solid black 1px;margin:0 5px;'" v-on:click="spuc.update_account( { avatar : idx } )">
                                <div class="row squish">
                                  <a href="#" v-if="a.path != spuc.login.avatar.path" v-on:click="update_account( $event, { avatar : idx } )">use</a>
                                  <a href="#" v-on:click="edit_avatar( $event, a.path )">edit</a>
                                  <a href="#" v-on:click="delete_avatar( $event, idx )">delete</a>
                                </div>
                              </div>
                           </div>
                           <div>Click to pick default avatar</div>
                        </div>
                      </div>

                     <div class="row squish standout">
                       <div class="request col">
                          <span>draw an avatar</span>
                          <div class="ent">&dArr;</div>
                       </div>
                       <div class="request">
                          OR
                       </div>
                       <div class="col request">
                          <span>upload avatar</span>
                          <file v-on:upload="upload_avatar"></file>
                       </div>
                     </div>

                     <div>
                      <div class="row">
                           <imed v-bind:width="150" 
                                v-bind:height="150"
                                   v-on:ready="ready_avatar_image"
                                v-bind:img_src="edit_avatar_src"
                                v-bind:cscale="2"
                                v-bind:startdirty="start_dirty"
                                v-bind:buttons="['save avatar']"
                                  ></imed>
                      </div>
                     </div>
                  </div>


                  <div class="request standout">update your password</div>
                  <msg v-bind:thing="this"/>
                  <err v-bind:thing="this"/>
                  <div class="row">
                     <div class="col">
                       <input id="pw" type="password" required="1"
                              placeholder="old password"
                              v-bind:class="{ err : this.err.match(/wrong/) }"
                              v-on:input="check_passwords('opw',$event.target.value)"
                              />

                       <input id="pw" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password (at least 7 characters)"
                              v-bind:class="{ err : this.err.match(/too/) }"
                              v-on:input="check_passwords('pw',$event.target.value)"
                              v-on:change="reset_password()" />

                       <input id="pw2" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password repeat"
                              v-bind:class="{ err : this.err.match(/too/) }"
                              v-on:input="check_passwords('pw2',$event.target.value)"
                              v-on:change="reset_password()" />
                       <button v-bind:disabled="passwords_ready?null:'true'" v-on:click="reset_password()" type="button">Reset</button>
                      </div>
                   </div>
                </div>`
} ); //profile


Vue.component('recover-err', {
    data : function() { return { spuc, err : 'bad or expired token' } },
    template : `<div class="col">
                  <h2>Recover Account</h2>
                  <div>
                     <err v-bind:thing="this"/>
                     <a href="#comics">return to site</a>
                  </div>
                </div>`,
} );

Vue.component('recover', {
    data : function() { return { spuc, pw : '', pw2 : '', ready : false, err : '', complete : false } },
    methods : {
        check_passwords : function(fld,val) {
            this[fld] = val;
            this.ready = false;
            this.err = '';
            if( this.pw != this.pw2 && (this.pw && this.pw2) ) {
                this.err = "passwords don't match";
            }
            else if( this.pw && ! this.pw.match( /\S{7}\S*/ ) ) {
                this.err = "password too short";
            }
            else if( this.pw && this.pw2) {
                this.ready = true;
            }
        },
        reset_password() {
            if( this.ready )
                this.spuc.reset_password( this.pw, this.spuc.token )
                .then( e => {
                    this.complete = true;
                    this.err = '';
                } )
                .catch( e => this.err = e );
        },
    },
    template : `<div class="col">
                  <h2>Recover Account</h2>
                  <div>
                    <h3>Reset Password</h3>
                    <div v-if="complete" class="col">
                       Your password is reset and you are logged in. <a href="#comics">Look at some comics</a>.
                    </div>
                    <div v-else class="col">
                       <div v-if="err">
                          <err v-bind:thing="this"/>
                          <a href="#comics">return to site</a>
                       </div>
                       <input id="pw" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password (at least 7 characters)"
                              v-bind:class="{ err : this.err.match(/password/) }"
                              v-on:input="check_passwords('pw',$event.target.value)"
                              v-on:change="reset_password()" />

                       <input id="pw2" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password repeat"
                              v-bind:class="{ err : this.err.match(/password/) }"
                              v-on:input="check_passwords('pw2',$event.target.value)"
                              v-on:change="reset_password()" />
                       <button v-bind:disabled="ready?null:'true'" v-
                               on:click="reset_password()" type="button">Reset</button>
                  </div>
                </div>
              </div>`,
} ); //recover

Vue.component('reset-request', {
    data : function() { return { spuc, ready : false, email : '', msg : '' } },
    methods : {
        checklink : function(txt) {
            this.email = txt.trim();
            this.ready = txt.match( /^\S+\@\S+\.\S+$/ ) ? true : false;
        },
        send_reset : function() {
            spuc.send_reset_request( this.email ).then( msg => {
                this.msg = 'sent';
            } );
        }
    },
    template : `<div class="row squish">
                  <div class="col">
                    <div class="request">Request Password Reset Link</div>
                    <div v-if="msg" class="col msg center">
                      Request reset sent. Be sure to check your spam folder.
                      <a href="#comics">back to SPUC</a>
                    </div>
                    <div v-else class="row standout spread">
                        <input class="large" type="text"
                               v-on:input="checklink($event.target.value)"
                               @keyup.enter="send_reset()"
                               placeholder="email">
                        <button class="large" v-bind:disabled="!ready" v-on:click="send_reset()"
                                type="button">
                            send request
                        </button>
                    </div>
                  </div>
                </div>`,
} ); //reset-request

Vue.component('signup', {
    data : function() { return { spuc,
                                 ready : false,
                                 err : '',
                                 login : '',
                                 email : '',
                                 password : '',
                                 p2 : '' }
                      },
    methods : {
        set_signup : function( k, v ) {
            this[k] = v;
            return this.check_signup();
        },
        check_signup : function() {
            this.ready = false;
            this.err = '';
            if( this.password != this.p2 && (this.password && this.p2) ) {
                this.err = "passwords don't match";
            }
            else if( this.email && ! this.email.match( /^[^@]+@[a-zA-Z]+\.[a-zA-Z.]*[a-zA-Z]+$/ ) ) {
                this.err = "invalid looking email";
            }
            else if( this.password && ! this.password.match( /\S{7}\S*/ ) ) {
                this.err = "password too short";
            }
            else if( this.login && ! this.login.match( /\S/ ) ) {
                this.err = "login too short";
            }
            else if( this.login && this.email ) {
                spuc.IO.rpc('check_login_and_email',{ login: this.login,
                                                   email: this.email } )
                    .then( e => this.ready = true )
                    .catch( msg => { this.err = msg; this.ready = false; } );
            }
            else if( this.login && this.email && this.password && this.p2 ) {
                return false;
            }
            else {
                this.ready = true;
                return true;
            }
            return false;
        },
        signup() {
            if( ! this.ready )
                return false;
            
            return spuc.signup( this.email, this.login, this.password )
                .catch( e => this.err = e );
        },
        blur : function(ev) {
            ev.target.blur();
        },
    },
    template : `<div class="row squish">
                  <div class="col">
                   <div class="request">Make an Artist Account</div>
                   <err v-bind:thing="this"/>
                   <div class="col">
                    <input type="email" required="1"
                         pattern="^[^@]+@[a-zA-Z]+\\.[a-zA-Z.]*[a-zA-Z]+$"
                         placeholder="email"
                         v-bind:class="{ err : this.err.match(/email/) }"
                         @keyup.enter="signup() || blur($event)"
                         v-on:input="set_signup('email',$event.target.value)" />

                    <input type="text" required="1"  placeholder="username"
                         v-bind:class="{ err : this.err.match(/login/) }"
                         @keyup.enter="signup() || blur($event)"
                         v-on:input="set_signup('login',$event.target.value)" />

                    <input id="pw" type="password" required="1" pattern="\\S{7}\\S*"
                         placeholder="password (at least 7 characters)"
                         v-bind:class="{ err : this.err.match(/password/) }"
                         @keyup.enter="signup() || blur($event)"
                         v-on:input="set_signup('password',$event.target.value)" />

                    <input id="pw2" type="password" required="1" pattern="\\S{7}\\S*"
                         placeholder="password repeat"
                         v-bind:class="{ err : this.err.match(/password/) }"
                         @keyup.enter="signup() || blur($event)"
                         v-on:input="set_signup('p2',$event.target.value)" />
                  </div>
                    <button type="button"
                            v-on:click="signup"
                            v-bind:class="this.ready?'valid' : ''"
                            v-bind:disabled="this.ready?null:'true'">Sign Up</button>
                    <a href="#comics">cancel</a>
                 </div>
                </div>`,
} ); //signup

Vue.component('start', {
    data : function() { return { spuc,
                                 IO,
                                 start : '',
                                 msg   : '',
                                 ready : false, } },
    methods : {
        start_comic : function() {
            if( this.ready )
                this.IO.rpc( 'start_comic', this.start )
                .then( comic => {
                    this.msg = 'started comic with the caption "' + comic.panels[0].caption + '"';
                } );
        },
        update : function(val) {
            this.start = val;
            this.ready = this.start.match( /\S/);
        },
    },
    template : `<div v-if="spuc.login" class="col">
                  <msg v-bind:thing="spuc"/>
                  <div v-if="msg">
                    <msg v-bind:thing="this"/>
                    <a href="#comics">Back to Comics</a>
                  </div>
                  <div v-else>

                    <div class="request standout">Start a New Comic</div>
                    <div class="row">
                     <input class="x-large" 
                            type="text" 
                            @keyup.enter="start_comic()" 
                            v-on:input="update($event.target.value)"
                            placeholder="starting caption" 
                            size="70">
                     <button class="x-large" type="button" v-bind:disabled="ready ? null : 'true'" v-on:click="start_comic()">Start the Comic</button>
                    </div>
                  </div>
                </div>`
} ); //start

Vue.component('play', {
    data : function() {
        return { spuc, IO, caption : undefined, ready : false, msg : '', command : undefined }
    },
    methods : {
        caption_comic : function() {
            if( this.ready ) {
                this.IO.rpc( 'caption_comic', { comic : spuc.last_play, caption : this.caption } )
                    .then( is_finished => {
                        this.msg = 'Added Caption';
                        this.caption = '';
                        spuc.last_picture = undefined;
                        spuc.last_picture_autosave = undefined;
                        if( is_finished === '1') {
                            this.msg = is_finished + 'Added Caption and finished comic!';
                            spuc.comics_paginator.reset();
                        }
                        spuc.show_play();
                    } );
            }
        },
        pick_again : function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
	    if( confirm( "really skip this panel?" ) ) {
		this.IO.rpc( 'reset_playing', 1 )
		    .then( e => spuc.show_play(true) );
	    }
        },
        upload_panel : function( file ) {
            this.IO.rpc( 'use_panel', { comic : spuc.last_play }, [file] )
                .then( e => spuc.show_play );
        },
        image_ready : function( btn, image ) {
            if( btn === 'use this picture' ) {
                if( confirm( 'is this picture is ready?' ) ) {
                    this.IO.rpc( 'use_panel', { comic : spuc.last_play, imgdata : image  } )
                        .then( e => {
                            this.msg = 'Added Picture';
                            this.command = 'clear';
                            let that = this;
                            Vue.nextTick( function() { that.command = '' } );
                            
                            spuc.last_picture = undefined;
                            spuc.last_picture_autosave = undefined;
                            spuc.show_play();
                        } );
                }
            }
            else if( btn === 'save for later' ) {
                this.IO.rpc( 'save_panel', image )
                    .then( e => {
                        this.msg = 'Saved Picture';
                    } );
            }
            else if( btn === 'auto-save' ) {
                this.IO.rpc( 'autosave_panel', image );
            }
        },
        select_autosaved_picture : function() {
            this.IO.rpc('select_autosaved_picture')
                .then( e => spuc.show_play() );
        },
        select_saved_picture : function() {
            this.IO.rpc('select_saved_picture')
                .then( e => spuc.show_play() );
        },
        update : function( txt ) {
            this.caption = txt;
            this.ready = this.caption.match( /\S/ );
        },
    },
    template : `<div v-if="spuc.login" class="col">
                   <msg v-bind:thing="this"/>
                   <div class="large">Panel {{ spuc.last_play.panels.length }} of 9</div>
                   <div v-if="spuc.last_play.panels[spuc.last_play.panels.length-1].type === 'caption'" class="col">
                      <div class="ask-caption">
                            &ldquo; {{ spuc.last_play.panels[spuc.last_play.panels.length-1].caption }} &rdquo;</div> 

                      <div v-if="spuc.last_picture && spuc.last_picture_autosave" class="col">
                          <div class="request">Choose saved or autosaved picture?</div>
                          <div class="row squish">
                            <div class="col">
                              <img class="panel detail" v-bind:src="spuc.last_picture.path"> 
                              <div class="row" style="margin:0 auto"><button class="large" type="button" v-on:click="select_saved_picture">select save</button></div>
                            </div>
                            <div class="col">
                              <img class="panel detail" v-bind:src="spuc.last_picture_autosave.path">
                              <div class="row" style="margin:0 auto"><button class="large" type="button" v-on:click="select_autosaved_picture">select autosave</button></div>
                            </div>
                          </div>
                      </div>                      
                      <div v-else>
                         <div class="row squish standout">
                           <div class="request col">
                              <span>Draw a picture for this caption</span>
                              <div class="ent">&dArr;</div>
                           </div>
                           <div class="request">
                              OR
                           </div>
                           <div class="col request">
                              <span>Upload a picture</span>
                              <file v-on:upload="upload_panel"></file>
                           </div>
                           <div class="request">
                              OR
                           </div>
			   <div class="col request">
	                     <a href="#" v-on:click="pick_again($event)">pick again</a>
                           </div>
                         </div>
                         <div class="row" id="comic-edit" style="margin:0 auto;">
                              <imed v-bind:width="900" 
                                   v-bind:height="630"
                                      v-on:ready="image_ready"
                                   v-bind:command="command"
                                   v-bind:img_src="spuc.last_picture ? spuc.last_picture.path : undefined"
                                   v-bind:cscale="1"
                                  v-bind:buttons="['use this picture','save for later']"
                                     ></imed>
                         </div>
                      </div>
                   </div>
                   <div v-else>
                      <div class="col">
                         <img class="picture captioning"
                              v-bind:src="spuc.last_play.panels[spuc.last_play.panels.length-1].picture.path">
                      </div>
                      <input class="x-large" type="text" 
                             @keyup.enter="caption_comic()" 
                             v-bind:value="caption"
                             v-on:input="update($event.target.value)" 
                             placeholder="caption this picture"
                             size="70">
                      <button class="x-large" type="button" v-bind:disabled="ready ? null : 'true'" v-on:click="caption_comic">
                         Add Caption
                      </button>
                   </div>
                   <a href="#" v-on:click="pick_again($event)">pick again</a>
                </div>`
} ); //play

// --------------------- Admin UI -----

Vue.component('logs', {
    data : function() {
        return { spuc };
    },
    methods : {
        prev : function(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            spuc.log_paginator.prev();
        },
        next : function (ev) {
            ev.preventDefault();
            ev.stopPropagation();
            spuc.log_paginator.next();
        },
    },
    template : `<div class="col">
                     <div class="row">
                       <a v-if="spuc.log_paginator.has_prev()"
                          href="#"
                          v-on:click="prev">prev &lt;</a>
                       <div v-else>&lt; prev</div>
                       <div class="large">Logs</div>
                       <a v-if="spuc.log_paginator.has_next()"
                          href="#"
                          v-on:click="next">next &gt;</a>
                       <div v-else>next &gt;</div>
                     </div>
{{ spuc.log_paginator.window.length }}
                     <div v-for="l in spuc.log_paginator.window">{{l}}</div>
                </div>`,
} ); //logs


// --------------------- TOP LEVEL UI

new Vue({
    el : '#main',
    data : function() { return { spuc } },
    template : `<main class="main col">
                   <div style="min-height:3em"><div class="row center" id="loading" v-bind:class="spuc.IO.loading > 0 ? 'loading' : ''">...Loading</div></div>
                   <err v-bind:thing="spuc"/>
                   <msg v-bind:thing="spuc"/>
                   <artist v-if="spuc.showing==='artist'"></artist>
                   <signup v-else-if="spuc.showing==='signup'"></signup>
                   <artists v-else-if="spuc.showing==='artists'"></artists>
                   <reset-request v-else-if="spuc.showing==='reset-request'"></reset-request>
                   <recover v-else-if="spuc.showing==='recover'"></recover>
                   <recover-err v-else-if="spuc.showing==='recover-err'"></recover-err>
                   <play v-else-if="spuc.showing==='play'"></play>
                   <profile v-else-if="spuc.showing==='profile'"></profile>
                   <start v-else-if="spuc.showing==='start'"></start>
                   <logs v-else-if="spuc.showing==='logs'"></logs>
                   <comics v-else></comics>
                </main>
                `,
});

new Vue({
    el : '#header',
    data : function() { return { spuc, IO } },
    template : `<header class="col">
                  <div class="row spread">
                    <a class="col grow splash xx-large" href="#comics">
                     <p>Scarf Poutine You Clone</p>
                     <p>Collaborative Comics</p>
                    </a>
                    <logged-in v-if="spuc.login"></logged-in>
                    <login v-else-if="! IO.sess_id && spuc.showing != 'signup'"></login>
                  </div>
                </header>
               `,
});

new Vue({
    el : '#navbar',
    data : function() { return { spuc } },
    methods : {
        nav : function(ev,dest) {
            ev.preventDefault();
            ev.stopPropagation();
            if( typeof dest === 'function' ) {
                dest();
            } else {
                window.location.hash = dest;
            }
        }
    },
    template : `<nav class="row navbar squish">
                   <a class="navbut" 
                        v-bind:href="'#'+( n[1] && 'function' !== typeof n[1] ? n[1] : n[0])"
                        v-on:click="nav( $event, n[1] ? n[1] : n[0] )"
                        v-for="n in spuc.public_nav_list">
                      {{ n[0] }}
                   </a>
                   <a class="navbut" 
                        v-if="spuc.login"
                        v-bind:href="'#'+( n[1] && 'function' !== typeof n[1] ? n[1] : n[0])"
                        v-on:click="nav( $event, n[1] ? n[1] : n[0] )"
                        v-for="n in spuc.logged_in_nav_list">
                      {{ n[0] }}
                   </a>
                   <a class="navbut" 
                        v-if="spuc.login && spuc.login.admin"
                        v-bind:href="'#'+( n[1] && 'function' !== typeof n[1] ? n[1] : n[0])"
                        v-on:click="nav( $event, n[1] ? n[1] : n[0] )"
                        v-for="n in spuc.admin_nav_list">
                      {{ n[0] }}
                   </a>
                </nav>
               `,
});
}
