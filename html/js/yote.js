class YoteObj {
    constructor( yoteapp, appname, def, data, id ) {
        this.yoteapp = yoteapp;
        this.appname = appname;
        this.data = {};
        this.id = id;
        if( def ) 
            def.forEach( mthd => this[ mthd ] = this._callMethod.bind( this, mthd ) );
    } //constructor
    
    _callMethod( mthd, args, files ) {
        return this.yoteapp.act( mthd, args, files );
    }
    get( fld ) {
        let fval = this.data[fld];
        let t = fval.substring( 0, 1 );
        let val = undefined;
        if( t === 'v' )
            val = fval.substring( 1 );
        else if( t === 'r' )
            return this.yoteapp.act( {
                appname : this.yoteapp.appname,
                action : mthd,
                args : fval.substring( 1 ),
            } );

        return new Promise((resolve, reject) => {        
            resolve( val );
        } );
    } //get
    update( newdata ) {
        Object.keys( this.data ).forEach( k => k in newdata ? undefined : delete this.data[k] );
        Object.keys( newdata ).forEach( k => this.data[k] = newdata[k] );
    } //update
} //YoteObj

class Yote {
    constructor( args ) {
        args = args || {};
        this.endpoint = args.endpoint || '/cgi-bin/adapter.cgi'; // or '/app'
        this.cache = {};
    }
} //Yote

class YoteApp {
    constructor( appname, args ) {
        this.sess_id = undefined; 
        this.appname = appname;
        
        this.cache = {};
        
        args = args || {};
        this.endpoint = args.endpoint || '/cgi-bin/adapter.cgi'; // or '/app'

        this.appobj = undefined;
    } //constructor

    fetch_app() {
        return new Promise( (resolve,reject) => {
            if( this.appobj !== undefined ) {
                resolve( this.appobj );
            } else {
                this.act( 'fetch' )
                    .then( yap => {
                        this.appobj = yap;
                        resolve( yap );
                    } );
            }
        } );
    } //fetch_app
    
    
    translate_return_value( retval ) {
        if( Array.isArray( retval ) ) {
            return retval.map( rv => this.translate_return_value( rv ) );
        }
        else if( typeof retval === 'object' ) {
            let ret = {};
            Object.keys( retval ).forEach( k => ret[k] = rv => this.translate_return_value( retval[k] ) );
            return ret;
        }
        let t = retval.substring(0,1);
        let v = retval.substring(1);
        if( t === 'r' )
            return this.cache[v];
        else if( t === 'u' )
            return undefined;
        return v;
    } //translate_return_value

    translate_args( args ) {
        if( typeof args === 'object' ) {
            if( args.id !== undefined )
                return 'r' + args.id;
            if( Array.isArray( args ) ) {
                return args.map( el => this.translate_args( el ) );
            }
            let ret = {};
            Object.keys( args ).forEach( k => ret[k] = this.translate_args( args[k] ) );
            return ret;
        }
        if( args === undefined )
            return 'u';
        return 'v' + args;
    } //translate_args
    
    act( action, args, files ) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.responseType = 'json';
            console.log( "REUSE XHR" );
            xhr.open( 'POST', this.endpoint );
            xhr.onload = () => {
                if( xhr.status === 200 ) {
                    if( xhr.response.succ ) {
                        // xhr response succ, data, error, ret
                        let retv = xhr.response.ret;
                        let data = xhr.response.data;
                        let defs = xhr.response.defs;
                        
                        console.log( xhr.response, "RECEIVED" );
                        
                        // make or refresh objects based on this
                        Object.keys( data ).forEach( id => {
                            if( this.cache[id] ) {
                                this.cache[id].update( data );
                            } else {
                                this.cache[id] = new YoteObj( this, this.appname, defs[data[id]._cls], data, id );
                            }
                        } );
                        console.log( data, this.cache, "CACHE" );
                        let payload = this.translate_return_value( retv );
                        console.log( retv, payload, "PAYL" );
                        resolve(payload);
                    }
                    else
                        reject(xhr.response.error);
                } else {
                    reject('unknown');
                }
            }
            xhr.onerror = () => reject(xhr.statusText);
            
            let fd = new FormData();

            let converted_args = this.translate_args( args );
            
            let payload = {
                action,
                args    : converted_args,
                app     : this.appname,
                sess_id : this.sess_id,
            };
            console.log( "PAYLOAD", JSON.stringify(payload) );
            fd.append( 'payload', JSON.stringify(payload) );
            if( files ) {
                fd.append( 'files', files.length );
                for( let i=0; i<files.length; i++ )
                    fd.append( 'file_' + i, files[i] );
            }
            xhr.send(fd);
        } );
    };
} //class YoteApp

let yoteApps = {};

const fetchYoteApp = (appname,args) => {
    let app = yoteApps[appname] = yoteApps[appname] || new YoteApp(appname,args);
    return app.fetch_app();
};

export { fetchYoteApp };
