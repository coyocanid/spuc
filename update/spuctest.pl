#/usr/bin/perl

use strict;
use warnings;

# comment out for write
#use lib '/home/coyo/proj/spuc/lib';
use lib '/home/coyo/proj/recordstore/lib';
use lib '/home/coyo/proj/objectstore/lib';

use Data::RecordStore;
use Data::ObjectStore;
use JSON;

my $make_dump = 0;
my $write_dump = 1;
my $check_dump = 0;

if ($make_dump == 0) {
    if ( $check_dump ) {
	# my $os = Data::ObjectStore->open_store( "./spuc" );
	# my $root = $os->load_root_container;
	# print STDERR Data::Dumper->Dump([$root,"ROO"]);
	my $os = Data::ObjectStore->activate_store( "./spuc2" );
	my $root = $os->fetch_root;
	print STDERR Data::Dumper->Dump([$root,"ROO"]);
    }
    elsif( $write_dump ) {
	write_db();
    }
    exit;
}

sub write_db {
    my $rs = Data::RecordStore->activate_store( "./spuc2" );
    print STDERR "CREATED\n";
    open my $in, '<', 'spuc_json_dump.txt';
    while (my $id = <$in>) {
        chomp $id;
        if ($id == 2) {
            $id = 1;
        }
	print STDERR "GETTING $id\n";
        my $json = <$in>;
        chomp $json;
        my $data = from_json( $json );
        my $class = $data->{class};
        my $vals = $data->{values};
        my @parts = ( $class );
        if ($class eq 'Data::ObjectStore::Array') {
            for my $val (@$vals) {
                if ($val =~ /^\d+$/) {
                    $val = "r$val";
                }
                push @parts, $val;
            }
        } else {
            for my $key (keys %$vals) {
                my $val = $vals->{$key};
                if ($val =~ /^\d+$/) {
                    $val = "r$val";
                }
                push @parts, $key, $val;
            }
        }
        
        my @lengths = map { do { use bytes; length($_) } } @parts;
        my $template = "I".(@parts+1) . join( '', map { "(a$_)" } @lengths );
        $data = pack $template, scalar(@parts), @lengths, @parts;
        
        while( $rs->next_id < $id ) {}
        
        $rs->stow( $data, $id );
    }
    my $os = Data::ObjectStore->activate_store( "./spuc2" );
    print STDERR Data::Dumper->Dump([$os->fetch_root,"ROO"]);
}

my( %seen );

# gotta open it with the correct version
my $rs = Data::RecordStore->open_store( "/var/www/data/spuc/RECORDSTORE" );

get( 1 );
my $count = 0;
sub get {
    my $id = shift;
    return if $seen{$id}++;
    my $data = $rs->fetch($id);
    my ($cls,$post) = ($data =~ /^([^|]+)[^ ]* (.*)/);

    my @parts = split( /\`/, $post );
    if ($post =~ /\\`/ ) {
        @parts = unescape_parts( $post );
    }
    
    #    return if $count++ > 10;

    print "$id\n";
    

    if ($cls eq 'Data::ObjectStore::Array') {
        my ($level,$block_count,$item_count,$underneath) = splice @parts, 0, 4;
        print to_json( {
            class       => $cls,
            values      => \@parts,
            block_count => $block_count,
            item_count  => $item_count,
            underneath  => $underneath,
            level       => $level,
                       } )."\n";
        while (@parts) {
            my $val = shift @parts;
            if ($val =~ /^r?(\d+)$/) {
                get( $1 );
            }
        }
        
    } elsif ($cls eq 'Data::ObjectStore::Hash') {
        my ($level,$buckets,$size) = splice @parts, 0, 3;

        if (@parts / 2 != int(@parts/2)) {
            die "BLAGH $data\n";
        }
        my %parts = @parts;

        print to_json( {
            class   => $cls,
            values  => \%parts,
            size    => $size,
            buckets => $buckets,
            level   => $level,
                       } )."\n";

        my @vals = values %parts;
        while (@vals) {
            my $val = shift @vals;
            if ($val =~ /^r?(\d+)$/) {
                get( $1 );
            }
        }
    }
    else {
        my %parts = @parts;

        print to_json( {
            class   => $cls,
            values  => \%parts,
                       } )."\n";
        
        my @vals = values %parts;
        while (@vals) {
            my $val = shift @vals;
            if ($val =~ /^r?(\d+)$/) {
                get( $1 );
            }
        }
    }
} #get

sub unescape_parts {
    my $post = shift;

    my $len = length($post);
    my $pos = 0;
    my $buf = '';
    my @parts;
    my $escaped;
    while( $pos < $len ) {
        my $char = substr($post,$pos,1);
        if ($escaped) {
            $buf .= $char;
            $escaped = 0;
        }
        elsif ($char eq '\\' ) {
            $escaped = 1;
        }
        elsif ( $char eq '`') {
            push @parts, $buf;
            $buf = '';
        }
        else {
            $buf .= $char;
        }
        $pos++;
    }
    push @parts, $buf;
    return @parts;
    
} #unescape_parts

