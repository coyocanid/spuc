function noop(){
}

function destroyable(proc, container, onEmpty){
	let result = {proc: proc};
	if(container){
		result.destroy = () => {
			result.destroy = result.proc = noop;
			let index = container.indexOf(result);
			if(index !== -1){
				container.splice(index, 1);
			}
			!container.length && onEmpty && onEmpty();
		};
		container.push(result);
	}else{
		result.destroy = () => {
			result.destroy = result.proc = noop;
		};
	}

	return result;
}

function destroyAll(container){
	for(let i = 0, end = container.length; i < end && container.length; i++){
		container.pop().destroy();
	}
}

const listenerCatalog = new WeakMap();

function eventHub(superClass){
	return class extends (superClass || class{
	}){
		// protected interface...
		bdNotify(e){
			let events = listenerCatalog.get(this);
			if(!events){
				return;
			}

			let handlers;
			if(e instanceof Event){
				handlers = events[e.type];
			}else{
				if(e.type){
					handlers = events[e.type];
					e.target = this;
				}else if(!e.name){
					handlers = events[e];
					e = {type: e, name: e, target: this};
				}else{
					// eslint-disable-next-line no-console
					console.warn("event.name is deprecated; use event.type");
					handlers = events[e.name];
					e.type = e.name;
					e.target = this;
				}
			}

			if(handlers){
				handlers.slice().forEach(destroyable => destroyable.proc(e));
			}
		}

		// public interface...
		get isBdEventHub(){
			return true;
		}

		advise(eventName, handler){
			if(!handler){
				let hash = eventName;
				return Reflect.ownKeys(hash).map(key => this.advise(key, hash[key]));
			}else if(Array.isArray(eventName)){
				return eventName.map(name => this.advise(name, handler));
			}else{
				let events = listenerCatalog.get(this);
				if(!events){
					listenerCatalog.set(this, (events = {}));
				}
				let result = destroyable(handler, events[eventName] || (events[eventName] = []));
				this.own && this.own(result);
				return result;
			}
		}

		destroyAdvise(eventName){
			let events = listenerCatalog.get(this);
			if(!events){
				return;
			}
			if(eventName){
				let handlers = events[eventName];
				if(handlers){
					handlers.forEach(h => h.destroy());
					delete events[eventName];
				}
			}else{
				Reflect.ownKeys(events).forEach(eventName => {
					events[eventName].forEach(h => h.destroy());
				});
				listenerCatalog.delete(this);
			}
		}
	};
}

const EventHub = eventHub();

let eqlComparators = new Map();

function eql(refValue, otherValue){
	if(!refValue){
		return otherValue === refValue;
	}
	if(refValue instanceof Object){
		let comparator = eqlComparators.get(refValue.constructor);
		if(comparator){
			return comparator(refValue, otherValue);
		}
	}
	if(otherValue instanceof Object){
		let comparator = eqlComparators.get(otherValue.constructor);
		if(comparator){
			return comparator(otherValue, refValue);
		}
	}
	return refValue === otherValue;
}

const watcherCatalog = new WeakMap();
const STAR = Symbol("bd-star");
const OWNER = Symbol("bd-owner");
const OWNER_NULL = Symbol("bd-owner-null");
const PROP = Symbol("bd-prop");
const UNKNOWN_OLD_VALUE = {
	value: "UNKNOWN_OLD_VALUE"
};

const pWatchableWatchers = Symbol("bd-pWatchableWatchers");
const pWatchableHandles = Symbol("bd-pWatchableHandles");
const pWatchableSetup = Symbol("bd-pWatchableSetup");

class WatchableRef{
	constructor(referenceObject, referenceProp, formatter){
		if(typeof referenceProp === "function"){
			// no referenceProp,...star watcher
			formatter = referenceProp;
			referenceProp = STAR;
		}

		Object.defineProperty(this, "value", {
			enumerable: true,
			get: (function (){
				if(formatter){
					if(referenceProp === STAR){
						return () => formatter(referenceObject);
					}else{
						return () => formatter(referenceObject[referenceProp]);
					}
				}else if(referenceProp === STAR){
					return () => referenceObject;
				}else{
					return () => referenceObject[referenceProp];
				}
			})()
		});

		// if (referenceObject[OWNER] && referenceProp === STAR), then we cValue===newValue===referenceObject...
		// therefore can't detect internal mutations to referenceObject, so don't try
		let cannotDetectMutations = referenceProp === STAR && referenceObject[OWNER];

		this[pWatchableWatchers] = [];

		let cValue;
		let callback = (newValue, oldValue, target, referenceProp) => {
			if(formatter){
				oldValue = oldValue === UNKNOWN_OLD_VALUE ? oldValue : formatter(oldValue);
				newValue = formatter(newValue);
			}
			if(cannotDetectMutations || oldValue === UNKNOWN_OLD_VALUE || !eql(cValue, newValue)){
				this[pWatchableWatchers].slice().forEach(destroyable => destroyable.proc((cValue = newValue), oldValue, target, referenceProp));
			}
		};

		this[pWatchableSetup] = function (){
			cValue = this.value;
			if(referenceObject[OWNER]){
				this[pWatchableHandles] = [watch(referenceObject, referenceProp, (newValue, oldValue, receiver, _prop) => {
					if(referenceProp === STAR){
						callback(referenceObject, UNKNOWN_OLD_VALUE, referenceObject, _prop);
					}else{
						callback(newValue, oldValue, referenceObject, _prop);
					}
				})];
			}else if(referenceObject.watch){
				this[pWatchableHandles] = [
					referenceObject.watch(referenceProp, (newValue, oldValue, target) => {
						callback(newValue, oldValue, target, referenceProp);
						if(this[pWatchableHandles].length === 2){
							this[pWatchableHandles].pop().destroy();
						}
						if(newValue && newValue[OWNER]){
							// value is a watchable
							this[pWatchableHandles].push(watch(newValue, (newValue, oldValue, receiver, referenceProp) => {
								callback(receiver, UNKNOWN_OLD_VALUE, referenceObject, referenceProp);
							}));
						}
					})
				];
				let value = referenceObject[referenceProp];
				if(value && value[OWNER]){
					// value is a watchable
					this[pWatchableHandles].push(watch(value, (newValue, oldValue, receiver, referenceProp) => {
						callback(receiver, UNKNOWN_OLD_VALUE, referenceObject, referenceProp);
					}));
				}
				referenceObject.own && referenceObject.own(this);
			}else{
				throw new Error("don't know how to watch referenceObject");
			}
		};
	}

	destroy(){
		destroyAll(this[pWatchableWatchers]);
	}

	watch(watcher){
		this[pWatchableHandles] || this[pWatchableSetup]();
		return destroyable(watcher, this[pWatchableWatchers], () => {
			destroyAll(this[pWatchableHandles]);
			delete this[pWatchableHandles];
		});
	}
}

WatchableRef.pWatchableWatchers = pWatchableWatchers;
WatchableRef.pWatchableHandles = pWatchableHandles;
WatchableRef.pWatchableSetup = pWatchableSetup;
WatchableRef.UNKNOWN_OLD_VALUE = UNKNOWN_OLD_VALUE;
WatchableRef.STAR = STAR;

function getWatchableRef(referenceObject, referenceProp, formatter){
	// (referenceObject, referenceProp, formatter)
	// (referenceObject, referenceProp)
	// (referenceObject, formatter) => (referenceObject, STAR, formatter)
	// (referenceObject) => (referenceObject, STAR)
	if(typeof referenceProp === "function"){
		// no referenceProp,...star watcher
		formatter = referenceProp;
		referenceProp = STAR;
	}
	return new WatchableRef(referenceObject, referenceProp || STAR, formatter);
}

function watch(watchable, name, watcher){
	if(typeof name === "function"){
		watcher = name;
		name = STAR;
	}

	let variables = watcherCatalog.get(watchable);
	if(!variables){
		watcherCatalog.set(watchable, (variables = {}));
	}

	let insWatcher = (name, watcher) => destroyable(watcher, variables[name] || (variables[name] = []));
	if(!watcher){
		let hash = name;
		return Reflect.ownKeys(hash).map(name => insWatcher(name, hash[name]));
	}else if(Array.isArray(name)){
		return name.map(name => insWatcher(name, watcher));
	}else{
		return insWatcher(name, watcher);
	}
}

let holdStarNotifications = false;

function applyWatchers(newValue, oldValue, receiver, name){
	let catalog = watcherCatalog.get(receiver);
	if(catalog){
		if(name === STAR){
			let watchers = catalog[STAR];
			watchers && watchers.slice().forEach(destroyable => destroyable.proc(receiver, oldValue, receiver, [STAR]));
		}else{
			let prop = name[0];
			let watchers = catalog[prop];
			watchers && watchers.slice().forEach(destroyable => destroyable.proc(receiver[prop], oldValue, receiver, name));
			if(!holdStarNotifications){
				(watchers = catalog[STAR]) && watchers.slice().forEach(destroyable => destroyable.proc(receiver, oldValue, receiver, name));
			}
		}
	}
	if(watch.log){
		// eslint-disable-next-line no-console
		console.log(name, newValue);
	}
	if(!holdStarNotifications && receiver[OWNER] !== OWNER_NULL){
		name.unshift(receiver[PROP]);
		applyWatchers(receiver, UNKNOWN_OLD_VALUE, receiver[OWNER], name);
	}
}

let pauseWatchers = false;
let _silentSet = false;

function set(target, prop, value, receiver){
	if(_silentSet){
		target[prop] = value;
		return true;
	}else{
		let oldValue = target[prop];
		if(value instanceof Object){
			let holdPauseWatchers = pauseWatchers;
			try{
				pauseWatchers = true;
				value = createWatchable(value, receiver, prop);
				pauseWatchers = holdPauseWatchers;
			}catch(e){
				pauseWatchers = holdPauseWatchers;
				throw e;
			}
		}
		// we would like to set and applyWatchers iff target[prop] !== value. Unfortunately, sometimes target[prop] === value
		// even though we haven't seen the mutation before, e.g., length in an Array instance
		let result = Reflect.set(target, prop, value, receiver);
		!pauseWatchers && applyWatchers(value, oldValue, receiver, [prop]);
		return result;
	}
}

const watcher = {
	set: set
};

const SWAP_OLD_LENGTH = Symbol("SWAP_OLD_LENGTH");
const OLD_LENGTH = Symbol("old-length");
const NO_CHANGE = Symbol("splice-no-change");
const QUICK_COPY = Symbol("slice-quick-copy");
const BEFORE_ADVICE = Symbol("BEFORE_ADVICE");
const noop$1 = () => {
};

const arrayWatcher = {
	set(target, prop, value, receiver){
		if(prop === "length"){
			let result = Reflect.set(target, prop, value, receiver);
			let oldValue = target[SWAP_OLD_LENGTH](value);
			!pauseWatchers && !_silentSet && applyWatchers(value, oldValue, receiver, ["length"]);
			return result;
		}else{
			return set(target, prop, value, receiver);
		}
	}
};


function getAdvice(owner, method){
	let advice = owner[BEFORE_ADVICE] && owner[BEFORE_ADVICE][method];
	return advice && advice.map(f => f());
}

class WatchableArray extends Array{
	// note: we can make all of these much more efficient, particularly shift and unshift.
	// But, it's probably rare that it will matter, so we'll do it when the need arises
	[SWAP_OLD_LENGTH](newLength){
		let result = this[OLD_LENGTH];
		this[OLD_LENGTH] = newLength;
		return result;
	}

	before(method, proc){
		let beforeAdvice = this[BEFORE_ADVICE];
		if(!beforeAdvice){
			Object.defineProperty(this, BEFORE_ADVICE, {value: {}});
			beforeAdvice = this[BEFORE_ADVICE];
		}
		let stack = beforeAdvice[method] || (beforeAdvice[method] = []);
		stack.push(proc);
		let handle = {
			destroy(){
				handle.destroy = noop$1;
				let index = stack.indexOf(proc);
				if(index !== -1) stack.splice(index, 1);
			}
		};
		return handle;
	}

	_splice(...args){
		let oldValues = this.slice(QUICK_COPY);
		let changeSet = [];
		let result;
		try{
			_silentSet = true;
			result = super.splice(...args);
			for(let i = 0, end = Math.max(oldValues.length, this.length); i < end; i++){
				let value = this[i];
				if(value instanceof Object){
					if(value[OWNER]){
						if(value[OWNER] !== this){
							// a new item that came from another watchable
							value = this[i] = createWatchable(value, this, i);
							changeSet.push(value);
						}else if(value[PROP] != i){ // INTENTIONAL !=
							// an item that was moved within this
							value[PROP] = i;
							changeSet.push(value);
						}else{
							changeSet.push(NO_CHANGE);
						}
					}else{
						// a new item that is not already a watchable
						value = this[i] = createWatchable(value, this, i);
						changeSet.push(value);
					}
				}else if(value !== oldValues[i]){
					changeSet.push(value);
				}else{
					changeSet.push(NO_CHANGE);
				}
			}
			_silentSet = false;
		}catch(e){
			try{
				oldValues.forEach((value, i) => (this[i] = value));
			}catch(e){
				// eslint-disable-next-line no-console
				console.error(e);
			}
			_silentSet = false;
			throw e;
		}
		try{
			holdStarNotifications = true;
			let change = false;
			changeSet.forEach((value, i) => {
				if(value !== NO_CHANGE){
					change = true;
					applyWatchers(value, oldValues[i], this, [i]);
				}
			});
			if(this.length !== oldValues.length){
				change = true;
				applyWatchers(this.length, oldValues.length, this, ["length"]);
			}
			holdStarNotifications = false;
			if(change){
				applyWatchers(this, UNKNOWN_OLD_VALUE, this, STAR);
			}
		}catch(e){
			holdStarNotifications = false;
			throw e;
		}
		return result;
	}

	splice(...args){
		let advice = getAdvice(this, "splice");
		let result = this._splice(...args);
		advice && advice.map(f => f && f(result));
		return result;
	}

	pop(){
		let advice = getAdvice(this, "pop");
		let result = fromWatchable(super.pop());
		advice && advice.map(f => f && f(result));
		return result;
	}

	shift(){
		let advice = getAdvice(this, "shift");
		let result = fromWatchable(this._splice(0, 1)[0]);
		advice && advice.map(f => f && f(result));
		return result;
	}

	slice(...args){
		return args[0] === QUICK_COPY ? super.slice() : fromWatchable(super.slice(...args));
	}

	unshift(...args){
		let advice = getAdvice(this, "unshift");
		this._splice(0, 0, ...args);
		advice && advice.map(f => f && f());
	}

	reverse(){
		let advice = getAdvice(this, "reverse");
		let oldValues = this.slice(QUICK_COPY);
		try{
			_silentSet = true;
			for(let i = 0, j = this.length - 1; i < j; i++, j--){
				let temp = this[i];
				this[i] = this[j];
				this[i][PROP] = i;
				this[j] = temp;
				temp[PROP] = j;
			}
			_silentSet = false;
		}catch(e){
			_silentSet = false;
			throw e;
		}
		try{
			holdStarNotifications = true;
			if(this.length % 2){
				for(let i = 0, end = Math.floor(this.length / 2); i < end; i++){
					applyWatchers(this[i], oldValues[i], this, [i]);
				}
				for(let i = Math.ceil(this.length / 2), end = this.length; i < end; i++){
					applyWatchers(this[i], oldValues[i], this, [i]);
				}
			}else{
				this.forEach((value, i) => applyWatchers(value, oldValues[i], this, [i]));
			}
			holdStarNotifications = false;
			if(this.length > 1){
				applyWatchers(this, UNKNOWN_OLD_VALUE, this, STAR);
			}
		}catch(e){
			holdStarNotifications = false;
			throw e;
		}
		advice && advice.map(f => f && f(this));
		return this;
	}

	_reorder(proc){
		let oldValues = this.slice(QUICK_COPY);
		let changeSet = Array(this.length).fill(false);
		let changes = false;
		try{
			_silentSet = true;
			proc(this);
			this.forEach((value, i) => {
				if(value[PROP] != i){ // INTENTIONAL !=
					value[PROP] = i;
					changeSet[i] = true;
					changes = true;
				}
			});
			_silentSet = false;
		}catch(e){
			_silentSet = false;
			throw e;
		}
		try{
			holdStarNotifications = true;
			changeSet.forEach((value, i) => {
				if(value){
					applyWatchers(this[i], oldValues[i], this, [i]);
				}
			});
			holdStarNotifications = false;
			if(changes){
				applyWatchers(this, UNKNOWN_OLD_VALUE, this, STAR);
			}
		}catch(e){
			holdStarNotifications = false;
			throw e;
		}
	}

	reorder(proc){
		let advice = getAdvice(this, "reorder");
		this._reorder(proc);
		advice && advice.map(f => f && f(this));
		return this;
	}

	sort(...args){
		let advice = getAdvice(this, "sort");
		this._reorder(theArray => super.sort.apply(theArray, args));
		advice && advice.map(f => f && f(this));
		return this;
	}
}

function silentSet(watchable, prop, value, enumerable, configurable){
	try{
		_silentSet = true;
		if(value === undefined){
			delete watchable[prop];
		}else if(enumerable !== undefined && !enumerable && !watchable.hasOwnProperty(prop)){
			Object.defineProperty(watchable, prop, {
				writable: true,
				configurable: configurable !== undefined ? configurable : true,
				value: value
			});
		}else{
			watchable[prop] = value;
		}
		_silentSet = false;
	}catch(e){
		_silentSet = false;
		throw e;
	}
}

function createWatchable(src, owner, prop){
	let keys = Reflect.ownKeys(src);
	let isArray = Array.isArray(src);
	let result = isArray ? new Proxy(new WatchableArray(), arrayWatcher) : new Proxy({}, watcher);
	if(isArray){
		keys.forEach(k => k !== "length" && (result[k] = src[k]));
		Object.defineProperty(result, OLD_LENGTH, {writable: true, value: result.length});
	}else{
		keys.forEach(k => (result[k] = src[k]));
	}

	let silentHold = _silentSet;
	_silentSet = true;
	Object.defineProperty(result, OWNER, {writable: true, value: owner});
	prop !== undefined && Object.defineProperty(result, PROP, {writable: true, value: prop});
	_silentSet = silentHold;
	return result;
}

function toWatchable(data){
	if(!(data instanceof Object)){
		throw new Error("scalar values are not watchable");
	}
	try{
		pauseWatchers = true;
		data = createWatchable(data, OWNER_NULL, false);
		pauseWatchers = false;
		return data;
	}catch(e){
		pauseWatchers = false;
		throw e;
	}
}

function fromWatchable(data){
	if(data instanceof Object){
		if(!data){
			return data;
		}
		if(!data[OWNER]){
			return data;
		}
		let result = Array.isArray(data) ? Array(data.length) : {};
		Reflect.ownKeys(data).forEach(k => {
			if(k !== OWNER && k !== PROP && k != OLD_LENGTH){
				result[k] = fromWatchable(data[k]);
			}
		});
		return result;
	}else{
		return data;
	}
}

let onMutateBeforeNames = {};
let onMutateNames = {};

function mutate(owner, name, privateName, newValue){
	let oldValue = owner[privateName];
	if(eql(oldValue, newValue)){
		return false;
	}else{
		let onMutateBeforeName, onMutateName;
		if(typeof name !== "symbol"){
			onMutateBeforeName = onMutateBeforeNames[name];
			if(!onMutateBeforeName){
				let suffix = name.substring(0, 1).toUpperCase() + name.substring(1);
				onMutateBeforeName = onMutateBeforeNames[name] = "onMutateBefore" + suffix;
				onMutateName = onMutateNames[name] = "onMutate" + suffix;
			}else{
				onMutateName = onMutateNames[name];
			}
		}

		if(onMutateBeforeName && owner[onMutateBeforeName]){
			if(owner[onMutateBeforeName](newValue, oldValue) === false){
				// the proposed mutation is illegal
				return false;
			}
		}
		if(owner.hasOwnProperty(privateName)){
			owner[privateName] = newValue;
		}else{
			// not enumerable or configurable
			Object.defineProperty(owner, privateName, {writable: true, value: newValue});
		}
		onMutateName && owner[onMutateName] && owner[onMutateName](newValue, oldValue);
		return [name, newValue, oldValue];
	}
}

function getWatcher(owner, watcher){
	return typeof watcher === "function" ? watcher : owner[watcher].bind(owner);
}

function watchHub(superClass){
	return class extends (superClass || class{
	}){
		// protected interface...
		bdMutateNotify(name, newValue, oldValue){
			let variables = watcherCatalog.get(this);
			if(!variables){
				return;
			}
			if(Array.isArray(name)){
				// each element in name is either a triple ([name, oldValue, newValue]) or false
				let doStar = false;
				for(const p of name) if(p){
					doStar = true;
					let watchers = variables[p[0]];
					if(watchers){
						newValue = p[1];
						oldValue = p[2];
						watchers.slice().forEach(destroyable => destroyable.proc(newValue, oldValue, this, name));
					}
				}
				if(doStar){
					let watchers = variables[STAR];
					if(watchers){
						watchers.slice().forEach(destroyable => destroyable.proc(this, oldValue, this, name));
					}
				}
			}else{
				let watchers = variables[name];
				if(watchers){
					watchers.slice().forEach(destroyable => destroyable.proc(newValue, oldValue, this, name));
				}
				watchers = variables[STAR];
				if(watchers){
					watchers.slice().forEach(destroyable => destroyable.proc(this, oldValue, this, name));
				}
			}
		}

		bdMutate(name, privateName, newValue){
			if(arguments.length > 3){
				let i = 0;
				let results = [];
				let mutateOccurred = false;
				while(i < arguments.length){
					let mutateResult = mutate(this, arguments[i++], arguments[i++], arguments[i++]);
					mutateOccurred = mutateOccurred || mutateResult;
					results.push(mutateResult);
				}
				if(mutateOccurred){
					this.bdMutateNotify(results);
					return results;
				}
				return false;
			}else{
				let result = mutate(this, name, privateName, newValue);
				if(result){
					this.bdMutateNotify(...result);
					return result;
				}
				return false;
			}
		}

		// public interface...
		get isBdWatchHub(){
			return true;
		}

		watch(...args){
			// possible sigs:
			// 1: name, watcher
			// 2: name[], watcher
			// 3: hash: name -> watcher
			// 4: watchable, name, watcher
			// 5: watchable, name[], watcher
			// 6: watchable, hash: name -> watcher
			// 7: watchable, watcher // STAR watcher

			if(arguments.length === 1){
				// sig 3
				let hash = args[0];
				return Reflect.ownKeys(hash).map(name => this.watch(name, hash[name]));
			}
			if(args[0][OWNER]){
				// sig 4-6
				let result;
				if(arguments.length === 2){
					if(typeof args[1] === "object"){
						// sig 6
						let hash = args[1];
						Reflect.ownKeys(hash).map(name => (hash[name] = getWatcher(this, hash[name])));
						result = watch(args[0], hash);
					}else{
						// sig 7
						result = watch(args[0], STAR, getWatcher(this, args[1]));
					}
				}else{
					// sig 4 or 5
					result = watch(args[0], args[1], getWatcher(this, args[2]));
				}
				this.own && this.own(result);
				return result;
			}
			if(Array.isArray(args[0])){
				// sig 2
				return args[0].map(name => this.watch(name, getWatcher(this, args[1])));
			}
			// sig 1
			let name = args[0];
			let watcher = getWatcher(this, args[1]);
			let variables = watcherCatalog.get(this);
			if(!variables){
				watcherCatalog.set(this, (variables = {}));
			}
			let result = destroyable(watcher, variables[name] || (variables[name] = []));
			this.own && this.own(result);
			return result;
		}

		destroyWatch(name){
			let variables = watcherCatalog.get(this);

			function destroyList(list){
				if(list){
					while(list.length) list.shift().destroy();
				}
			}

			if(variables){
				if(name){
					destroyList(variables[name]);
					delete variables[name];
				}else{
					Reflect.ownKeys(variables).forEach(k => destroyList(variables[k]));
					watcherCatalog.delete(this);
				}
			}
		}

		getWatchableRef(name, formatter){
			let result = new WatchableRef(this, name, formatter);
			this.own && this.own(result);
			return result;
		}
	};
}

const WatchHub = watchHub();

function isWatchable(target){
	return target && target[OWNER];
}

function withWatchables(superClass, ...args){
	let prototype;
	let publicPropNames = [];

	function def(name){
		let pname;
		if(Array.isArray(name)){
			pname = name[1];
			name = name[0];
		}else{
			pname = "_" + name;
		}
		publicPropNames.push(name);
		Object.defineProperty(prototype, name, {
			enumerable: true,
			get: function (){
				return this[pname];
			},
			set: function (value){
				this.bdMutate(name, pname, value);
			}
		});
	}

	function init(owner, kwargs){
		publicPropNames.forEach(name => {
			if(kwargs.hasOwnProperty(name)){
				owner[name] = kwargs[name];
			}
		});
	}

	let result = class extends superClass{
		constructor(kwargs){
			kwargs = kwargs || {};
			super(kwargs);
			init(this, kwargs);
		}
	};
	prototype = result.prototype;
	args.forEach(def);
	return result;
}

function bind(src, srcProp, dest, destProp){
	dest[destProp] = src[srcProp];
	if(src.isBdWatchHub){
		return src.watch(srcProp, newValue => dest[destProp] = newValue);
	}else if(src[OWNER]){
		return watch(srcProp, newValue => dest[destProp] = newValue);
	}else{
		throw new Error("src is not watchable");
	}
}

function biBind(src1, prop1, src2, prop2){
	src2[prop2] = src1[prop1];
	return [bind(src1, prop1, src2, prop2), bind(src2, prop2, src1, prop1)];
}

let postProcessingFuncs = Object.create(null);

function insPostProcessingFunction(name, transform, func){
	if(typeof transform === "string"){
		// transform is an alias for name
		if(!postProcessingFuncs[name]){
			throw Error("cannot alias to a non-existing post-processing function: " + name);
		}
		postProcessingFuncs[transform] = postProcessingFuncs[name];
		return;
	}
	if(arguments.length === 3){
		if(typeof transform !== "function"){
			transform = (prop, value) => prop ? {[prop]: value} : value;
		}
	}else{
		func = transform;
		transform = (prop, value) => value;
	}
	func.bdTransform = transform;
	if(postProcessingFuncs[name]){
		throw Error("duplicate postprocessing function name: " + name);
	}
	postProcessingFuncs[name] = func;
}

function replacePostProcessingFunction(name, func){
	postProcessingFuncs[name] = func;
}

function getPostProcessingFunction(name){
	return postProcessingFuncs[name];
}

function flattenChildren(children){
	// single children can be falsey, single children (of type Element or string), or arrays of single children, arbitrarily deep
	let result = [];

	function flatten_(child){
		if(Array.isArray(child)){
			child.forEach(flatten_);
		}else if(child){
			result.push(child);
		}
	}

	flatten_(children);
	return result;
}

class Element{
	constructor(type, props, ...children){
		if(type instanceof Element){
			// copy constructor
			this.type = type.type;
			type.isComponentType && (this.isComponentType = type.isComponentType);
			type.ctorProps && (this.ctorProps = type.ctorProps);
			type.ppFuncs && (this.ppFuncs = type.ppFuncs);
			type.children && (this.children = type.children);
		}else{
			// type must either be a constructor (a function) or a string; guarantee that as follows...
			if(type instanceof Function){
				this.isComponentType = true;
				this.type = type;
			}else if(type){
				// leave this.isComponentType === undefined
				this.type = Array.isArray(type) ? type : type + "";
			}else{
				throw new Error("type is required");
			}

			// if the second arg is an Object and not an Element or and Array, then it is props...
			if(props){
				if(props instanceof Element || Array.isArray(props)){
					children.unshift(props);
					this.ctorProps = {};
				}else if(props instanceof Object){
					let ctorProps = {};
					let ppFuncs = {};
					let ppFuncCount = 0;
					let match, ppf;
					let setPpFuncs = (ppKey, value) => {
						if(ppFuncs[ppKey]){
							let dest = ppFuncs[ppKey];
							Reflect.ownKeys(value).forEach(k => dest[k] = value[k]);
						}else{
							ppFuncCount++;
							ppFuncs[ppKey] = value;
						}
					};
					Reflect.ownKeys(props).forEach((k) => {
						if((ppf = getPostProcessingFunction(k))){
							let value = ppf.bdTransform(null, props[k]);
							setPpFuncs(k, value);
						}else if((match = k.match(/^([A-Za-z0-9$]+)_(.+)$/)) && (ppf = getPostProcessingFunction(match[1]))){
							let ppKey = match[1];
							let value = ppf.bdTransform(match[2], props[k]);
							setPpFuncs(ppKey, value);
						}else{
							ctorProps[k] = props[k];
						}
					});
					this.ctorProps = Object.freeze(ctorProps);
					if(ppFuncCount){
						this.ppFuncs = Object.freeze(ppFuncs);
					}
				}else{
					children.unshift(props);
					this.ctorProps = {};
				}
			}else{
				this.ctorProps = {};
			}


			let flattenedChildren = flattenChildren(children);
			if(flattenedChildren.length === 1){
				let child = flattenedChildren[0];
				this.children = child instanceof Element ? child : child + "";
			}else if(flattenedChildren.length){
				this.children = flattenedChildren.map(child => (child instanceof Element ? child : child + ""));
				Object.freeze(this.children);
			}// else children.length===0; therefore, no children
		}
		Object.freeze(this);
	}
}

function element(type, props, ...children){
	// make elements without having to use new
	return new Element(type, props, children);
}

"a.abbr.address.area.article.aside.audio.base.bdi.bdo.blockquote.br.button.canvas.caption.cite.code.col.colgroup.data.datalist.dd.del.details.dfn.div.dl.dt.em.embed.fieldset.figcaption.figure.footer.form.h1.head.header.hr.html.i.iframe.img.input.ins.kbd.label.legend.li.link.main.map.mark.meta.meter.nav.noscript.object.ol.optgroup.option.output.p.param.picture.pre.progress.q.rb.rp.rt.rtc.ruby.s.samp.script.section.select.slot.small.source.span.strong.style.sub.summary.sup.table.tbody.td.template.textarea.tfoot.th.thead.time.title.tr.track.u.ul.var.video.wbr".split(".").forEach(tag => {
	element[tag] = function div(props, ...children){
		return new Element(tag, props, children);
	};
});

function div(props, ...children){
	return new Element("div", props, children);
}

const SVG = Object.create(null, {
	toString: {
		value: () => "http://www.w3.org/2000/svg"
	}
});
Object.freeze(SVG);

function svg(type, props, ...children){
	if(typeof type !== "string"){
		children.unshift(props);
		props = type;
		type = "svg";
	}
	return new Element([SVG, type], props, children);
}

let document$1 = 0;
let createNode = 0;
let insertNode = 0;

function initialize(_document, _createNode, _insertNode){
	document$1 = _document;
	Component.createNode = createNode = _createNode;
	Component.insertNode = insertNode = _insertNode;
}

function cleanClassName(s){
	return s.replace(/\s{2,}/g, " ").trim();
}

function conditionClassNameArgs(args){
	return args.reduce((acc, item) => {
		if(item instanceof RegExp){
			acc.push(item);
		}else if(item){
			acc = acc.concat(item.split(" ").map(s => s.trim()).filter(s => !!s));
		}
		return acc;
	}, []);
}

function classValueToRegExp(v, args){
	return v instanceof RegExp ? v : RegExp(" " + v + " ", args);
}

function calcDomClassName(component){
	let staticClassName = component.staticClassName;
	let className = component.bdClassName;
	return staticClassName && className ? (staticClassName + " " + className) : (staticClassName || className);
}

function addChildToDomNode(parent, domNode, child, childIsComponent){
	if(childIsComponent){
		let childDomRoot = child.bdDom.root;
		if(Array.isArray(childDomRoot)){
			childDomRoot.forEach((node) => insertNode(node, domNode));
		}else{
			insertNode(childDomRoot, domNode);
		}
		parent.bdAdopt(child);
	}else{
		insertNode(child, domNode);
	}
}

function validateElements(elements){
	if(Array.isArray(elements)){
		elements.forEach(validateElements);
	}else if(elements.isComponentType){
		throw new Error("Illegal: root element(s) for a Component cannot be Components");
	}
}

function postProcess(ppFuncs, owner, target){
	Reflect.ownKeys(ppFuncs).forEach(ppf => {
		let args = ppFuncs[ppf];
		if(Array.isArray(args)){
			getPostProcessingFunction(ppf)(owner, target, ...args);
		}else{
			getPostProcessingFunction(ppf)(owner, target, args);
		}
	});
}

function noop$2(){
}

function pushHandles(dest, ...handles){
	handles.forEach(h => {
		if(Array.isArray(h)){
			pushHandles(dest, ...h);
		}else if(h){
			let destroy = h.destroy.bind(h);
			h.destroy = function (){
				destroy();
				let index = dest.indexOf(h);
				if(index !== -1){
					dest.splice(index, 1);
				}
				h.destroy = noop$2;
			};
			dest.push(h);
		}
	});
}

const ownedHandlesCatalog = new WeakMap();
const domNodeToComponent = new Map();

class Component extends eventHub(WatchHub){
	constructor(kwargs = {}){
		// notice that this class requires only the per-instance data actually used by its subclass/instance
		super();

		if(!this.constructor.noKwargs){
			this.kwargs = kwargs;
		}

		// id, if provided, is read-only
		if(kwargs.id){
			Object.defineProperty(this, "id", {value: kwargs.id + "", enumerable: true});
		}

		if(kwargs.className){
			Array.isArray(kwargs.className) ? this.addClassName(...kwargs.className) : this.addClassName(kwargs.className);
		}

		if(kwargs.tabIndex !== undefined){
			this.tabIndex = kwargs.tabIndex;
		}

		if(kwargs.title){
			this.title = kwargs.title;
		}

		if(kwargs.disabled || (kwargs.enabled !== undefined && !kwargs.enabled)){
			this.disabled = true;
		}

		if(kwargs.elements){
			if(typeof kwargs.elements === "function"){
				this.bdElements = kwargs.elements;
			}else{
				this.bdElements = () => kwargs.elements;
			}
		}

		if(kwargs.postRender){
			this.postRender = kwargs.postRender;
		}

		if(kwargs.mix){
			Reflect.ownKeys(kwargs.mix).forEach(p => (this[p] = kwargs.mix[p]));
		}

		if(kwargs.callbacks){
			let events = this.constructor.events;
			Reflect.ownKeys(kwargs.callbacks).forEach(key => {
				if(events.indexOf(key) !== -1){
					this.advise(key, kwargs.callbacks[key]);
				}else{
					this.watch(key, kwargs.callbacks[key]);
				}
			});
		}
	}

	destroy(){
		this.unrender();
		let handles = ownedHandlesCatalog.get(this);
		if(handles){
			while(handles.length) handles.shift().destroy();
			ownedHandlesCatalog.delete(this);
		}
		this.destroyWatch();
		this.destroyAdvise();
		delete this.kwargs;
		this.destroyed = true;
	}

	render(
		proc // [function, optional] called after this class's render work is done, called in context of this
	){
		if(!this.bdDom){
			let dom = this.bdDom = this._dom = {};
			let elements = this.bdElements();
			validateElements(elements);
			let root = dom.root = this.constructor.renderElements(this, elements);
			if(Array.isArray(root)){
				root.forEach((node) => domNodeToComponent.set(node, this));
			}else{
				domNodeToComponent.set(root, this);
				if(this.id){
					root.id = this.id;
				}
				this.addClassName(root.getAttribute("class") || "");
				let className = calcDomClassName(this);
				if(className){
					root.setAttribute("class", className);
				}

				if(this.bdDom.tabIndexNode){
					if(this.bdTabIndex === undefined){
						this.bdTabIndex = this.bdDom.tabIndexNode.tabIndex;
					}else{
						this.bdDom.tabIndexNode.tabIndex = this.bdTabIndex;
					}
				}else if(this.bdTabIndex !== undefined){
					(this.bdDom.tabIndexNode || this.bdDom.root).tabIndex = this.bdTabIndex;
				}
				if(this.bdTitle !== undefined){
					(this.bdDom.titleNode || this.bdDom.root).title = this.bdTitle;
				}

				this[this.bdDisabled ? "addClassName" : "removeClassName"]("bd-disabled");
			}
			this.ownWhileRendered(this.postRender());
			proc && proc.call(this);
			this.bdMutateNotify("rendered", true, false);
		}
		return this.bdDom.root;
	}

	postRender(){
		// no-op
	}

	bdElements(){
		return new Element("div", {});
	}

	unrender(){
		if(this.rendered){
			if(this.bdParent){
				this.bdParent.delChild(this, true);
			}

			if(this.children){
				this.children.slice().forEach((child) => {
					child.destroy();
				});
			}
			delete this.children;

			let root = this.bdDom.root;
			if(Array.isArray(root)){
				root.forEach((node) => {
					domNodeToComponent.delete(node);
					node.parentNode && node.parentNode.removeChild(node);
				});
			}else{
				domNodeToComponent.delete(root);
				root.parentNode && root.parentNode.removeChild(root);
			}
			if(this.bdDom.handles){
				this.bdDom.handles.forEach(handle => handle.destroy());
			}
			delete this.bdDom;
			delete this._dom;
			this.bdAttachToDoc(false);
			this.bdMutateNotify("rendered", false, true);
		}
	}

	get rendered(){
		return !!(this.bdDom && this.bdDom.root);
	}

	own(...handles){
		let _handles = ownedHandlesCatalog.get(this);
		if(!_handles){
			ownedHandlesCatalog.set(this, (_handles = []));
		}
		pushHandles(_handles, ...handles);
	}

	ownWhileRendered(...handles){
		pushHandles(this.bdDom.handles || (this.bdDom.handles = []), ...handles);
	}

	get parent(){
		return this.bdParent;
	}

	bdAdopt(child){
		if(child.bdParent){
			throw new Error("unexpected");
		}
		(this.children || (this.children = [])).push(child);

		child.bdMutate("parent", "bdParent", this);
		child.bdAttachToDoc(this.bdAttachedToDoc);
	}

	bdAttachToDoc(value){
		if(this.bdMutate("attachedToDoc", "bdAttachedToDoc", !!value)){
			if(value && this.resize){
				this.resize();
			}
			this.children && this.children.forEach(child => child.bdAttachToDoc(value));
			return true;
		}else{
			return false;
		}
	}

	get attachedToDoc(){
		return !!this.bdAttachedToDoc;
	}

	insChild(...args){
		if(!this.rendered){
			throw new Error("parent component must be rendered before explicitly inserting a child");
		}
		let {src, attachPoint, position} = decodeRender(args);
		let child;
		if(src instanceof Component){
			child = src;
			if(child.parent){
				child.parent.delChild(child, true);
			}
			child.render();
		}else{ // child instanceof Element
			if(!src.isComponentType){
				src = new Element(Component, {elements: src});
			}
			child = this.constructor.renderElements(this, src);
		}

		if(/before|after|replace|only|first|last/.test(attachPoint) || typeof attachPoint === "number"){
			position = attachPoint;
			attachPoint = 0;
		}

		if(attachPoint){
			if(attachPoint in this){
				// node reference
				attachPoint = this[attachPoint];
			}else if(typeof attachPoint === "string"){
				attachPoint = document$1.getElementById(attachPoint);
				if(!attachPoint){
					throw new Error("unexpected");
				}
			}else if(position !== undefined){
				// attachPoint must be a child Component
				let index = this.children ? this.children.indexOf(attachPoint) : -1;
				if(index !== -1){
					// attachPoint is a child
					attachPoint = attachPoint.bdDom.root;
					if(Array.isArray(attachPoint)){
						switch(position){
							case "replace":
							case "only":
							case "before":
								attachPoint = attachPoint[0];
								break;
							case "after":
								attachPoint = attachPoint[attachPoint.length - 1];
								break;
							default:
								throw new Error("unexpected");
						}
					}
				}else{
					throw new Error("unexpected");
				}
			}else{
				// attachPoint without a position must give a node reference
				throw new Error("unexpected");
			}
		}else if(child.bdParentAttachPoint){
			// child is telling the parent where it wants to go; this is more specific than pChildrenAttachPoint
			if(child.bdParentAttachPoint in this){
				attachPoint = this[child.bdParentAttachPoint];
			}else{
				throw new Error("unexpected");
			}
		}else{
			attachPoint = this.bdChildrenAttachPoint || this.bdDom.root;
			if(Array.isArray(attachPoint)){
				throw new Error("unexpected");
			}
		}

		let childRoot = child.bdDom.root;
		if(Array.isArray(childRoot)){
			let firstChildNode = childRoot[0];
			unrender(insertNode(firstChildNode, attachPoint, position));
			childRoot.slice(1).reduce((prevNode, node) => {
				insertNode(node, prevNode, "after");
				return node;
			}, firstChildNode);
		}else{
			unrender(insertNode(childRoot, attachPoint, position));
		}

		this.bdAdopt(child);
		return child;
	}

	delChild(child, preserve){
		let index = this.children ? this.children.indexOf(child) : -1;
		if(index !== -1){
			let root = child.bdDom && child.bdDom.root;
			let removeNode = (node) => {
				node.parentNode && node.parentNode.removeChild(node);
			};
			Array.isArray(root) ? root.forEach(removeNode) : removeNode(root);
			child.bdMutate("parent", "bdParent", null);
			child.bdAttachToDoc(false);
			this.children.splice(index, 1);
			if(!preserve){
				child.destroy();
				child = false;
			}else if(preserve === "unrender"){
				child.unrender();
			}
			return child;
		}
		return false;
	}

	reorderChildren(children){
		let thisChildren = this.children;
		if(thisChildren && thisChildren.length){
			let node = this.children[0].bdDom.root.parentNode;

			children.forEach((child, i) => {
				if(thisChildren[i] !== child){
					let index = thisChildren.indexOf(child, i + 1);
					thisChildren.splice(index, 1);
					node.insertBefore(child.bdDom.root, thisChildren[i].bdDom.root);
					thisChildren.splice(i, 0, child);
				}
			});
		}
	}

	get staticClassName(){
		return this.kwargs.hasOwnProperty("staticClassName") ?
			this.kwargs.staticClassName : (this.constructor.className || "");
	}

	get className(){
		// WARNING: if a staticClassName was given as a constructor argument, then that part of node.className is NOT returned
		if(this.rendered){
			// if rendered, then look at what's actually in the document...maybe client code _improperly_ manipulated directly
			let root = this.bdDom.root;
			if(Array.isArray(root)){
				root = root[0];
			}
			let className = root.className;
			let staticClassName = this.staticClassName;
			if(staticClassName){
				staticClassName.split(" ").forEach(s => className = className.replace(s, ""));
			}
			return cleanClassName(className);
		}else{
			return this.bdClassName || "";
		}
	}

	set className(value){
		// WARNING: if a staticClassName was given as a constructor argument, then that part of node.className is NOT affected

		// clean up any space sloppiness, sometimes caused by client-code algorithms that manipulate className
		value = cleanClassName(value);
		if(!this.bdClassName){
			this.bdSetClassName(value, "");
		}else if(!value){
			this.bdSetClassName("", this.bdClassName);
		}else if(value !== this.bdClassName){
			this.bdSetClassName(value, this.bdClassName);
		}
	}

	containsClassName(value){
		// WARNING: if a staticClassName was given as a constructor argument, then that part of node.className is NOT considered

		value = cleanClassName(value);
		return (" " + (this.bdClassName || "") + " ").indexOf(value) !== -1;
	}

	addClassName(...values){
		let current = this.bdClassName || "";
		this.bdSetClassName(conditionClassNameArgs(values).reduce((className, value) => {
			return classValueToRegExp(value).test(className) ? className : className + value + " ";
		}, " " + current + " ").trim(), current);
		return this;
	}

	removeClassName(...values){
		// WARNING: if a staticClassName was given as a constructor argument, then that part of node.className is NOT considered
		let current = this.bdClassName || "";
		this.bdSetClassName(conditionClassNameArgs(values).reduce((className, value) => {
			return className.replace(classValueToRegExp(value, "g"), " ");
		}, " " + current + " ").trim(), current);
		return this;
	}

	toggleClassName(...values){
		// WARNING: if a staticClassName was given as a constructor argument, then that part of node.className is NOT considered
		let current = this.bdClassName || "";
		this.bdSetClassName(conditionClassNameArgs(values).reduce((className, value) => {
			if(classValueToRegExp(value).test(className)){
				return className.replace(classValueToRegExp(value, "g"), " ");
			}else{
				return className + value + " ";
			}
		}, " " + current + " ").trim(), current);
		return this;
	}

	get classList(){
		if(!this._classList){
			let self = this;
			this._classList = {
				get: function (){
					return self.className;
				},

				set: function (value){
					return self.className = value;
				},

				add: function (...values){
					return self.addClassName(...values);
				},

				ins: function (...values){
					return self.addClassName(...values);
				},

				remove: function (...values){
					return self.removeClassName(...values);
				},

				del: function (...values){
					return self.removeClassName(...values);
				},

				toggle: function (...values){
					return self.toggleClassName(...values);
				},

				contains: function (...values){
					return self.containsClassName(...values);
				},

				has: function (...values){
					return self.containsClassName(...values);
				}
			};
		}
		return this._classList;
	}

	bdSetClassName(newValue, oldValue){
		if(newValue !== oldValue){
			this.bdClassName = newValue;
			if(this.rendered){
				this.bdDom.root.setAttribute("class", calcDomClassName(this));
			}
			this.bdMutateNotify("className", newValue, oldValue);
			let oldVisibleValue = oldValue ? oldValue.indexOf("hidden") === -1 : true,
				newVisibleValue = newValue ? newValue.indexOf("hidden") === -1 : true;
			if(oldVisibleValue !== newVisibleValue){
				this.bdMutateNotify("visible", newVisibleValue, oldVisibleValue);
			}
		}
	}

	bdOnFocus(){
		this.addClassName("bd-focused");
		this.bdMutate("hasFocus", "bdHasFocus", true);
	}

	bdOnBlur(){
		this.removeClassName("bd-focused");
		this.bdMutate("hasFocus", "bdHasFocus", false);
	}

	get hasFocus(){
		return !!this.bdHasFocus;
	}

	focus(){
		if(this.bdDom){
			(this.bdDom.tabIndexNode || this.bdDom.root).focus();
		}
	}

	get tabIndex(){
		if(this.rendered){
			// unconditionally make sure this.bdTabIndex and the dom is synchronized on each get
			return (this.bdTabIndex = (this.bdDom.tabIndexNode || this.bdDom.root).tabIndex);
		}else{
			return this.bdTabIndex;
		}
	}

	set tabIndex(value){
		if(!value && value !== 0){
			value = "";
		}
		if(value !== this.bdTabIndex){
			this.rendered && ((this.bdDom.tabIndexNode || this.bdDom.root).tabIndex = value);
			this.bdMutate("tabIndex", "bdTabIndex", value);
		}
	}

	get enabled(){
		return !this.bdDisabled;
	}

	set enabled(value){
		this.disabled = !value;
	}

	get disabled(){
		return !!this.bdDisabled;
	}

	set disabled(value){
		value = !!value;
		if(this.bdDisabled !== value){
			this.bdDisabled = value;
			this.bdMutateNotify([["disabled", value, !value], ["enabled", !value, value]]);
			this[value ? "addClassName" : "removeClassName"]("bd-disabled");
		}
	}

	get visible(){
		return !this.containsClassName("bd-hidden");
	}

	set visible(value){
		value = !!value;
		if(value !== !this.containsClassName("bd-hidden")){
			if(value){
				this.removeClassName("bd-hidden");
				this.resize && this.resize();
			}else{
				this.addClassName("bd-hidden");
			}
			this.bdMutateNotify("visible", value, !value);
		}
	}

	get title(){
		if(this.rendered){
			return (this.bdDom.titleNode || this.bdDom.root).title;
		}else{
			return this.bdTitle;
		}
	}

	set title(value){
		if(this.bdMutate("title", "bdTitle", value)){
			this.rendered && ((this.bdDom.titleNode || this.bdDom.root).title = value);
		}
	}

	static get(domNode){
		return domNodeToComponent.get(domNode);
	}

	static renderElements(owner, e){
		if(Array.isArray(e)){
			return e.map((e) => Component.renderElements(owner, e));
		}else if(e instanceof Element){
			const {type, ctorProps, ppFuncs, children} = e;
			let result;
			if(e.isComponentType){
				let componentInstance = result = new type(ctorProps);
				componentInstance.render();
				ppFuncs && postProcess(ppFuncs, owner, componentInstance);
				if(children){
					let renderedChildren = Component.renderElements(owner, children);
					if(Array.isArray(renderedChildren)){
						renderedChildren.forEach((child) => result.insChild(child));
					}else{
						result.insChild(renderedChildren);
					}
				}
			}else{
				let domNode = result = createNode(type, ctorProps);
				if("tabIndex" in ctorProps && ctorProps.tabIndex !== false){
					owner.bdDom.tabIndexNode = domNode;
				}
				ppFuncs && postProcess(ppFuncs, owner, domNode);
				if(children){
					let renderedChildren = Component.renderElements(owner, children);
					if(Array.isArray(renderedChildren)){
						renderedChildren.forEach((child, i) => addChildToDomNode(owner, domNode, child, children[i].isComponentType));
					}else{
						addChildToDomNode(owner, domNode, renderedChildren, children.isComponentType);
					}
				}
			}
			return result;
		}else{
			// e must be convertible to a string
			return document$1.createTextNode(e);
		}
	}
}

Component.watchables = ["rendered", "parent", "attachedToDoc", "className", "hasFocus", "tabIndex", "enabled", "visible", "title"];
Component.events = [];
Component.withWatchables = (...args) => withWatchables(Component, ...args);

insPostProcessingFunction("bdAttach",
	function (ppfOwner, ppfTarget, name){
		if(typeof name === "function"){
			ppfOwner.ownWhileRendered(name(ppfTarget, ppfOwner));
		}else{
			ppfOwner[name] = ppfTarget;
			ppfOwner.ownWhileRendered({
				destroy: function (){
					delete ppfOwner[name];
				}
			});
		}
	}
);

insPostProcessingFunction("bdWatch", true,
	function (ppfOwner, ppfTarget, watchers){
		Reflect.ownKeys(watchers).forEach(eventType => {
			let watcher = watchers[eventType];
			if(typeof watcher !== "function"){
				watcher = ppfOwner[eventType].bind(ppfOwner);
			}
			ppfTarget.ownWhileRendered(ppfTarget.watch(eventType, watcher));
		});
	}
);

insPostProcessingFunction("bdExec",
	function (ppfOwner, ppfTarget, ...args){
		for(let i = 0; i < args.length;){
			let f = args[i++];
			if(typeof f === "function"){
				f(ppfOwner, ppfTarget);
			}else if(typeof f === "string"){
				if(!(typeof ppfTarget[f] === "function")){
					// eslint-disable-next-line no-console
					console.error("unexpected");
				}
				if(i < args.length && Array.isArray(args[i])){
					ppfTarget[f](...args[i++], ppfOwner, ppfTarget);
				}else{
					ppfTarget[f](ppfOwner, ppfTarget);
				}
			}else{
				// eslint-disable-next-line no-console
				console.error("unexpected");
			}
		}
	}
);

insPostProcessingFunction("bdTitleNode",
	function (ppfOwner, ppfTarget){
		ppfOwner.bdDom.titleNode = ppfTarget;
	}
);

insPostProcessingFunction("bdParentAttachPoint",
	function (ppfOwner, ppfTarget, propertyName){
		ppfTarget.bdParentAttachPoint = propertyName;
	}
);

insPostProcessingFunction("bdChildrenAttachPoint",
	function (ppfOwner, ppfTarget){
		ppfOwner.bdChildrenAttachPoint = ppfTarget;
	}
);

insPostProcessingFunction("bdReflectClass",
	function (ppfOwner, ppfTarget, ...args){
		// args is a list of ([owner, ] property, [, formatter])...
		// very much like bdReflect, except we're adding/removing components (words) from this.classname

		function normalize(value){
			return !value ? "" : value + "";
		}

		function install(owner, prop, formatter){
			let watchable = getWatchableRef(owner, prop, formatter);
			ppfOwner.ownWhileRendered(watchable);
			let value = normalize(watchable.value);
			value && ppfOwner.addClassName(value);
			ppfOwner.ownWhileRendered(watchable.watch((newValue, oldValue) => {
				newValue = normalize(newValue);
				oldValue = normalize(oldValue);
				if(newValue !== oldValue){
					oldValue && ppfOwner.removeClassName(oldValue);
					newValue && ppfOwner.addClassName(newValue);

				}
			}));
		}

		args = args.slice();
		let owner, prop;
		while(args.length){
			owner = args.shift();
			if(typeof owner === "string" || typeof owner === "symbol"){
				prop = owner;
				owner = ppfOwner;
			}else{
				prop = args.shift();
			}
			install(owner, prop, typeof args[0] === "function" ? args.shift() : null);
		}
	}
);

function isComponentDerivedCtor(f){
	return f === Component || (f && isComponentDerivedCtor(Object.getPrototypeOf(f)));
}

const prototypeOfObject = Object.getPrototypeOf({});

function decodeRender(args){
	// eight signatures...
	//     Signatures 1-2 render an element, 3-6 render a Component, 7-8 render an instance of a Component
	//
	//     Each of the above groups may or may not have the args node:domNode[, position:Position="last"]
	//     which indicate where to attach the rendered Component instance (or not).
	//
	//     when this decode routine is used by Component::insertChild, then node can be a string | symbol, indicating
	//     an instance property that holds the node
	//
	//     1. render(e:Element)
	//     => isComponentDerivedCtor(e.type), then render e.type(e.props); otherwise, render Component({elements:e})
	//
	//     2. render(e:Element, node:domNode[, position:Position="last"])
	// 	   => [1] with attach information
	//
	//     3. render(C:Component)
	//     => render(C, {})
	//
	//     4. render(C:Component, args:kwargs)
	//     => render(C, args)
	//     // note: args is kwargs for C's constructor; therefore, postprocessing instructions are meaningless unless C's
	//     // construction defines some usage for them (atypical)
	//
	//     5. render(C:Component, node:domNode[, position:Position="last"])
	//     => [3] with attach information
	//
	//     6. render(C:Component, args:kwargs, node:domNode[, position:Position="last"])
	//     => [4] with attach information
	//
	//     7. render(c:instanceof Component)
	//     => c.render()
	//
	//     8. render(c:instanceof Component, node:domNode[, position:Position="last"])
	// 	   => [7] with attach information
	//
	//     Position one of "first", "last", "before", "after", "replace", "only"; see dom::insert
	//
	//     returns {
	//	       src: instanceof Component | Element
	//		   attachPoint: node | string | undefined
	//		   position: string | undefined
	//     }
	//
	//     for signatures 3-6, an Element is manufactured given the arguments
	//
	let [arg1, arg2, arg3, arg4] = args;
	if(arg1 instanceof Element || arg1 instanceof Component){
		// [1] or [2] || [7] or [8]
		return {src: arg1, attachPoint: arg2, position: arg3};
	}else{
		if(!isComponentDerivedCtor(arg1)){
			throw new Error("first argument must be an Element, Component, or a class derived from Component");
		}
		if(args.length === 1){
			// [3]
			return {src: new Element(arg1)};
		}else{
			// more than one argument; the second argument is either props or not
			if(Object.getPrototypeOf(arg2) === prototypeOfObject){
				// [4] or [6]
				// WARNING: this signature requires kwargs to be a plain Javascript Object (which is should be!)
				return {src: new Element(arg1, arg2), attachPoint: arg3, position: arg4};
			}else{
				// [5]
				return {src: new Element(arg1), attachPoint: arg2, position: arg3};
			}
		}
	}
}

function unrender(node){
	function unrender_(node){
		let component = domNodeToComponent.get(node);
		if(component){
			component.destroy();
		}
	}

	Array.isArray(node) ? node.forEach(unrender_) : (node && unrender_(node));
}

function render(...args){
	let result;
	let {src, attachPoint, position} = decodeRender(args);
	if(src instanceof Element){
		if(src.isComponentType){
			result = new src.type(src.ctorProps);
		}else{
			result = new Component({elements: src});
		}
		result.render();
	}else{ // src instanceof Component
		result = src;
		result.render();
	}

	if(typeof attachPoint === "string"){
		attachPoint = document$1.getElementById(attachPoint);
	}

	if(attachPoint){
		let root = result.bdDom.root;
		if(Array.isArray(root)){
			let firstChildNode = root[0];
			unrender(insertNode(firstChildNode, attachPoint, position));
			root.slice(1).reduce((prevNode, node) => {
				insertNode(node, prevNode, "after");
				return node;
			}, firstChildNode);
		}else{
			unrender(insertNode(root, attachPoint, position));
		}
		result.bdAttachToDoc(document$1.body.contains(attachPoint));
	}
	return result;
}

function getAttributeValueFromEvent(e, attributeName, stopNode){
	let node = e.target;
	while(node && node !== stopNode){
		if(node.getAttributeNode(attributeName)){
			return node.getAttribute(attributeName);
		}
		node = node.parentNode;
	}
	return undefined;
}


function normalizeNodeArg(arg){
	return arg instanceof Component ? arg.bdDom.root : (typeof arg === "string" ? document.getElementById(arg) : arg);
}

function setAttr(node, name, value){
	node = normalizeNodeArg(node);
	if(arguments.length === 2){
		let hash = name;
		Object.keys(hash).forEach(name => {
			setAttr(node, name, hash[name]);
		});
	}else{
		if(name === "style"){
			setStyle(node, value);
		}else if(name === "innerHTML" || (name in node && node instanceof HTMLElement)){
			node[name] = value;
		}else{
			node.setAttribute(name, value);
		}
	}
}

function getAttr(node, name){
	node = normalizeNodeArg(node);
	if(name in node && node instanceof HTMLElement){
		return node[name];
	}else{
		return node.getAttribute(name);
	}
}

let lastComputedStyleNode = 0;
let lastComputedStyle = 0;

function getComputedStyle(node){
	node = normalizeNodeArg(node);
	if(lastComputedStyleNode !== node){
		lastComputedStyle = window.getComputedStyle((lastComputedStyleNode = node));
	}
	return lastComputedStyle;
}

function getStyle(node, property){
	node = normalizeNodeArg(node);
	if(lastComputedStyleNode !== node){
		lastComputedStyle = window.getComputedStyle((lastComputedStyleNode = node));
	}
	let result = lastComputedStyle[property];
	return (typeof result === "string" && /px$/.test(result)) ? parseFloat(result) : result;
}

function getStyles(node, ...styleNames){
	node = normalizeNodeArg(node);
	if(lastComputedStyleNode !== node){
		lastComputedStyle = window.getComputedStyle((lastComputedStyleNode = node));
	}

	let styles = [];
	styleNames.forEach((p) => {
		if(Array.isArray(p)){
			styles = styles.concat(p);
		}else if(typeof p === "string"){
			styles.push(p);
		}else{
			Object.keys(p).forEach((p) => styles.push(p));
		}
	});

	let result = {};
	styles.forEach((property) => {
		let value = lastComputedStyle[property];
		result[property] = (typeof value === "string" && /px$/.test(value)) ? parseFloat(value) : value;
	});
	return result;
}

function setStyle(node, property, value){
	node = normalizeNodeArg(node);
	if(arguments.length === 2){
		if(typeof property === "string"){
			node.style = property;
		}else{
			let hash = property;
			Object.keys(hash).forEach(property => {
				node.style[property] = hash[property];
			});
		}
	}else{
		node.style[property] = value;
	}
}

function getPosit(node){
	let result = normalizeNodeArg(node).getBoundingClientRect();
	result.t = result.top;
	result.b = result.bottom;
	result.l = result.left;
	result.r = result.right;
	result.h = result.height;
	result.w = result.width;
	return result;
}

function setPosit(node, posit){
	node = normalizeNodeArg(node);
	for(let p in posit){
		switch(p){
			case "t":
				node.style.top = posit.t + "px";
				break;
			case "b":
				node.style.bottom = posit.b + "px";
				break;
			case "l":
				node.style.left = posit.l + "px";
				break;
			case "r":
				node.style.right = posit.r + "px";
				break;
			case "h":
				node.style.height = posit.h + "px";
				break;
			case "w":
				node.style.width = posit.w + "px";
				break;
			case "maxH":
				node.style.maxHeight = posit.maxH + "px";
				break;
			case "maxW":
				node.style.maxWidth = posit.maxW + "px";
				break;
			case "z":
				node.style.zIndex = posit.z;
				break;
			default:
		}
	}
}

function insertBefore(node, refNode){
	refNode.parentNode.insertBefore(node, refNode);
}

function insertAfter(node, refNode){
	let parent = refNode.parentNode;
	if(parent.lastChild === refNode){
		parent.appendChild(node);
	}else{
		parent.insertBefore(node, refNode.nextSibling);
	}
}

function insert(node, refNode, position){
	if(position === undefined || position === "last"){
		// short circuit the common case
		refNode.appendChild(node);
	}else switch(position){
		case "before":
			insertBefore(node, refNode);
			break;
		case "after":
			insertAfter(node, refNode);
			break;
		case "replace":
			refNode.parentNode.replaceChild(node, refNode);
			return (refNode);
		case "only":{
			let result = [];
			while(refNode.firstChild){
				result.push(refNode.removeChild(refNode.firstChild));
			}
			refNode.appendChild(node);
			return result;
		}
		case "first":
			if(refNode.firstChild){
				insertBefore(node, refNode.firstChild);
			}else{
				refNode.appendChild(node);
			}
			break;
		default:
			if(typeof position === "number"){
				let children = refNode.childNodes;
				if(!children.length || children.length <= position){
					refNode.appendChild(node);
				}else{
					insertBefore(node, children[position < 0 ? Math.max(0, children.length + position) : position]);
				}
			}else{
				throw new Error("illegal position");
			}
	}
}

function create(tag, props){
	let result = Array.isArray(tag) ? document.createElementNS(tag[0] + "", tag[1]) : document.createElement(tag);
	if(props){
		Reflect.ownKeys(props).forEach(p => setAttr(result, p, props[p]));
	}
	return result;
}


const DATA_BD_HIDE_SAVED_VALUE = "data-bd-hide-saved-value";

function hide(...nodes){
	nodes.forEach((node) => {
		node = normalizeNodeArg(node);
		if(node){
			if(!node.hasAttribute(DATA_BD_HIDE_SAVED_VALUE)){
				node.setAttribute(DATA_BD_HIDE_SAVED_VALUE, node.style.display);
				node.style.display = "none";
			}//else, ignore, multiple calls to hide
		}
	});
}

function show(...nodes){
	nodes.forEach((node) => {
		node = normalizeNodeArg(node);
		if(node){
			let displayValue = "";
			if(node.hasAttribute(DATA_BD_HIDE_SAVED_VALUE)){
				displayValue = node.getAttribute(DATA_BD_HIDE_SAVED_VALUE);
				node.removeAttribute(DATA_BD_HIDE_SAVED_VALUE);
			}
			node.style.display = displayValue;
		}
	});
}

function getMaxZIndex(parent){
	let node, cs, max = 0, children = parent.childNodes, i = 0, end = children.length;
	while(i < end){
		node = children[i++];
		cs = node && node.nodeType === 1 && getComputedStyle(node);
		max = Math.max(max, (cs && cs.zIndex && Number(cs.zIndex)) || 0);
	}
	return max;
}

function destroyDomChildren(node){
	let childNode;
	while((childNode = node.lastChild)){
		node.removeChild(childNode);
	}
}

function destroyDomNode(node){
	node && node.parentNode && node.parentNode.removeChild(node);
}

function connect(target, type, listener, useCapture){
	let destroyed = false;
	useCapture = !!useCapture;
	target.addEventListener(type, listener, useCapture);
	return {
		destroy: function (){
			if(!destroyed){
				destroyed = true;
				target.removeEventListener(type, listener, useCapture);
			}
		}
	};
}

function stopEvent(event){
	if(event && event.preventDefault){
		event.preventDefault();
		event.stopPropagation();
	}
}


let focusedComponent = null,
	focusedNode = null,
	focusedStack = [],
	previousFocusedComponent = null,
	previousFocusedNode = null;

class FocusManager extends EventHub{
	get focusedComponent(){
		return focusedComponent;
	}

	get focusedNode(){
		return focusedNode;
	}

	get focusedStack(){
		return focusedStack;
	}

	get previousFocusedComponent(){
		return previousFocusedComponent;
	}

	get previousFocusedNode(){
		return previousFocusedNode;
	}
}

let focusManager = new FocusManager();
let focusWatcher = 0;

function processNode(node){
	if(focusWatcher){
		clearTimeout(focusWatcher);
		focusWatcher = 0;
	}

	previousFocusedNode = focusedNode;
	focusedNode = node;
	if(previousFocusedNode === focusedNode){
		return;
	}

	// find the focused component, if any
	while(node && (!Component.get(node))){
		node = node.parentNode;
	}
	let focusedComponent_ = node && Component.get(node),
		stack = [];
	if(focusedComponent_){
		let p = focusedComponent_;
		while(p){
			stack.unshift(p);
			p = p.parent;
		}
	}

	let focusStack = focusedStack,
		newStackLength = stack.length,
		oldStackLength = focusStack.length,
		i = 0,
		j, component;
	while(i < newStackLength && i < oldStackLength && stack[i] === focusStack[i]){
		i++;
	}
	// [0..i-1] are identical in each stack


	// signal blur from the path end to the first identical component (not including the first identical component)
	for(j = i; j < oldStackLength; j++){
		component = focusStack.pop();
		component.bdOnBlur();
		focusManager.bdNotify({type: "blurComponent", component: component});
	}

	// signal focus for all new components that just gained the focus
	for(j = i; j < newStackLength; j++){
		focusStack.push(component = stack[j]);
		component.bdOnFocus();
		focusManager.bdNotify({type: "focusComponent", component: component});
	}

	previousFocusedComponent = focusedComponent;
	focusedComponent = focusedComponent_;
	focusManager.bdNotify({type: "focusedComponent", component: focusedComponent_});
}

connect(document.body, "focusin", function (e){
	let node = e.target;
	if(!node || node.parentNode == null || node === self[focusedNode]){
		return;
	}
	processNode(node);
});

// eslint-disable-next-line no-unused-vars
connect(document.body, "focusout", function (){
	// If the blur event isn't followed by a focus event, it means the user clicked on something unfocusable,
	// so clear focus.
	if(focusWatcher){
		clearTimeout(focusWatcher);
	}

	focusWatcher = setTimeout(function (){
		processNode(null);
	}, 5);
});

let viewportWatcher = new EventHub;

let scrollTimeoutHandle = 0;

// eslint-disable-next-line no-unused-vars
connect(window, "scroll", function (){
	if(scrollTimeoutHandle){
		clearTimeout(scrollTimeoutHandle);
	}
	scrollTimeoutHandle = setTimeout(function (){
		scrollTimeoutHandle = 0;
		viewportWatcher.bdNotify({type: "scroll"});
	}, 10);
}, true);


let resizeTimeoutHandle = 0;

// eslint-disable-next-line no-unused-vars
connect(window, "resize", function (){
	if(resizeTimeoutHandle){
		clearTimeout(resizeTimeoutHandle);
	}
	resizeTimeoutHandle = setTimeout(function (){
		resizeTimeoutHandle = 0;
		viewportWatcher.bdNotify({type: "resize"});
	}, 10);
}, true);


insPostProcessingFunction("bdReflect",
	function (prop, value){
		if(prop === null && value instanceof Object && !Array.isArray(value)){
			// e.g., bdReflect:{p1:"someProp", p2:[refObject, "someOtherProp", someFormatter]}
			return value;
		}else if(prop){
			// e.g., bdReflect_someProp: [refObject, ] prop [, someFormatter]
			return {[prop]: value};
		}else{
			// e.g., bdReflect: [refObject, ] prop [, someFormatter]
			return {innerHTML: value};
		}
	},
	function (ppfOwner, ppfTarget, props){
		// props is a hash from property in ppfTarget to a list of ([refObject, ] property, [, formatter])...
		let install, watchable;
		if(ppfTarget instanceof Component){
			install = function (destProp, refObject, prop, formatter){
				ppfOwner.ownWhileRendered((watchable = getWatchableRef(refObject, prop, formatter)));
				ppfTarget[destProp] = watchable.value;
				ppfOwner.ownWhileRendered(watchable.watch(newValue => {
					ppfTarget[destProp] = newValue;
				}));
			};
		}else{
			install = function (destProp, refObject, prop, formatter){
				ppfOwner.ownWhileRendered((watchable = getWatchableRef(refObject, prop, formatter)));
				setAttr(ppfTarget, destProp, watchable.value);
				ppfOwner.ownWhileRendered(watchable.watch(newValue => {
					setAttr(ppfTarget, destProp, newValue);
				}));
			};
		}

		Reflect.ownKeys(props).forEach(destProp => {
			let args = Array.isArray(props[destProp]) ? props[destProp].slice() : [props[destProp]];
			let refObject, prop;
			while(args.length){
				refObject = args.shift();
				if(typeof refObject === "string" || typeof refObject === "symbol"){
					prop = refObject;
					refObject = ppfOwner;
				}else{
					prop = args.shift();
				}
				install(destProp, refObject, prop, typeof args[0] === "function" ? args.shift() : null);
			}
		});
	}
);

insPostProcessingFunction("bdAdvise", true,
	function (ppfOwner, ppfTarget, listeners){
		Reflect.ownKeys(listeners).forEach(eventType => {
			let listener = listeners[eventType];
			if(typeof listener !== "function"){
				listener = ppfOwner[listener].bind(ppfOwner);
			}
			ppfOwner.ownWhileRendered(ppfTarget instanceof Component ? ppfTarget.advise(eventType, listener) : connect(ppfTarget, eventType, listener));
		});
	}
);
insPostProcessingFunction("bdAdvise", "bdOn");

function applyLengthWatchers(owner, newValue, oldValue){
	if(oldValue !== newValue){
		owner.children.forEach(child => {
			child.onMutateCollectionLength && child.onMutateCollectionLength(newValue, oldValue);
			child.bdMutateNotify("collectionLength", newValue, oldValue);
		});
	}
}

function updateChildren(collection, owner, oldLength){
	let children = owner.children;
	let newLength = collection.length;
	for(let i = 0, end = newLength; i < end; i++){
		let item = collection[i];
		let child, j = i, childrenCount = children.length;
		while(j < childrenCount && (child = children[j]) && child.collectionItem !== item) j++;
		if(j >= childrenCount){
			// new item
			owner.insChild(i);
		}else{
			if(j !== i){
				// moved child
				Component.insertNode(child.bdDom.root, child.bdDom.root.parentNode, i);
				children.splice(j, 1);
				children.splice(i, 0, child);
			}
			// child may have moved by inserting new children before it; therefore...
			child.collectionIndex = i;
		}
	}
	children.slice(newLength).forEach(child => owner.delChild(child));
	applyLengthWatchers(owner, newLength, oldLength);
}

function setSpliceAdvice(collection, owner){
	return collection.before("splice", () => {
		if(!owner.rendered){
			return;
		}
		let oldLength = collection.length;
		let holdSuspendWatchAdvise = owner.suspendWatchAdvise;
		owner.suspendWatchAdvise = true;
		return () => {
			owner.suspendWatchAdvise = holdSuspendWatchAdvise;
			updateChildren(collection, owner, oldLength);
		};
	});
}

function setShiftAdvice(collection, owner){
	return collection.before("shift", () => {
		if(!owner.rendered || !collection.length){
			return;
		}
		let holdSuspendWatchAdvise = owner.suspendWatchAdvise;
		owner.suspendWatchAdvise = true;
		return () => {
			owner.suspendWatchAdvise = holdSuspendWatchAdvise;
			let children = owner.children;
			owner.delChild(children[0]);
			let newLength = collection.length;
			let oldLength = newLength + 1;
			children.forEach((child, i) => (child.collectionIndex = i));
			applyLengthWatchers(owner, newLength, oldLength);
		};
	});
}

function setUnshiftAdvice(collection, owner){
	return collection.before("unshift", () => {
		if(!owner.rendered){
			return;
		}
		let oldLength = collection.length;
		let holdSuspendWatchAdvise = owner.suspendWatchAdvise;
		owner.suspendWatchAdvise = true;
		return () => {
			owner.suspendWatchAdvise = holdSuspendWatchAdvise;
			let newLength = collection.length;
			for(let i = 0, end = newLength - oldLength; i < end; i++){
				owner.insChild(i);
			}
			for(let children = owner.children, i = newLength - oldLength; i < newLength; i++){
				children[i].collectionIndex = i;
			}
			applyLengthWatchers(owner, newLength, oldLength);
		};
	});
}

function setReverseAdvice(collection, owner){
	return collection.before("reverse", () => {
		if(!owner.rendered){
			return;
		}
		let holdSuspendWatchAdvise = owner.suspendWatchAdvise;
		owner.suspendWatchAdvise = true;
		return () => {
			owner.suspendWatchAdvise = holdSuspendWatchAdvise;
			let children = owner.children;
			for(let i = children.length - 1; i >= 0; i--){
				let node = children[i].bdDom.root;
				node.parentNode.appendChild(node);
			}
			children.reverse();
			children.forEach((child, i) => child.collectionIndex = i);
		};
	});
}

function setSortAdvice(collection, owner){
	return collection.before("sort", () => {
		if(!owner.rendered){
			return;
		}
		let holdSuspendWatchAdvise = owner.suspendWatchAdvise;
		owner.suspendWatchAdvise = true;
		return () => {
			owner.suspendWatchAdvise = holdSuspendWatchAdvise;
			updateChildren(collection, owner, collection.length);
		};
	});
}

function setOrderAdvice(collection, owner){
	return collection.before("order", () => {
		if(!owner.rendered){
			return;
		}
		let holdSuspendWatchAdvise = owner.suspendWatchAdvise;
		owner.suspendWatchAdvise = true;
		return () => {
			owner.suspendWatchAdvise = holdSuspendWatchAdvise;
			updateChildren(collection, owner, collection.length);
		};
	});
}

class Collection extends Component{
	constructor(kwargs){
		super(kwargs);
		this.collection = kwargs.collection;
	}

	set collection(value){
		// always an array
		value = value || toWatchable([]);
		if(this.bdMutate("collection", "bdCollection", value)){
			this.bdSetupHandle && this.bdSetupHandle.destroy();

			if(this.rendered){
				let children = this.children;
				let collection = this.bdCollection;
				let oldLength = children.length;
				let newLength = collection.length;
				let mutateLength = oldLength !== newLength;
				for(let i = 0, end = Math.min(oldLength, newLength); i < end; i++){
					let child = children[i];
					child.collectionItem = collection[i];
					if(mutateLength){
						child.onMutateCollectionLength && child.onMutateCollectionLength(newLength, oldLength);
						child.bdMutateNotify("collectionLength", newLength, oldLength);
					}
				}
			}

			let collection = this.bdCollection;
			if(isWatchable(collection) && !this.kwargs.collectionIsScalars){
				let handles = [
					this.watch(this.bdCollection, "length", (newValue, oldValue) => {
						if(this.suspendWatchAdvise) return;
						if(this.rendered){
							this.bdSynchChildren();
							applyLengthWatchers(this, newValue, oldValue);
						}
					}),
					setSpliceAdvice(collection, this),
					setShiftAdvice(collection, this),
					setUnshiftAdvice(collection, this),
					setReverseAdvice(collection, this),
					setSortAdvice(collection, this),
					setOrderAdvice(collection, this)
				];
				let setupHandle = this.bdSetupHandle = {
					destroy(){
						// multiple calls imply no-op
						setupHandle.destroy = () => 0;
						destroyAll(handles);
					}
				};
				this.own(setupHandle);
			}
		}
	}

	get collection(){
		return this.bdCollection;
	}

	render(proc){
		if(!this.bdDom){
			super.render();
			this.children = [];
			this.bdSynchChildren();
			proc && proc();
		}
		return this.bdDom.root;
	}

	insChild(collectionIndex){
		let child = new this.kwargs.childType({parent: this, collectionIndex: collectionIndex});
		Component.insertNode(child.render(), this.bdChildrenAttachPoint || this.bdDom.root, collectionIndex);
		this.children.splice(collectionIndex, 0, child);
		child.bdAttachToDoc(this.bdAttachedToDoc);
		return child;
	}

	bdSynchChildren(){
		let children = this.children;
		let collection = this.collection;
		while(children.length < collection.length){
			this.insChild(children.length);
		}
		while(children.length > collection.length){
			this.delChild(children[children.length - 1]);
		}
	}
}

let onMutateNames$1 = {};

function onMutateItemWatchable(propName, owner, newValue, oldValue){
	let procName = onMutateNames$1[propName];
	if(procName === undefined){
		if(typeof propName === "symbol"){
			return (onMutateNames$1[propName] = false);
		}
		procName = onMutateNames$1[propName] = "onMutate" + propName.substring(0, 1).toUpperCase() + propName.substring(1);
	}
	procName && owner[procName] && owner[procName](newValue, oldValue);
}

class CollectionChild extends Component{
	constructor(kwargs){
		super(kwargs);
		this._itemWatchHandles = [];
		this.bdParent = kwargs.parent;
		this.bdCollectionIndex = kwargs.collectionIndex;
		this.bdCollectionItem = kwargs.parent.collection[kwargs.collectionIndex];
		this.constructor.itemWatchables.forEach(prop => Object.defineProperty(this, prop, {
			enumerable: true,
			get: () => this.bdCollectionItem[prop]
		}));
		this._connectWatchers();
	}

	get collectionIndex(){
		// watchable
		return this.bdCollectionIndex;
	}

	set collectionIndex(newValue){
		if(newValue !== this.bdCollectionIndex){
			let oldValue = this.bdCollectionIndex;
			this.bdCollectionIndex = newValue;
			this.onMutateCollectionIndex && this.onMutateCollectionIndex(newValue, oldValue);
			this.bdMutateNotify("collectionIndex", newValue, oldValue);
			this._connectWatchers();
		}
	}

	get collectionLength(){
		// watchable iff the parent, like Collection, applies the watchers
		return this.parent.collection.length;
	}

	get collectionItem(){
		// watchable, see setter and _connectWatchers
		return this.bdCollectionItem;
	}

	set collectionItem(newValue){
		this._applyWatchers(newValue, this.bdCollectionItem);
		this.bdCollectionItem = newValue;
		this._connectWatchers();
	}

	_applyWatchers(newValue, oldValue){
		if(newValue !== oldValue){
			this.onMutateCollectionItem && this.onMutateCollectionItem(newValue, oldValue);
			this.bdMutateNotify("collectionItem", newValue, oldValue);
			this.constructor.itemWatchables.forEach(prop => {
				onMutateItemWatchable(prop, this, newValue[prop], oldValue[prop]);
				this.bdMutateNotify(prop, newValue[prop], oldValue[prop]);
			});
		}
	}

	_connectWatchers(){
		this._itemWatchHandles.forEach(h => h.destroy());
		let handles = this._itemWatchHandles = [];
		if(isWatchable(this.parent.collection)){
			let collectionItem = this.bdCollectionItem;
			handles.push(this.watch(this.parent.collection, this.collectionIndex, (newValue, oldValue, target, prop) => {
				if(this.parent.suspendWatchAdvise) return;
				if(prop.length === 1){
					// the whole item changed; therefore, reset the watch
					this._applyWatchers(newValue, oldValue);
					this._connectWatchers();
				}else{
					this.onMutateCollectionItem && this.onMutateCollectionItem(collectionItem, UNKNOWN_OLD_VALUE);
					this.bdMutateNotify("collectionItem", collectionItem, UNKNOWN_OLD_VALUE);
				}
			}));
			this.constructor.itemWatchables.forEach(prop =>
				handles.push(this.watch(collectionItem, prop, (newValue, oldValue, target, _prop) => {
					if(this.parent.suspendWatchAdvise) return;
					onMutateItemWatchable(prop, this, newValue, oldValue, target, _prop);
					this.bdMutateNotify(prop, newValue, oldValue);
				}))
			);
		}
	}
}

CollectionChild.itemWatchables = [];

CollectionChild.withWatchables = function (...args){
	let superclass = typeof args[0] === "function" ? args.shift() : CollectionChild;
	let itemWatchables = [];
	args = args.filter(prop => {
		let match = prop.match(/^item:(.+)/);
		if(match){
			itemWatchables = itemWatchables.concat(match[1].split(",").map(p => p.trim()).filter(p => !!p));
			return false;
		}
		return true;
	});
	let result = withWatchables(superclass, ...args);
	result.itemWatchables = itemWatchables;
	return result;
};

initialize(document, create, insert);

const version = "2.3.1";

// bd-core must be installed as a sibling of bd-widgets

var keys = {
	backspace: 8,
	tab: 9,
	clear: 12,
	enter: 13,
	shift: 16,
	ctrl: 17,
	alt: 18,
	pause: 19,
	capsLock: 20,
	escape: 27,
	space: 32,
	pageUp: 33,
	pageDown: 34,
	end: 35,
	home: 36,
	left: 37,
	up: 38,
	right: 39,
	down: 40,
	insert: 45,
	delete: 46,
	help: 47,
	leftWindow: 91,
	rightWindow: 92,
	select: 93,
	numpad_0: 96,
	numpad_1: 97,
	numpad_2: 98,
	numpad_3: 99,
	numpad_4: 100,
	numpad_5: 101,
	numpad_6: 102,
	numpad_7: 103,
	numpad_8: 104,
	numpad_9: 105,
	numpad_multiply: 106,
	numpad_plus: 107,
	numpad_enter: 108,
	numpad_minus: 109,
	numpad_period: 110,
	numpad_divide: 111,
	f1: 112,
	f2: 113,
	f3: 114,
	f4: 115,
	f5: 116,
	f6: 117,
	f7: 118,
	f8: 119,
	f9: 120,
	f10: 121,
	f11: 122,
	f12: 123,
	f13: 124,
	f14: 125,
	f15: 126,
	numLock: 144,
	scrollLock: 145,
	dpadUp: 175,
	dpadDown: 176,
	dpadLeft: 177,
	dpadRight: 178
};

const VALID = 0;
const CONTEXT_INFO = 1;
const SCALAR_INFO = 2;
const CONTEXT_WARN = 3;
const SCALAR_WARN = 4;
const CONTEXT_ERROR = 5;
const SCALAR_ERROR = 6;

// privates...
const pLevel = Symbol("pLevel");
const pMessages = Symbol("pMessages");
const pMutate = Symbol("pMutate");
const pValidateParams = Symbol("pValidateParams");
const pClassName = Symbol("pClassName");
const pAddMessage = Symbol("pAddMessage");
const pDelMessage = Symbol("pDelMessage");
let qlock = 0;
let eventQ = [];

function pushQ(type, level, message, target){
	eventQ.push({type: type, level: level, message: message, messages: target[pMessages], target: target});
}

class Base {
	[pValidateParams](level, message){
		level = Number(level);
		if(level < this.constructor.MIN_LEVEL || this.constructor.MAX_LEVEL < level){
			throw new Error("level must be minimum and maxium level");
		}
		return [level, message ? (message + "").trim() : ""];
	}

	[pMutate](proc){
		try{
			qlock++;
			proc();
		}finally{
			qlock--;
			let e = 0;
			while(!qlock && eventQ.length){
				let event = eventQ.pop();
				try{
					event.target.bdNotify(event);
				}catch(_e){
					e = _e;
				}
			}
			if(e){
				// eslint-disable-next-line no-unsafe-finally
				throw(e);
			}
		}
	}

	[pAddMessage](level, message){
		// no duplicates allowed
		// empty message is converted to default message given by ctor
		let levelMessages = this[pMessages][level];
		if(!levelMessages){
			this[pMessages][level] = message;
			return true;
		}else{
			if(Array.isArray(levelMessages)){
				if(levelMessages.indexOf(message) === -1){
					levelMessages.push(message);
					return true;
				}
			}else if(levelMessages !== message){
				this[pMessages][level] = [levelMessages, message];
				return true;
			}
			return false;
		}
	}

	[pDelMessage](level, message){
		let messages = this[pMessages];
		let levelMessages = messages[level];
		if(levelMessages){
			if(Array.isArray(levelMessages)){
				let index = levelMessages.indexOf(message);
				if(index !== -1){
					levelMessages.splice(index, 1);
					if(!levelMessages.length){
						delete messages[level];
					}
					return true;
				}// else no-op
			}else if(levelMessages === message){
				delete messages[level];
				return true;
			}
		}
		return false;
	}
}

//
// VStat - validation status
//
// manages a validation status and associated messages and className
//
class VStat extends eventHub(watchHub(Base)) {
	constructor(level, message){
		super();
		this[pMessages] = [];
		if(level === undefined){
			// default ctor
			level = this[pLevel] = this.constructor.MIN_LEVEL;
			this[pMessages][level] = this.constructor.levels[level].message;
		}else if(typeof level === "string"){
			message = level.trim();
			level = this.constructor.MAX_LEVEL;
			this[pLevel] = level = this.constructor.MAX_LEVEL;
			this[pMessages][level] = message || this.constructor.levels[level].message;
		}else if(level instanceof this.constructor){
			// copy constructor
			let src = level;
			this[pLevel] = src[pLevel];
			this[pMessages] = src[pMessages].map(item => Array.isArray(item) ? item.slice() : item);
		}else if(level instanceof Object){
			let src = level;
			let args = "level" in src ? [src.level, src.message] : [src.message];
			src = new this.constructor(...args);
			this[pLevel] = src[pLevel];
			this[pMessages] = src[pMessages];
		}else{
			level = Number(level);
			if(this.constructor.MIN_LEVEL <= level && level <= this.constructor.MAX_LEVEL){
				if(message){
					message = (message + "").trim();
				}
				message = message || this.constructor.levels[level].message;
			}else{
				throw new Error("level must be MIN_LEVEL<=level<=MAX_LEVEL");
			}
			this[pLevel] = level;
			this[pMessages][level] = message;
		}
	}

	get level(){
		return this[pLevel];
	}

	get message(){
		let msg = this[pMessages][this[pLevel]];
		if(!msg){
			return "";
		}else if(Array.isArray(msg)){
			return msg.join("\n");
		}else{
			return msg;
		}
	}

	get className(){
		return this.constructor.levels[this[pLevel]].className;
	}

	get isLegal(){
		return this[pLevel] < this.constructor.ERROR_LEVEL;
	}

	get isScalarLegal(){
		return this[pLevel] < this.constructor.SCALAR_ERROR_LEVEL;
	}

	get isError(){
		return this[pLevel] >= this.constructor.ERROR_LEVEL;
	}

	get isValid(){
		return this[pLevel] === VALID;
	}

	get isContextInfo(){
		return this[pLevel] === CONTEXT_INFO;
	}

	get isScalarInfo(){
		return this[pLevel] === SCALAR_INFO;
	}

	get isContextWarn(){
		return this[pLevel] === CONTEXT_WARN;
	}

	get isScalarWarn(){
		return this[pLevel] === SCALAR_WARN;
	}

	get isContextError(){
		return this[pLevel] === CONTEXT_ERROR;
	}

	get isScalarError(){
		return this[pLevel] === SCALAR_ERROR;
	}

	get hasValidMessages(){
		return !!this[pMessages][VALID];
	}

	get hasContextInfoMessages(){
		return !!this[pMessages][CONTEXT_INFO];
	}

	get hasScalarInfoMessages(){
		return !!this[pMessages][SCALAR_INFO];
	}

	get hasContextWarnMessages(){
		return !!this[pMessages][CONTEXT_WARN];
	}

	get hasScalarWarnMessages(){
		return !!this[pMessages][SCALAR_WARN];
	}

	get hasContextErrorMessages(){
		return !!this[pMessages][CONTEXT_ERROR];
	}

	get hasScalarErrorMessages(){
		return !!this[pMessages][SCALAR_ERROR];
	}

	getMessages(level, separator){
		let m = this[pMessages][level];
		return !m ? "" : (Array.isArray(m) ? m.join(separator || "\n") : m);
	}

	getMessagesRaw(level){
		// guaranteed to return an array
		let m = this[pMessages] && this[pMessages][level];
		return (Array.isArray(m) ? m : (m ? [m] : [])).slice();
	}



	set(level, message){
		// forces exactly message at level; if message is missing, then the default message is provided
		[level, message] = this[pValidateParams](level, message);
		this[pMutate](() => {
			if(!message){
				message = this.constructor.levels[level].message;
			}
			let current = this[pMessages][level];
			if(Array.isArray(current)){
				current.forEach(msg => {
					if(msg !== message){
						pushQ("messageDel", level, msg, this);
					}
				});
				if(current.indexOf(message) === -1){
					pushQ("messageIns", level, message, this);
				}
				this[pMessages][level] = message;
			}else if(current !== message){
				if(current){
					pushQ("messageDel", level, current, this);
				}
				pushQ("messageIns", level, message, this);
				this[pMessages][level] = message;
			}// else current!==message; therefore, no change
			if(level > this[pLevel]){
				this.bdMutate("level", pLevel, level);
			}// else no level change
		});
	}

	addMessage(level, message){
		[level, message] = this[pValidateParams](level, message);
		if(!message){
			return;
		}
		this[pMutate](() => {
			if(level > this[pLevel]){
				this.set(level, message);
			}else if(this[pAddMessage](level, message)){
				pushQ("messageIns", level, message, this);
			}
		});
	}

	delMessage(level, message){
		[level, message] = this[pValidateParams](level, message);
		if(!message){
			return;
		}
		this[pMutate](() => {
			if(this[pDelMessage](level, message)){
				pushQ("messageDel", level, message, this);
				if(!this[pMessages].some(x => x)){
					// always must have at least this...
					message = this.constructor.levels[VStat.VALID].message;
					this[pMessages][VStat.VALID] = message;
					pushQ("messageIns", VStat.VALID, message, this);
				}
				let maxLevel = this[pMessages].reduce((acc, item, level) => item ? level : acc, VALID);
				if(maxLevel !== this[pLevel]){
					this.bdMutate("level", pLevel, maxLevel);
				}
			}
		});
	}

	static eql(lhs, rhs){
		if(!(lhs instanceof VStat) || !(rhs instanceof VStat)){
			return false;
		}
		if(lhs[pLevel] !== rhs[pLevel]){
			return false;
		}
		let lhsMessages = lhs[pMessages];
		let rhsMessages = rhs[pMessages];
		for(let i = 0, end = lhsMessages.length; i < end; i++){
			let lhsLevelMessages = lhsMessages[i];
			let rhsLevelMessages = rhsMessages[i];
			// either undefined, string or array
			if(lhsLevelMessages === undefined || typeof lhsLevelMessages === "string"){
				if(lhsLevelMessages !== rhsLevelMessages){
					return false;
				}
			}
			// lhsLevelMessages is an array
			if(!Array.isArray(rhsLevelMessages) || lhsLevelMessages.length !== rhsLevelMessages.length){
				return false;
			}
			// both arrays of the same size; see if there is a string in the left not in the right
			if(lhsLevelMessages.some(lMessage => !rhsLevelMessages.some(rMessage => lMessage === rMessage))){
				return false;
			}
		}
		return true;
	}

	static valid(message){
		return new VStat(VALID, message);
	}

	static contextInfo(message){
		return new VStat(CONTEXT_INFO, message);
	}

	static scalarInfo(message){
		return new VStat(SCALAR_INFO, message);
	}

	static contextWarn(message){
		return new VStat(CONTEXT_WARN, message);
	}

	static scalarWarn(message){
		return new VStat(SCALAR_WARN, message);
	}

	static contextError(message){
		return new VStat(CONTEXT_ERROR, message);
	}

	static scalarError(message){
		return new VStat(SCALAR_ERROR, message);
	}
}

eqlComparators.set(VStat, VStat.eql);

Object.assign(VStat, {
	MIN_LEVEL: VALID,
	VALID: VALID,
	CONTEXT_INFO: CONTEXT_INFO,
	SCALAR_INFO: SCALAR_INFO,
	CONTEXT_WARN: CONTEXT_WARN,
	SCALAR_WARN: SCALAR_WARN,
	CONTEXT_ERROR: CONTEXT_ERROR,
	SCALAR_ERROR: SCALAR_ERROR,
	MAX_LEVEL: SCALAR_ERROR,

	ERROR_LEVEL: CONTEXT_ERROR,
	SCALAR_ERROR_LEVEL: SCALAR_ERROR,

	// remember: a level *must* have at least one message to stay at that level
	// therefore, define one such default message for each level
	levels: [
		{id: "valid", className: "bd-vStat-valid", message: "valid"},
		{id: "contextInfo", className: "bd-vStat-contextInfo", message: "information in context"},
		{id: "scalarInfo", className: "bd-vStat-scalarInfo", message: "information"},
		{id: "contextWarn", className: "bd-vStat-contextWarn", message: "warning in context"},
		{id: "scalarWarn", className: "bd-vStat-scalarWarn", message: "warning"},
		{id: "contextError", className: "bd-vStat-contextError", message: "error in context"},
		{id: "scalarError", className: "bd-vStat-scalarError", message: "error"}
	],

	// privates...
	pLevel: pLevel,
	pMessages: pMessages,
	pClassName: pClassName,
	pMutate: pMutate,
	pValidateParams: pValidateParams,
	pAddMessage: pAddMessage,
	pDelMessage: pDelMessage
});

// We use macros in this library! Hopefully...eventually...JavaScript will have what lisp has had for 50 years!

// For those who think this is dangerous, notice that the only time code from one of these functions is actually eval'd is
// is after applying said function in our own code. Therefore, we completely control the output code by completely controlling
// the input. Put another way: the only way to make these macro generators do something wrong is to change the code that
// calls them. Of course eval is not any more "dangerous" than changing executing code.

function defReadOnly(name){
	return `
	get(){
		return "${name}" in this.kwargs ?
				this.kwargs.${name} :
				this.constructor.${name};
	}`;
}

function defReadOnlyWithPrivate(name, pName){
	return `
	get(){
		return "${pName}" in this ?
			this.${pName} :
			("${name}" in this.kwargs ?
				this.kwargs.${name} :
				this.constructor.${name});
	}`;
}

function defSetter(name, pName){
	return `
	set(value){
		if(this.${name}!==value){
			this.bdMutate("${name}", "${pName}", value);
		}
	}`;
}

function defMutable(name, pName){
	return defReadOnlyWithPrivate(name, pName)+ ","+ defSetter(name, pName);
}

function defProps(theClass, props){
	let propsDefExpr = props.map(prop => {
		let [type, name, pName] = prop;
		let macro;
		if(type=="rw"){
			macro = defMutable;
		}else{
			macro = pName ? defReadOnlyWithPrivate : defReadOnly;
		}
		let getterSetterExpr = macro(name, pName);
		return `"${name}":{${getterSetterExpr}}`;
	}).join(",\n");
	let code = `(Object.defineProperties(${theClass}.prototype, {
	${propsDefExpr}
}))`;
	return code;
}

class Button extends Component.withWatchables("label") {
	constructor(kwargs){
		super(kwargs);
		if(this.label === undefined){
			this.label = "";
		}
		kwargs.handler && (this.handler = kwargs.handler);
	}

	// protected API...

	bdElements(){
		//
		// 1  div.bd-button [bd-disabled] [bd-focused] [bd-hidden]
		// 2      div
		// 3          <this.label>
		//
		return element("div", {className: "bd-button", bdAdvise: {click: "bdOnClick", mousedown: "bdOnMouseDown"}},
			element("div", {tabIndex: 0, bdReflect: "label"})
		);
	}

	// private API...

	bdOnFocus(){
		if(!this.bdKeyHandler){
			this.bdKeyHandler = connect(this._dom.root, "keypress", (e) => {
				if(e.charCode == 32){
					// space bar => click
					this.bdOnClick(e);
				}
			});
		}
		super.bdOnFocus();
	}

	bdOnBlur(){
		super.bdOnBlur();
		this.bdKeyHandler && this.bdKeyHandler.destroy();
		delete this.bdKeyHandler;
	}

	bdOnClick(e){
		stopEvent(e);
		if(this.enabled){
			if(!this.hasFocus){
				this.focus();
			}
			this.handler && this.handler();
			this.bdNotify({name: "click", nativeEvent: e});
		}
	}

	bdOnMouseDown(e){
		if(this.hasFocus){
			// pressing the left mouse down outside of the label (the focus node) inside the containing div causes
			// the focus to leave the label; we don't want that when we have the focus...
			stopEvent(e);
		}
	}
}

Button.watchables = ["label"].concat(Component.watchables);
Button.events = ["click"].concat(Component.events);

const VALUE = 0;
const TEXT = 1;
const SIFTED = 2;

class ComboList extends Array {
	constructor(kwargs){
		super();
		let list = kwargs.list || [];
		if(Array.isArray(list[0])){
			list.forEach(item => this.push([item[VALUE], item[TEXT] + ""]));
		}else{
			list.forEach(item => this.push([item, item + ""]));
		}

		let sift = this._sift = kwargs.sift || (kwargs.noCase && ((s) => s.trim().toLowerCase()));
		if(sift){
			this.forEach(item => (item[SIFTED] = sift(item[TEXT])));
		}

		function lexicalSort(lhs, rhs){
			if(lhs[TEXT] < rhs[TEXT]){
				return -1;
			}else if(lhs[TEXT] > rhs[TEXT]){
				return 1;
			}else{
				return 0;
			}
		}

		let sortFunc = this.sortFunc =
			typeof kwargs.sort === "function" ? kwargs.sort : (kwargs.sort === false ? false : lexicalSort);
		if(sortFunc){
			this.sort(sortFunc);
		}

		if(kwargs.default){
			this._defaultValue = this.geByValue(kwargs.default);
			if(this._defaultValue[0] === undefined){
				throw new Error("default value does not exist in ComboList");
			}
		}

		this._valueEq = kwargs.valueEq;
	}

	sift(text){
		return this._sift ? this._sift(text) : text.trim();
	}

	get defaultValue(){
		return this._defaultValue || (this.length && this[0][VALUE]) || null;
	}

	getByValue(value){
		if(this._valueEq){
			let eq = this._valueEq;
			for(const item of this){
				if(eq(value, item[VALUE])) return item;
			}
		}else{
			for(const item of this){
				if(value === item[VALUE]) return item;
			}
		}
		return [undefined, undefined];
	}

	getByText(text){
		if(this._sift){
			text = this._sift(text.trim());
			for(const item of this){
				if(text === item[SIFTED]) return item;
			}
		}else{
			text = text.trim();
			for(const item of this){
				if(text === item[TEXT]) return item;
			}
		}
		return [undefined, undefined];
	}

	match(text){
		let siftedTarget = this.sift(text);
		let siftedLength = siftedTarget.length;
		if(!siftedLength){
			return false;
		}
		let match = false;
		if(this._sift){
			for(const item of this){
				if(item[SIFTED].substring(0, siftedLength) === siftedTarget){
					match = item;
					break;
				}
			}
		}else{
			for(const item of this){
				if(item[TEXT].substring(0, siftedLength) === siftedTarget){
					match = item;
					break;
				}
			}
		}
		if(match){
			match = {
				value: match[VALUE],
				text: match[TEXT],
				perfect: match[this._sift ? SIFTED : TEXT] === siftedTarget
			};
			if(!match.perfect){
				// figure out what suffix to put on text to make it a perfect match; to understand, how this can be hard, consider SSNs:
				// Let's say the client design considers 123-45-6789 equal to 123456789 (or, even, the "-" could be any non-digit);
				// then, sifted("123-45")===sifted("123-45-")===sifted("123 45")===sifted("123.45")==="12345". Now the problem...
				// What should the suffix hint be for text="123-45" compared to "123-45-" when the actual entry of "123-45-6789" exists?!
				// Notice "123-45", "123-45-", and "123-45-6789" all have the same sifted prefix, namely "12345". For "123-45", we want
				// the hint "-6789" for "123-45-" we want the hint "6789". Here's how we proceed:
				// 1. Note that "123-45" doesn't contain any sifted characters at the end.
				// 2. Note that "123-45-" does contain sifted characters at the end ("-").
				// 3. Note that the sifted prefix "12345" of the matched value ("123-45-6789") does contain sifted characters at the end ("-").
				// THEREFORE
				// * Since [1] doesn't and [3] does, choose to include [3]'s sifted characters in the hint.
				// * Since [2] does and [3] does, prefer to user's meaningless characters and do not include [3]'s sifted characters in the hint

				// find the minimal  match[TEXT] substring which sifted value === siftedTarget
				let i = siftedLength - 1;
				let matchText = match.text;
				while(i < matchText.length){
					if(this.sift(matchText.substring(0, i + 1)) !== siftedTarget){
						break;
					}
					i++;
				}
				// matchText[0..i] is the minimal prefix that matches the  prefix that sifted text gives
				// matchText[i+1..length-1] is the maximal suffix that can be added to text to make a perfect match

				// find any characters after the minimal substring above that are sifted
				let j = i;
				while(j < matchText.length && this.sift(matchText.substring(0, j + 1)) === siftedTarget) j++;
				// [i+1..j] are the characters in matchText that are actually sifted out (meaningless)
				// matchText[j+1..length-1] is the minimal suffix that can be added to text to make a perfect match

				if(j > i){
					// there are characters in matchText that are actually sifted out after prefix that matches sifted(text)
					// are there any such characters in text?
					if(siftedLength < text.length && this.sift(text.substring(0, siftedLength - 1)) === siftedTarget){
						// there is at least one character at the end of text that would be sifted
						// there are actually sifted out (meaningless) at the end of text
						// BOTH text AND matchText have sifted characters between the prefixes that match and the suffixes that don't
						// therefore...do not add matchText's sifted characters to the hint
						match.suffix = matchText.substring(j, matchText.length);
					}else{
						// no meaningless characters at the end of text; therefore give the hint of everything
						match.suffix = matchText.substring(i, matchText.length);
					}
				}else{
					// no meaningless characters at the end of matchText that matches the prefix that text gives
					match.suffix = matchText.substring(i, matchText.length);
				}
			}
		}
		return match;
	}

	getListBoxParams(text){
		let list = this.map(item => item[TEXT]);
		let match = this.match(text);
		return {list: list, focusedItem: match ? match.text : null};
	}
}

ComboList.VALUE = VALUE;
ComboList.TEXT = TEXT;
ComboList.SIFTED = SIFTED;

function twiddleClass(className, item, oldItem){
	let index = this.bdMap.get(oldItem);
	if(index !== undefined){
		this.children[index].removeClassName(className);
	}
	index = this.bdMap.get(item);
	if(index !== undefined){
		this.children[index].addClassName(className);
	}
}

class ListBox extends Component {
	constructor(kwargs){
		super(kwargs);
		this.bdList = [];
		this.bdMap = new Map();
		this.setList(kwargs.list, kwargs.selectedItem, kwargs.focusedItem);
	}

	setList(list, selectedItem, focusedItem){
		this.focusedItem = null;
		this.selectedItem = null;
		this.bdMutate("mouseOverItem", "bdMouseOverItem", null);
		this.bdUnrenderList();

		list = this.bdList = this.bdConditionList(list);
		selectedItem = this.bdMap.has(selectedItem) ? selectedItem : null;
		focusedItem = this.bdMap.has(focusedItem) ? focusedItem : selectedItem || (list.length ? list[0] : null);
		this[list.length ? "removeClassName" : "addClassName"]("no-data");
		this.rendered && this.bdRenderList();
		this.selectedItem = selectedItem;
		this.focusedItem = focusedItem;
	}

	splice(start, deleteCount, insertItems){
		// note: unlike Array.splice, insertItems is _always_ an array of zero to many items to insert at start

		// juggle for optional deleteCount
		if(Array.isArray(deleteCount)){
			insertItems = deleteCount;
			deleteCount = 0;
		}

		// start===-1 is sugar for this.bdList.length
		let list = this.bdList;
		if(start === -1){
			start = list.length;
		}

		// check args; don' to anything is args irrational
		if(start < 0 || list.length < start){
			throw new Error("start must be in [0..list.length] (start===-1 implies list.length) in ListBox::splice");
		}
		if(deleteCount && start + deleteCount > list.length){
			throw new Error("trying to delete items that don't exist in ListBox::splice");
		}

		let children = this.children;
		let childrenStart = this.bdUpArrow ? start + 1 : start;
		for(let count = 0; count < deleteCount; count++){
			let item = list[start];
			if(item === this.bdSelectedItem){
				this.selectedItem = null;
			}
			if(item === this.bdFocusedItem){
				this.focusedItem = null;
			}
			list.splice(start, 1);
			if(children){
				let child = children[childrenStart];
				delete child.bdItem;
				super.delChild(child, !child.bdListBoxCreated);
			}
		}

		if(insertItems){
			let itemComponentType = this.kwargs.itemComponentType || this.constructor.itemComponentType;
			let ref, posit;
			if(children){
				if(children && children[childrenStart]){
					ref = children[childrenStart].bdDom.root;
					posit = "before";
				}else{
					ref = this.bdItemAttachPoint;
					posit = "last";
				}
			}
			insertItems.map((item, i) => {
				list.splice(start + i, 0, item);
				if(children){
					let child = this.bdInsChild(item, ref, posit, itemComponentType);
					children.pop();
					children.splice(childrenStart + i, 0, child);
				}
			});

		}

		let itemMap = this.bdMap;
		itemMap.clear();
		children.forEach((child, i) => itemMap.set(child.bdItem, i));

		if(list.length){
			this.removeClassName("no-data");
			let focusIndex = start < list.length ? start : list.length - 1;
			this.focusedItem = children[this.bdUpArrow ? ++focusIndex : focusIndex].bdItem;
			if(!this.isInView(this.focusedItem)){
				this.scrollToItem(this.focusedItem, "top");
			}
		}else{
			this.addClassName("no-data");
		}
	}

	get selectedItem(){
		return this.bdSelectedItem;
	}

	set selectedItem(target){
		if(this.selectable){
			this.bdMutate("selectedItem", "bdSelectedItem", this.bdMap.has(target) ? target : null);
		}else{
			throw new Error("can't select item in ListBox that was created with selectable false");
		}
	}

	get focusedItem(){
		let focusedItem = this.bdFocusedItem;
		return focusedItem === this.bdUpArrow || focusedItem === this.bdDownArrow ? null : focusedItem;
	}

	set focusedItem(target){
		let oldValue = this.bdFocusedItem;
		let normalizedOldValue = this.focusedItem;
		let newValue = this.bdFocusedItem = this.bdMap.has(target) ? target : null;
		let normalizedNewValue = this.focusedItem;
		if(normalizedOldValue !== normalizedNewValue){
			this.bdMutateNotify("focusedItem", normalizedNewValue, normalizedOldValue);
		}
		if(this.rendered && oldValue !== newValue){
			twiddleClass.call(this, "focusedItem", newValue, oldValue);
		}
	}

	get mouseOverItem(){
		return this.bdMouseOverItem;
	}

	// illegal to set mouseOverItem

	scrollToItem(item, location){
		let root = this.bdDom.root;
		let scrollHeight = root.scrollHeight;
		let scrollTop = root.scrollTop;
		if(scrollHeight <= root.clientHeight){
			//everything is visible; therefore nothing to scroll
			root.scrollTop = 0;
			return;
		}

		let delta, newScrollTop;
		let children = this.children;
		let posit = getPosit(root);
		let targetPosit = getPosit(children.byItem(item));
		delta = location === "top" ? targetPosit.t - posit.t : targetPosit.b - posit.b;
		delta = Math.min(Math.max(-scrollTop, delta), scrollHeight - scrollTop - posit.h);
		newScrollTop = scrollTop + delta;

		// delta = Math.min(delta, getPosit(children[children.length - 1]).b - posit.b);
		// newScrollTop = Math.min(scrollHeight - posit.h + 1, Math.max(0, scrollTop + delta));

		if(scrollTop !== newScrollTop){
			// eslint-disable-next-line no-undef
			anime({
				targets: root,
				scrollTop: newScrollTop,
				duration: 300,
				easing: "easeOutQuad"
			});
			this.bdMutate("mouseOverItem", "bdMouseOverItem", null);
			return Math.abs(delta) > 1;
		}else{
			return false;
		}
	}

	down(){
		if(!this.rendered || !this.children || !this.bdList.length) return;
		if(this.bdFocusedItem === this.bdList.last){
			if(this.bdDownArrow){
				if(this.scrollToItem(this.bdDownArrow, "bottom"));else{
					// the top arrow was already in view, so get more items
					this.bdOnClickArrow({}, "down");
				}
			}// else, already at the top; nothing to do
			return;
		}else{
			let index = this.bdMap.get(this.bdFocusedItem);
			if(index !== undefined){
				// if the up arrow exists, then the map indexes are +1 of the list indexes
				this.bdUpArrow && (--index);
				this.focusedItem = this.bdList[Math.min(index + 1, this.bdList.length - 1)];
			}else{
				this.focusedItem = this.bdList[0];
			}
		}
		if(!this.isInView(this.bdFocusedItem)){
			this.scrollToItem(this.bdFocusedItem, "bottom");
		}
	}

	up(){
		if(!this.rendered || !this.children) return;
		if(this.bdFocusedItem === this.bdList[0]){
			if(this.bdUpArrow){
				if(this.scrollToItem(this.bdUpArrow, "top"));else{
					// the top arrow was already in view, so get more items
					this.bdOnClickArrow({}, "up");
				}
			}// else, already at the top; nothing to do
			return;
		}else{
			let index = this.bdMap.get(this.bdFocusedItem);
			if(index !== undefined){
				// if the up arrow exists, then the map indexes are +1 of the list indexes
				this.bdUpArrow && (--index);
				this.focusedItem = this.bdList[Math.max(index - 1, 0)];
			}else{
				this.focusedItem = this.bdList[this.bdList.length - 1];
			}
		}
		if(!this.isInView(this.bdFocusedItem)){
			this.scrollToItem(this.bdFocusedItem, "top");
		}
	}

	pageDown(){
		if(!this.rendered || !this.children) return;
		let posit = getPosit(this);
		let newTopChild;
		for(const child of this.children){
			let childPosit = getPosit(child);
			if(childPosit.b > posit.b) break;
			newTopChild = child;
		}
		let newTopItem = newTopChild.bdItem;
		if(this.scrollToItem(newTopItem, "top")){
			this.focusedItem = newTopItem;
		}else{
			this.focusedItem = this.bdList.last;
		}
	}

	pageUp(){
		if(!this.rendered || !this.children) return;
		let posit = getPosit(this);
		let newBottomChild;
		for(let i = this.children.length - 1; i >= 0; i--){
			let child = this.children[i];
			let childPosit = getPosit(child);
			if(childPosit.t < posit.t) break;
			newBottomChild = child;
		}
		let newBottomItem = newBottomChild.bdItem;
		if(this.scrollToItem(newBottomItem, "bottom")){
			this.focusedItem = newBottomItem;
		}else{
			this.focusedItem = this.bdList[0];
		}
	}

	isInView(item){
		let posit = getPosit(this.bdDom.root);
		let targetPosit = getPosit(this.children.byItem(item));
		let result = posit.t <= targetPosit.t && targetPosit.b <= posit.b;
		return result;
	}

	// public overrides...

	insChild(){
		throw new Error("insChild is private in ListBox");
	}

	delChild(){
		throw new Error("delChild is private in ListBox");
	}

	render(proc){
		return super.render(() => {
			this.bdRenderList();
			twiddleClass.call(this, "selectedItem", this.bdSelectedItem, null);
			twiddleClass.call(this, "focusedItem", this.bdFocusedItem, null);
			this.bdMouseOverItem = null;
			this.ownWhileRendered(
				this.watch("selectedItem", twiddleClass.bind(this, "selectedItem")),
				this.watch("mouseOverItem", twiddleClass.bind(this, "mouseOverItem"))
			);
			proc && proc();
		});
	}

	unrender(){
		this.bdUnrenderList();
		super.unrender();
	}

	// protected methods...

	bdElements(){
		//
		// 1  div.bd-listBox [bd-disabled] [bd-focused] [bd-hidden]
		// 2      div
		// 3          div.up-arrow
		// 4          div
		// 5          div.down-arrow
		// 6      div.no-items
		//
		// [1] Component root with Component's capabilities
		// [2, 6] one of [2] or [6] is visible depending on whether or not there are items in the list
		// [3, 5] are visible only if the list is dynamic
		// [4] holds items
		//
		return element("div", {
				className: "bd-listBox unselectable",
				tabIndex: 0,
				bdAdvise: {keydown: "bdOnKeyDown", mousedown: "bdOnMouseDown", mouseover: "bdOnMouseOver"}
			},
			element("div",
				element("div", {className: "items", bdAttach: "bdItemAttachPoint"}),
			),
			element("div", {className: "no-items", bdReflect: "noItemsMessage"})
		);
	}

	bdAttachToDoc(attach){
		if(super.bdAttachToDoc(attach)){
			if(attach && this.bdFocusedItem){
				this.scrollToItem(this.bdFocusedItem, "top");
			}
			return true;
		}
		return false;
	}

	// private API...

	bdInsChild(item, ref, posit, itemComponentType){
		let child;
		if(item instanceof Component){
			child = item;
			if(child.parent){
				child.parent.delChild(child, true);
			}
			// child.bdListBoxCreated remains undefined
		}else{
			child = new itemComponentType(item);
			child.bdListBoxCreated = true;
		}
		child.bdItem = item;
		child.render();
		Component.insertNode(child.bdDom.root, ref, posit);
		this.bdAdopt(child);
		return child;
	}

	bdConditionList(list){
		// this guarantees that list is a *private*, *array* with getter for "last"; initializes pMap
		let result;
		if(!list){
			result = [];
		}else if(!Array.isArray(list)){
			throw new Error("a list provided to ListBox must be an array or false");
		}else if(!list.length){
			result = [];
		}else{
			result = list.slice();
		}

		let map = this.bdMap;
		let offset = this.kwargs.get ? 1 : 0;
		map.clear();
		result.forEach((item, i) => map.set(item, i + offset));

		Object.defineProperty(result, "last", {
			get(){
				return this.length ? this[this.length - 1] : undefined;
			}
		});

		return result;
	}

	bdRenderList(){
		this.children = [];
		this.children.byItem = (item) => {
			return this.children[this.bdMap.get(item)];
		};

		let itemComponentType = this.kwargs.itemComponentType || this.constructor.itemComponentType;
		let arrowComponent = this.kwargs.arrowComponent || this.constructor.arrowComponent;
		let createArrow = (direction) => {
			let arrow = new arrowComponent({direction: direction});
			arrow.bdItem = arrow;
			arrow.bdListBoxCreated = true;
			arrow.render();
			Component.insertNode(arrow.bdDom.root, this.bdItemAttachPoint, direction === "up" ? "first" : "last");
			this.bdMap.set(arrow, this.children.length);
			this.bdAdopt(arrow);
			return arrow;
		};
		this.kwargs.get && (this.bdUpArrow = createArrow("up"));
		this.bdList.forEach((item) => this.bdInsChild(item, this.bdItemAttachPoint, "last", itemComponentType));
		this.kwargs.get && (this.bdDownArrow = createArrow("down"));
	}

	bdUnrenderList(){
		if(this.children) for(const child of this.children.slice()){
			delete child.bdItem;
			super.delChild(child, !child.bdListBoxCreated);
		}
		delete this.bdUpArrow;
		delete this.bdDownArrow;
	}

	bdItemFromNode(node){
		let itemAttachPoint = this.bdItemAttachPoint;
		while(node){
			if(node.parentNode === itemAttachPoint){
				let item = Component.get(node).bdItem;
				return item === this.bdUpArrow || item === this.bdDownArrow ? null : item;
			}
			node = node.parentNode;
		}
		return null;
	}

	bdOnFocus(){
		super.bdOnFocus();
		if(!this.bdFocusedItem && this.bdList.length){
			this.focusedItem = this.bdList[0];
		}
	}

	bdOnKeyDown(e){
		switch(e.keyCode){
			case keys.down:
				this.down();
				break;
			case keys.up:
				this.up();
				break;
			case keys.pageDown:
				this.pageDown();
				break;
			case keys.pageUp:
				this.pageUp();
				break;
			case keys.enter:
				if(this.focusedItem && this.selectable){
					this.selectedItem = this.focusedItem === this.selectedItem ? null : this.focusedItem;
				}
				break;
			default:
				return;
		}
		stopEvent(e);
	}

	bdOnMouseDown(e){
		stopEvent(e);
		if(e.button !== 0){
			return;
		}
		if(this.kwargs.multiSelect);else{
			let item = this.bdItemFromNode(e.target);
			if(item){
				this.focusedItem = item;
				if(this.selectable){
					this.selectedItem = item === this.selectedItem ? null : item;
				}
			}
		}
	}

	bdOnClickArrow(e, direction){
		stopEvent(e);
		this.focusedItem = direction === "up" ? this.bdUpArrow : this.bdDownArrow;
		if(this.kwargs.get){
			let up = direction === "up";
			if((up && this.bdMoreBeforePromise) || (!up && this.bdMoreAfterPromise)){
				// already waiting for data...
				return;
			}
			let ref = up ? "bdMoreBeforePromise" : "bdMoreAfterPromise";
			let p = this[ref] = this.kwargs.get(up ? "before" : "after");
			let className = up ? "query-before" : "query-after";
			this.addClassName(className);
			p.then(newItems => {
				delete this[ref];
				this.removeClassName(className);
				if(newItems && newItems.length){
					this.splice(up ? 0 : -1, 0, newItems);
					if(!up){
						this.focusedItem = this.bdList.last;
						this.scrollToItem(this.bdDownArrow, "bottom");
					}
				}
			});
		}// else ignore
	}

	bdOnMouseOver(e){
		this.bdMutate("mouseOverItem", "bdMouseOverItem", this.bdItemFromNode(e.target));
		let h1 = connect(this.bdDom.root, "mousemove", (e) => {
			this.bdMutate("mouseOverItem", "bdMouseOverItem", this.bdItemFromNode(e.target));
		});
		let h2 = connect(this.bdDom.root, "mouseleave", () => {
			this.bdMutate("mouseOverItem", "bdMouseOverItem", null);
			h1.destroy();
			h2.destroy();
		});
	}
}

eval(defProps("ListBox", [
	["ro", "selectable"],
	["rw", "noItemsMessage", "bdNoItemsMessage"]
]));

Object.assign(ListBox, {
	noItemsMessage: "no items",
	selectable: true,
	itemComponentType: class extends Component {
		constructor(item){
			let kwargs = {text: item + ""};
			super(kwargs);
		}

		bdElements(){
			return element("div", this.kwargs.text);
		}
	},
	arrowComponent: class extends Component {
		constructor(kwargs){
			super(kwargs);
			this.addClassName(kwargs.direction === "up" ? "up-arrow icon-caret-up" : "down-arrow icon-caret-down");
		}

		bdElements(){
			return element("div", {bdAdvise: {click: (e) => this.parent.bdOnClickArrow(e, this.kwargs.direction)}});
		}
	},
	watchables: ["focusedItem", "selectedItem", "mouseOverItem", "noItemsMessage"].concat(Component.watchables),
	events: [].concat(Component.events)
});

class ComboBox extends Component {
	constructor(kwargs){
		super(kwargs);
		this.bdList = kwargs.list instanceof ComboList ? kwargs.list : new ComboList(kwargs);
		let [value, text, vStat] = this.validateValue("value" in kwargs ? kwargs.value : this.bdList.defaultValue);
		this.bdValue = value;
		this.bdText = text;
		this.bdVStat = vStat;
	}

	get closed(){
		// can't use defProps because the value is a calculation
		return this.static || this.kwargs.closed;
	}

	// closed is read-only

	get value(){
		return this.bdValue;
	}

	set value(_value){
		let [value, text, vStat] = this.validateValue(_value);
		this.bdMutate("value", "bdValue", value, "text", "bdText", text, "vStat", "bdVStat", vStat);
	}

	get text(){
		return this.bdText;
	}

	set text(_text){
		let [value, text, vStat] = this.validateText(_text);
		this.bdMutate("value", "bdValue", value, "text", "bdText", text, "vStat", "bdVStat", vStat);
	}

	// setting vStat directly is not allowed...it's done by clients by setting value/text
	// note, however, this returns a reference to vStat, so the internal state of vStat can be manipulated
	get vStat(){
		return this.bdVStat;
	}

	// write-only
	set list(value){
		this.bdList = value instanceof ComboList ? value : new ComboList(Object.assign({}, this.kwargs, {list:value}));
		this.value = this.value;
	}

	// validateValue, validateText: very much analogous to design in ../input/Input.js; no formatter since we have the list

	validateValue(_value){
		let [value, text] = this.bdList.getByValue(_value);
		if(value === undefined){
			if(this.closed){
				throw new Error("illegal value provided for closed combo list");
			}
			return [_value, _value + "", this.closed ? VStat.scalarError() : VStat.valid()];
		}else{
			return [value, text, VStat.valid()];
		}
	}

	validateText(_text){
		let [value, text] = this.bdList.getByText(_text);
		if(value === undefined){
			if(this.closed){
				throw new Error("illegal value provided for closed combo list");
			}
			return [_text, _text + "", this.closed ? VStat.scalarError() : VStat.valid()];
		}else{
			return [value, text, VStat.valid()];
		}
	}

	// protected API/overrides...

	bdElements(){
		//
		// 1  div.bd-comboBox.bd-input [bd-disabled] [bd-focused] [bd-hidden] [vStat.className] [empty]
		// 2      Meta (optional)
		// 3      div
		// 4          input -or- div.static
		// 6      div.arrow.icon-caret-down
		//
		//  1. the component root; the tree rooted at [3] is identical to the tree in the same position for Input
		//     therefore we include the class bd-input so the tree uses the same CSS to get a uniform look
		//  2. optional Meta widget; defined by this.kwargs.Meta || this.constructor.Meta
		//  3. identical to the tree in the same position for Input; see Input
		//  6. the drop-down arrow
		//
		// Notice that [2] can be placed above/below/left/right of [3] by making [1] a flex box (row or column, block or inline)
		// and then setting the flex order of [2], [3], and [6]
		return div({
				//className: "bd-comboBox bd-input",
				className: "bd-comboBox",
				bdReflectClass: [
					"vStat", vStat => vStat.className,
					"text", text => (text.length ? "" : "empty")
				],
				bdAdvise: {
					mousedown: "bdOnMouseDown",
					keydown: "bdOnKeyDown"
				}
			},
			(this.Meta ? element(this.Meta, {bdReflect: {vStat: "vStat"}}) : false),
			div({className: "bd-rbox"},
				this.static ?
					div({className: "bd-static", tabIndex: 0, bdReflect: ["text", (s) => s || "&nbsp;"]}) :
					element("input", Object.assign({
						tabIndex: 0,
						bdAdvise: {input: "bdOnInput"},
						bdReflect: {disabled: "disabled", value: "text", placeholder: "placeholder"},
					}, (this.inputAttrs || this.kwargs.inputAttrs || this.constructor.inputAttrs)))
			),
			div({className: "arrow icon-caret-down"})
		);
	}

	get bdInputNode(){
		return this.bdDom ? this.bdDom.root.querySelector("input") : null;
	}

	// private API/overrides...
	bdOnFocus(){
		super.bdOnFocus();
		if(this.static){
			// no other way to pick a value than from the list; therefore, show it unconditionally
			this.bdDisplayListBox();
		}
	}

	bdOnBlur(){
		super.bdOnBlur();
		if(this.bdListBox){
			this.bdListBox.destroy();
			delete this.bdListBox;
			this.removeClassName("bd-listBox-visible");
		}
		this.bdAcceptInput();
	}

	bdAcceptInput(){
		let inputNode = this.bdInputNode;
		if(inputNode){
			let srcText = inputNode.value;
			let match = this.bdList.match(srcText);
			if(match && match.perfect){
				this.bdMutate("value", "bdValue", match.value, "text", "bdText", match.text, "vStat", "bdVStat", VStat.valid());
			}else if(this.closed){
				// don't allow an illegal value...just revert
				inputNode.value = this.text;
			}else{
				// delegate to the text getter to figure out what to do
				this.text = srcText;
			}
			inputNode.setSelectionRange(0, 0);
		}
	}

	bdDisplayListBox(){
		if(this.bdListBox){
			return;
		}
		let posit = this.bdComputeListBoxPosit();
		if(!posit){
			return;
		}
		let inputNode = this.bdInputNode;
		let text = inputNode ? inputNode.value : this.text;
		let listBox = this.bdListBox = new this.ListBox(this.bdList.getListBoxParams(text));
		listBox.own(listBox.watch("selectedItem", item => {
			if(item !== null){
				this.text = item;
				this.bdListBox.destroy();
				delete this.bdListBox;
				this.removeClassName("bd-listBox-visible");
			}
		}));
		setPosit(listBox, posit);
		document.body.appendChild(listBox.bdDom.root);
		this.addClassName("bd-listBox-visible");
	}

	bdComputeListBoxPosit(){
		let posit = getPosit(this);
		let h = document.documentElement.clientHeight;
		let w = document.documentElement.clientWidth;
		if(posit.b < 0 || posit.t > h || posit.r < 0 || posit.l > w){
			// this combobox is not visible; therefore, do not display the list box
			return false;
		}

		let result = {z: getMaxZIndex(document.body) + 1, w: Math.round(posit.w - (2 * getStyle(this, "borderWidth")))};
		let spaceBelow = h - posit.b;
		if(spaceBelow < 100 && spaceBelow < posit.t){
			// less than 100px below with more available above; therefore, put the list box above the combo box
			result.b = Math.round(posit.t + 1);
			//result.maxH = result.b;
		}else{
			result.t = Math.round(posit.b - 1);
			//result.maxH = h - result.t;
		}
		let spaceRight = w - posit.r;
		if(posit.l < 100 && spaceRight < posit.l){
			// less than 100px right with more available left; therefore, put the list box left the combo box
			result.r = posit.r;
			result.maxW = result.r;
		}else{
			result.l = posit.l;
			result.maxW = w - result.l;
		}
		return result;
	}

	bdOnKeyDown(e){
		let move = (listBox, direction) => {
			if(listBox){
				listBox[direction]();
				let item = listBox.focusedItem;
				if(item){
					let inputNode = this.bdInputNode;
					if(inputNode){
						inputNode.value = item;
						this.bdOnInput({});
					}else{
						this.text = item;
					}
				}
			}
		};

		let listBox = this.bdListBox;
		switch(e.keyCode){
			case keys.down:
				listBox ? move(listBox, "down") : this.bdDisplayListBox();
				break;
			case keys.up:
				move(listBox, "up");
				break;
			case keys.pageDown:
				move(listBox, "pageDown");
				break;
			case keys.pageUp:
				move(listBox, "pageUp");
				break;
			case keys.enter:
				if(listBox && listBox.focusedItem){
					listBox.selectedItem = listBox.focusedItem;
				}else{
					this.bdAcceptInput();
				}
				break;
			case keys.backspace:{
				let inputNode = this.bdInputNode;
				if(inputNode.selectionStart){
					inputNode.value = inputNode.value.substring(0, inputNode.selectionStart - 1);
					this.bdOnInput(e);
					break;
				}else{
					return;
				}
			}
			default:
				return;
		}
		stopEvent(e);
	}

	bdOnMouseDown(e){
		if(e.button !== 0){
			return;
		}
		if(!this.hasFocus){
			this.bdDom.tabIndexNode.focus();
		}
		if(e.target === this.bdDom.root.querySelector(".arrow")){
			if(this.bdListBox){
				if(!this.static){
					this.bdListBox.destroy();
					delete this.bdListBox;
					this.removeClassName("bd-listBox-visible");
				}// else keep static boxes open since that's the only way to edit the value
			}else{
				this.bdDisplayListBox();
			}
			stopEvent(e);
		}
	}

	bdOnInput(e){
		let inputNode = this.bdInputNode;
		let srcText = inputNode.value;
		if(inputNode !== document.activeElement){
			// this is unusual, the input node received input but is not the active (focused) element
			this.text = srcText;
			return;
		}
		let match = this.bdList.match(srcText);
		if(match){
			let matchText = match.text;
			if(!match.perfect){
				inputNode.value = srcText + match.suffix;
				inputNode.setSelectionRange(srcText.length, inputNode.value.length);
			}
			if(this.bdListBox){
				this.bdListBox.focusedItem = matchText;
				if(!this.bdListBox.isInView(matchText)){
					this.bdListBox.scrollToItem(matchText, "top");
				}
			}
		}else if(this.bdListBox){
			this.bdListBox.focusedItem = null;
		}
		this.bdNotify(e);
	}
}

eval(defProps("ComboBox", [
	["ro", "ListBox"],
	["ro", "Meta"],
	["ro", "default"],
	["ro", "static"],
	["ro", "sift"],
	["ro", "noCase"],
	["ro", "sort"],
	["rw", "placeholder", "bdPlaceholder"]
]));

Object.assign(ComboBox, {
		List: ComboList,
		ListBox: class ComboListBox extends ListBox {
			constructor(kwargs){
				kwargs = Object.assign({tabIndex: "", className: "bd-for-combo"}, kwargs);
				super(kwargs);
				this.render();
			}
		},
		Meta: false,
		static: false,
		sift: false,
		noCase: true,
		errorValue: Symbol("error"),
		inputAttrs: {type: "text"},
		placeholder: " enter value ",
		watchables: ["value", "text", "vStat", "placeholder"].concat(Component.watchables),
		events: ["input"].concat(Component.events),
	}
);

const PROMISE_TIME_EXPIRED = Symbol("promise-time-expired");
const PROMISE_CANCELED = Symbol("promise-canceled");
const ppResolver = Symbol("ppResolver");
const ppRejecter = Symbol("ppRejecter");
const ppResolved = Symbol("ppResolved");
const ppRejected = Symbol("ppRejected");
const ppCanceled = Symbol("ppCanceled");
const ppTimeoutHandle = Symbol("ppTimeoutHandle");

function stopTimer(promise){
	if(promise[ppTimeoutHandle]){
		clearTimeout(promise[ppTimeoutHandle]);
		promise[ppTimeoutHandle] = 0;
	}
}

class BdPromise extends Promise {
	constructor(
		timeout, // optional, time in milliseconds before promise is rejected; if missing, then never rejected because of time
		executor // standard promise constructor executor argument: function(resolve, reject)
	){
		if(typeof timeout==="function"){
			// signature is (executor)
			executor = timeout;
			timeout = false;
		}
		if(!executor){
			// signature is () or (timeout)
			executor = function(){
			};
		}

		let resolver = 0;
		let rejecter = 0;
		super(function(_resolver, _rejecter){
			resolver = _resolver;
			rejecter = _rejecter;
		});

		Object.defineProperties(this, {
			[ppTimeoutHandle]: {
				value: 0,
				writable: true
			},
			[ppResolver]: {
				value: resolver,
				writable: true
			},
			[ppRejecter]: {
				value: rejecter,
				writable: true
			},
			[ppResolved]: {
				value: false,
				writable: true
			},
			[ppRejected]: {
				value: false,
				writable: true
			},
			[ppCanceled]: {
				value: false,
				writable: true
			},
		});

		if(timeout){
			let self = this;
			this[ppTimeoutHandle] = setTimeout(function(){
				self[ppTimeoutHandle] = 0;
				self.cancel(PROMISE_TIME_EXPIRED);
			}, timeout);
		}

		executor(
			this.resolve.bind(this),
			this.reject.bind(this)
		);
	}

	cancel(cancelResult){
		if(!this[ppResolved] && !this[ppRejected] && !this[ppCanceled]){
			this[ppCanceled] = true;
			stopTimer(this);
			this[ppRejecter]((this.cancelResult = cancelResult === undefined ? PROMISE_CANCELED : cancelResult));
		}
		return this;
	}

	get resolved(){
		return this[ppResolved];
	}

	get rejected(){
		return this[ppRejected];
	}

	get canceled(){
		return this[ppCanceled];
	}

	resolve(result){
		if(!this[ppResolved] && !this[ppRejected] && !this[ppCanceled]){
			this[ppResolved] = true;
			this.result = result;
			stopTimer(this);
			this[ppResolver](result);
		}
		return this;
	};

	reject(error){
		if(!this[ppResolved] && !this[ppRejected] && !this[ppCanceled]){
			this[ppRejected] = true;
			this.error = error;
			stopTimer(this);
			this[ppRejecter](error);
		}
		return this;
	};
}

BdPromise.promiseTimeExpired = PROMISE_TIME_EXPIRED;
BdPromise.promiseCanceled = PROMISE_CANCELED;

class Dialog extends Component {
	constructor(kwargs){
		super(kwargs);
		this.promise = new BdPromise();
		this.promise.dialog = this;
	}

	get title(){
		return this.bdDialogTitle || "";
	}

	set title(value){
		if(this.bdMutate("title", "bdDialogTitle", value) && this.rendered){
			let titleNode = this.bdDom.root.querySelector(".bd-title");
			if(titleNode){
				titleNode.innerHTML = value;
			}
		}
	}

	onCancel(){
		this.promise.resolve(false);
	}

	onAccepted(){
		this.promise.resolve(true);
	}

	bdElements(){
		let body = this.dialogBody();
		return element("div",
			element("div", {className: "bd-inner"},
				element("div", {className: "bd-title-bar"},
					element("div", {className: "bd-title"}, this.title),
					element("div",
						element(Button, {className: "icon-close", handler: this.onCancel.bind(this)})
					)
				),
				element("div", {className: "bd-body"}, body)
			)
		);
	}
}
Dialog.className = "bd-dialog";

function getDialogPosit(){
	return {
		h: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
		w: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
	};
}

Object.assign(Dialog, {
	className: "bd-dialog",
	watchables: [].concat(Component.watchables),
	events: [].concat(Component.events),
	show(kwargs){
		let theDialog = new this(kwargs);
		render(theDialog, document.body);
		setPosit(theDialog, getDialogPosit());
		setStyle(theDialog, "zIndex", getMaxZIndex(document.body) + 100);
		theDialog.own(viewportWatcher.advise("resize", () => setPosit(theDialog, getDialogPosit())));
		theDialog.promise.then(() => theDialog.destroy());
		return theDialog.promise;
	}
});

class Input extends Component {
	constructor(kwargs){
		super(kwargs);
		"validateValue" in kwargs && (this.validateValue = kwargs.validateValue);
		"validateText" in kwargs && (this.validateText = kwargs.validateText);
		"format" in kwargs && (this.format = kwargs.format);

		let [value, text, vStat] = this.validateValue("value" in kwargs ? kwargs.value : this.default);
		this.bdValue = value;
		this.bdText = text;
		this.bdVStat = vStat;
	}

	get value(){
		return this.bdValue;
	}

	set value(_value){
		let [value, text, vStat] = this.validateValue(_value);
		this.bdMutate("value", "bdValue", value, "text", "bdText", text, "vStat", "bdVStat", vStat);
	}

	get text(){
		return this.bdText;
	}

	set text(_text){
		let [value, text, vStat] = this.validateText(_text);
		this.bdMutate("value", "bdValue", value, "text", "bdText", text, "vStat", "bdVStat", vStat);
	}

	// setting vStat directly is not allowed...it's done by clients by setting value/text
	// note, however, that the internal state of vStat can be manipulated
	get vStat(){
		return this.bdVStat;
	}


	//
	// validateValue, validateText, and format are the key methods that describe the behavior of an input
	// override in subclasses or provide per-instance methods to cause special behavior, e.g., see InputInteger et al
	validateValue(value){
		return [value, this.format(value), VStat.valid()];
	}

	validateText(_text){
		let value = this.trim ? _text.trim() : _text;
		return [value, value, VStat.valid()];
	}

	format(value){
		if(!value){
			return value === 0 ? "0" : "";
		}else{
			try{
				return value.toString();
			}catch(e){
				return this.default;
			}
		}
	}

	// protected API...

	bdElements(){
		//
		// 1  div.bd-input [bd-disabled] [bd-focused] [bd-hidden] [vStat.className] [empty]
		// 2      div
		// 3          input
		// 5      div.vStat
		//
		//  1. the component root
		//  2. typically a relative box, either let input determine its size or style explicitly
		//  3. either cause parent to size or absolute posit (0, 0, 0, 0) off of explicitly-sized parent
		//  4. absolute posit (0, 0, 0, 0) within parent box, which is typically relative
		//  5. may be filled via CSS content (based on vStat.className in root) or explicitly in subclasses based on vStat
		//
		// Notice that [5] can be placed above/below/left/right of [2] bu making [1] a flex box (row or column, block or inline)
		// and then setting the flex order of [2] and [5]

		return div({
				bdReflectClass: [
					"vStat", vStat => vStat.className,
					"text", value => (value.length ? "" : "empty")
				]
			},
			(this.Meta ? element(this.Meta, {bdReflect: {vStat: "vStat"}}) : false),
			div({className: "bd-rbox"},
				element("input", Object.assign({
					tabIndex: 0,
					style: this.kwargs.style || this.kwargs.width || "",
					bdAttach: "bdInputNode",
					bdAdvise: {input: "bdOnInput"},
					bdReflect: {value: "text", disabled: "disabled", placeholder: "placeholder"},
				}, (this.inputAttrs || this.kwargs.inputAttrs || this.constructor.inputAttrs)))
			)
		);
	}

	// private API...
	bdOnBlur(){
		super.bdOnBlur();
		// when the input has the focus, this.text and the input node value may _NOT_ be synchronized since
		// we must allow the user to have unformatted text on the way to inputting legal text. Upon
		// losing focus, this.text and the input node value must again be brought into congruence.
		this.text = this.bdInputNode.value;
	}

	bdOnInput(e){
		let inputNode = this.bdInputNode;
		let srcText = inputNode.value;
		if(inputNode === document.activeElement){
			// allow inputNode.value and this.text to be out of synch when input has the focus (illegal input
			// and/or not formatted); inputNode.value will be put back in synch and the text formatted when the
			// input loses the focus

			// eslint-disable-next-line no-unused-vars
			let [value, text, vStat] = this.validateText(srcText);
			this.bdMutate("value", "bdValue", value, "vStat", "bdVStat", vStat);
		}else{
			this.text = srcText;
		}
		this.bdNotify(e);
	}
}

eval(defProps("Input", [
	["ro", "Meta"],
	["ro", "default"],
	["ro", "trim"],
	["rw", "placeholder", "bdPlaceholder"]
]));

Object.assign(Input, {
	className: "bd-input",
	Meta: false,
	default: "",
	errorValue: Symbol("error"),
	trim: true,
	inputAttrs: {type: "text"},
	placeholder: " enter value ",
	watchables: ["value", "text", "vStat", "placeholder"].concat(Component.watchables),
	events: ["input"].concat(Component.events)
});

class IntegerInput extends Input {
	validateValue(_value){
		let value;
		if(!_value || _value==="false" || value==="0" || (typeof _value==="string" && !_value.trim())){
			return [false, this.format(false), VStat.valid()];
		}else{
			return [true, this.format(true), VStat.valid()];
		}
	}

	validateText(text){
		return this.validateValue(text);
	}

	format(value, checkMap){
		return value ? "true" : "false";
	}
}

class InputNumber extends Input {
	// an input control that accepts an optionally range-limited integer
	set min(value){
		if(this.bdMutate("min", "bdMin", value)){
			// force recalc of vStat
			this.value = this.value;
		}
	}

	set max(value){
		if(this.bdMutate("min", "bdMax", value)){
			// force recalc of vStat
			this.value = this.value;
		}
	}

	set absMin(value){
		if(this.bdMutate("min", "bdAbsMin", value)){
			// force recalc of vStat
			this.value = this.value;
		}
	}

	set absMax(value){
		if(this.bdMutate("min", "bdAbsMax", value)){
			// force recalc of vStat
			this.value = this.value;
		}
	}

	validateValue(value){
		let typeofValue = typeof value;
		if(typeofValue !== "number"){
			if(typeofValue === "string"){
				value = value.trim();
				if(!value){
					value = this.errorValue;
					return [value, this.format(value), VStat.scalarError()];
				}// else, fall through
			}
			value = Number(value);
			if(Number.isNaN(value)){
				let value = this.errorValue;
				return [value, this.format(value), VStat.scalarError()];
			}
		}
		let text = this.format(value);
		if(value < this.absMin){
			return [this.errorValue, text, VStat.scalarError(this.absMinMsg)];
		}else if(value < this.min){
			return [value, text, VStat.scalarWarn(this.minMsg)];
		}else if(value > this.absMax){
			return [this.errorValue, text, VStat.scalarError(this.absMaxMsg)];
		}else if(value > this.max){
			return [value, text, VStat.scalarWarn(this.maxMsg)];
		}else{
			return [value, text, VStat.valid()];
		}
	}

	validateText(text){
		return this.validateValue(text + "");
	}

	format(value){
		return value === this.errorValue ? "?" : value + "";
	}
}

eval(defProps("InputNumber", [
	["ro", "errorValue"],

	["rw", "minMsg", "bdMinMsg"],
	["rw", "maxMsg", "bdMaxMsg"],
	["rw", "absMinMsg", "bdAbsMinMsg"],
	["rw", "absMaxMsg", "bdAbsMaxMsg"],

	// only define the getters since the setters are defined explicitly to force a recalc of vStat
	["ro", "min", "bdMin"],
	["ro", "max", "bdMax"],
	["ro", "absMin", "bdAbsMin"],
	["ro", "absMax", "bdAbsMax"]
]));

Object.assign(InputNumber, {
	min: Number.NEGATIVE_INFINITY,
	max: Number.POSITIVE_INFINITY,
	absMin: Number.NEGATIVE_INFINITY,
	absMax: Number.POSITIVE_INFINITY,
	minMsg: "value is less than typical minimum",
	maxMsg: "value is greater than typical maximum",
	absMinMsg: "value is less than absolute minimum",
	absMaxMsg: "value is greater than absolute maximum",
});

class InputInteger extends InputNumber {
	// an input control that accepts an optionally range-limited integer

	validateValue(_value){
		let [value, text, vStat] = super.validateValue(_value);
		return [value === this.errorValue ? value : Math.round(value), text, vStat];
	}
}

class InputMap extends Input {
	constructor(kwargs){
		super(kwargs);
		if(!kwargs.valueMap && !this.constructor.valueMap){
			throw new Error("A valueMap must be provided to InputMap")
		}
		if(!kwargs.textMap && !this.constructor.textMap){
			throw new Error("A textMap must be provided to InputMap")
		}
	}

	// read-only
	get valueMap(){
		return this.kwargs.valueMap || this.constructor.valueMap;
	}

	get textMap(){
		return this.kwargs.textMap || this.constructor.textMap;
	}

	validateValue(_value){
		let valueMap = this.valueMap;
		if(valueMap.has(_value)){
			let value = valueMap.get(_value);
			return [value, this.format(value), VStat.valid()];
		}else{
			return [_value, _value + "", VStat.scalarError()];
		}
	}

	validateText(_text){
		_text = _text.trim();
		let textMap = this.textMap;
		if(textMap.has(_text)){
			let value = textMap.get(_text);
			return [value, this.format(value), VStat.valid()];
		}else{
			return [undefined, _text, VStat.scalarError()];
		}
	}

	format(value){
		return (this.formatter || this.kwargs.formatter || this.constructor.formatter)(value);
	}
}

InputMap.formatter = value => value === null || value === undefined ? "" : value + "";

class Labeled extends Component.withWatchables("label") {
	bdElements(){
		return element("div", {bdAttach: "bdChildrenAttachPoint"},
			element("label", {bdReflect: "label"})
		);
	}
}
Labeled.className = "bd-labeled";

class Meta extends Component {
	constructor(){
		super({});
		this.bdVStat = VStat.valid();
	}

	get vStat(){
		return this.bdVStat;
	}

	set vStat(value){
		if(!VStat.eql(this.bdVStat, value)){
			this.bdMutate("vStat", "bdVStat", value);
		}
	}

	bdElements(){
		return element("div", {
			className: "bd-meta icon-",
			bdReflect: {title: ["vStat", (v) => v.message]},
			bdReflectClass: ["vStat", (v) => v.className]
		});
	}
}

Object.assign(Meta, {
	watchables: ["vStat"].concat(Component.watchables),
	events: [""].concat(Component.events)
});

class States {
	constructor(owner, states, value){
		this.owner = owner;
		this.reset(states, value);
	}

	reset(states, value){
		this.currentState && this.owner.removeClassName(this.className);
		this.states = states;
		let index = this.findIndex(value);
		this.currentState = states[index !== -1 ? index : 0];
		this.owner.addClassName(this.className);
	}

	get state(){
		return this.currentState;
	}

	set value(value){
		let index = this.findIndex(value);
		if(index === -1){
			// eslint-disable-next-line no-console
			console.error("unexpected, but ignored");
		}else{
			this.owner.removeClassName(this.className);
			this.currentState = this.states[index];
			this.owner.addClassName(this.className);
		}
	}

	get value(){
		return this.currentState.value;
	}

	get className(){
		return this.currentState.className;
	}

	get label(){
		return this.currentState.label;
	}

	get mark(){
		return this.currentState.mark;
	}

	findIndex(value){
		return this.states.findIndex((state) => state.value === value);
	}

	exists(value){
		return this.findIndex(value) !== -1;
	}

	nextValue(){
		return this.states[(this.findIndex(this.value) + 1) % this.states.length].value;
	}
}

const DEFAULT_2_STATE_VALUES = [false, true];
const DEFAULT_3_STATE_VALUES = [null, true, false];

function valuesToStates(values){
	return values.map(value => ({value: value, mark: value + ""}));
}

class StateButton extends Button {
	constructor(kwargs){
		// note that we keep the handler feature, but watching "value" is likely much more useful
		super(kwargs);

		let states = kwargs.states || valuesToStates(kwargs.values);
		if(!Array.isArray(states)){
			throw new Error("illegal states");
		}
		Object.defineProperties(this, {
			bdStates: {
				value: new States(this, this.bdConditionStates(states), kwargs.value)
			}
		});
	}

	get value(){
		return this.bdStates.value;
	}

	set value(value){
		if(!this.bdStates.exists(value)){
			// eslint-disable-next-line no-console
			console.warn("illegal value provided; ignored");
		}else{
			let oldValue = this.value;
			if(value !== oldValue){
				let oldState = this.bdStates.state;
				this.bdStates.value = value;
				this.bdMutateNotify("value", value, oldValue);
				this.bdMutateNotify("state", this.bdStates.state, oldState);
			}
		}
	}

	get states(){
		// deep copy
		return this.bdStates.states.map(state => Object.assign({}, state));
	}

	get state(){
		// deep copy
		return Object.assign({}, this.bdStates.state);
	}

	reset(states, value){
		if(!Array.isArray(states)){
			throw new Error("illegal states");
		}else{
			this.bdStates.reset(this.bdConditionStates(states), value);
			this.bdMutateNotify("value", this.value, undefined);
			this.bdMutateNotify("state", this.value, undefined);
		}
	}

	// protected API...

	bdElements(){
		let labelText = (state) => {
			let label = state.label;
			return label !== undefined ? (label ? label : "") : (this.label !== undefined ? this.label : "");
		};

		let markText = (state) => {
			let mark = state.mark;
			return mark !== undefined ? (mark ? mark : "") : "";
		};

		return element("div", {tabIndex: -1, bdAdvise: {click: this.bdOnClick.bind(this)}},
			element("div",
				element("div", {bdReflect: ["state", labelText]}),
				element("div", {bdReflect: ["state", markText]})
			)
		);
	}

	// private API...

	bdConditionStates(value){
		return value.map((state, i) => {
			let result = {
				value: "value" in state ? state.value : i,
				className: "className" in state ? state.className : "state-" + i,
				mark: state.mark || "",
			};
			if("label" in state){
				result.label = state.label;
			}
			return result;
		});
	}

	bdOnClick(e){
		// override Button's Button.bdOnClick
		stopEvent(e);
		if(this.enabled){
			this.value = this.bdStates.nextValue();
			this.handler && this.handler();
			this.bdNotify({name: "click", e: e});
		}
	}
}
Object.assign(StateButton, {
	className: "bd-state-button",
	watchables: ["value", "state"].concat(Button.watchables),
	events: ["value", "state"].concat(Button.events)
});

function valuesToStatesNoMark(values){
	return values.map(value => ({value: value}));
}

function getStates(_states, nullable, nullMark, falseMark, trueMark){
	function setDefaults(dest, className, mark){
		if(!("className" in dest)){
			dest.className = className;
		}
		if(!("mark" in dest)){
			dest.mark = mark;
		}
	}

	let states;
	if(_states){
		if((nullable && _states.length !== 3) || (!nullable && _states.length !== 2)){
			throw new Error("illegal states");
		}else{
			// valid states provided...
			states = _states;
		}
	}else{
		states = valuesToStatesNoMark(nullable ? DEFAULT_3_STATE_VALUES : DEFAULT_2_STATE_VALUES);
	}

	let i = 0;
	if(nullable){
		setDefaults(states[i++], "state-null", nullMark);
	}
	setDefaults(states[i++], "state-false", falseMark);
	setDefaults(states[i++], "state-true", trueMark);
	return states;
}

StateButton.Checkbox = class CheckBox extends StateButton {
	constructor(kwargs){
		super(Object.assign({}, kwargs, {
			states: getStates(
				kwargs.values ? valuesToStatesNoMark(kwargs.values) : kwargs.states,
				kwargs.nullable,
				"?", " ", "X"
			)
		}));
		this.addClassName("checkbox");
	}
};


StateButton.RadioButton = class RadioButton extends StateButton {
	constructor(kwargs){
		super(Object.assign({}, kwargs, {
			states: getStates(
				kwargs.values ? valuesToStatesNoMark(kwargs.values) : kwargs.states,
				kwargs.nullable,
				"\u{e912}", "\u{e912}", "\u{e911}"
			)
		}));
		this.addClassName("radio-button");
	}
};

let cl = window.cl;

export { cl, keys, Button, ComboBox, ComboList, Dialog, Input, IntegerInput as InputBoolean, InputInteger, InputMap, InputNumber, Labeled, ListBox, Meta, StateButton, element as e, element, version, insPostProcessingFunction, replacePostProcessingFunction, getPostProcessingFunction, Element, div, svg, destroyable, destroyAll, eventHub, EventHub, eqlComparators, eql, UNKNOWN_OLD_VALUE, STAR, OWNER, OWNER_NULL, PROP, silentSet, WatchableRef, getWatchableRef, watch, toWatchable, fromWatchable, watchHub, WatchHub, isWatchable, withWatchables, bind, biBind, initialize, Component, render, Collection, CollectionChild, getAttributeValueFromEvent, setAttr, getAttr, getComputedStyle, getStyle, getStyles, setStyle, setPosit, create, insert, hide, show, getPosit, getMaxZIndex, destroyDomChildren, destroyDomNode, connect, stopEvent, focusManager, viewportWatcher };
