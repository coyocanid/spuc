let matches = [];
const match = ( str, regex ) => {
    matches.length = 0;
    let m = str.match( regex );
    if( m ) {
	    m.forEach( ma => matches.push(ma) );
	    return true;
    }
    return false;
}

const toArry = alike => {
    let ret = [];
    for( let i=0; i<alike.length; i++ ) {
        ret.push( alike[i] );
    }
    return ret;
};
const byId = id => document.getElementById(id);
const byClass = cls => toArry( document.getElementsByClassName(cls) );
const byTag = tag => toArry( document.getElementsByTagName(tag) );

const transform = (obj,cache) => {
    if( typeof obj === 'string' ) {
        if( obj.substr(0,1) === 'r' )
            return cache[ obj.substr(1) ];
        else
            return obj.substr(1);
    } else {
        for( let k in obj ) {
            if( k !== 'id' ) {
                let v = obj[k];
                if( typeof v === 'string' ) {
                    if( v.substr(0,1) === 'r' )
                        obj[k] = cache[ v.substr(1) ];
                    else
                        obj[k] = v.substr(1);
                } else if( typeof v === 'object' ) {
                    transform( obj[k], cache );
                }
            }
        }
    }
    return obj;
};

const args_to_output = item => {
    let ret;
    if( typeof item === 'object' ) {
        if( item.id )
            return 'r' + item.id;
        else if( Array.isArray( item ) )
            return item.map( it => args_to_output(it) );
        ret = {};
        Object.keys( item ).forEach( k => ret[k] = args_to_output(item[k]) );
        return ret;
    } else if( item !== undefined ) {
        ret = 'v' + item;
    } else {
        ret ='u';
    }
//    console.log(item,ret,"ATO");
    return ret;
};
const act = (act_args) => {
    let { app,action,args,cache,files,sess_id } = act_args;
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.responseType = 'json';
        xhr.open( 'POST', '/cgi-bin/adapter.cgi' );
//        xhr.open( 'POST', '/app' );
        xhr.onload = () => {
            if( xhr.status === 200 ) {
                if( xhr.response.succ ) {
                    // xhr response succ, data, error, ret
                    let payload = xhr.response.ret;
                    let data = xhr.response.data;
//                    console.log( payload, "RECEIVED" );

                    if( false && cache ) {

                        Object.keys( data ).forEach( id => {
                            let part = data[id];

                            // add it to the cache
                            if( ! cache[id] ) cache[id] = part;
                            let cached = cache[id];

                            // remove extranious old keys
                            let cachedKeys = {};
                            Object.keys( part ).forEach( k => cachedKeys[k] = true );
                            Object.keys( cached ).forEach( k => {
                                delete cachedKeys[k];
                            } );
                            Object.keys( cachedKeys ).forEach( k => {
                                delete cached[k];
                            } );

                            // now clean up the references
                            Object.keys( data ).forEach( id => {
                                let part = cache[id];
                                Object.keys( part ).forEach( k => {
                                    let v = part[k];
                                    if( v.substr(0,1) === 'r' )
                                        part[k] = cache[ v.substr(1) ];
                                    else
                                        part[k] = v.substr(1);
                                } );
                                part.id = id;
                            } );
                            
                        } );

                    } else {
                        
                        Object.keys( data ).forEach( id => {
                            let part = data[id];
                            Object.keys( part ).forEach( k => {
                                let v = part[k];
                                if( v.substr(0,1) === 'r' )
                                    part[k] = data[ v.substr(1) ];
                                else
                                    part[k] = v.substr(1);
                            } );
                            part.id = id;
                        } );

                    }
                    payload = transform( payload, data );
                    resolve(payload);
                }
                else
                    reject(xhr.response.error);
            } else {
                reject('unknown');
            }
        }
        xhr.onerror = () => reject(xhr.statusText);
        args = args_to_output( args );
//        console.log( action, args, "SENDING" );
        let fd = new FormData();
        fd.append( 'payload', JSON.stringify({ app, action, args, sess_id }) );
        if( files ) {
            fd.append( 'files', files.length );
            for( let i=0; i<files.length; i++ )
                fd.append( 'file_' + i, files[i] );
        }
        xhr.send(fd);
    } );
};

/*
  Paginator

   The paginator provides a moveable window for a list, caching parts of the list.

   For example, there might be a list of 10M log entries on the server.
   The Paginator might cache 1000 items at a time, and provide 100 at a time and
   be able to cache up to 100_000 items.

   Since the actual list may be remote, the paginator methods
   (except for has_next and has_prev) return Promise objects.

   Usage :
      // provided by you to contact the server. The server
      // must respond with { segment, list_size } where
      // segment is part of the full array.
      let pagefun = (start_idx,amount_to_get) => {
         return new Promise( resolve => {
           ... contact server or something ...
           resolve( { segment => [..part of big list], list_size => size of big list } );
         } );
      };

      let p = new Paginator( { pagefun,
                               fetch_chunk_size : 1000,
                               window_size: 100,
                               max_cached : 100000 } );

      // optionally provided by you, something that watches for
      // changes in the pagination window.
      yourWatchFunction( p.window );

      // set to the start of the list
      p.start().then( win => {
          // win === p.window
          // provided here if you want to use this rather than
          / a watch function
      } );

      p.has_next();  // check if it is not at the end

      // promises
      p.next();      // next window_size entries
      p.next( 500 ); // like a fast forward


      p.has_prev();  // check if it is not at the beginning

      // promises
      p.prev();      // prev window_size entries
      p.prev( 500 ); // like a fast rewind

      // promise to set an arbitrary position
      p.set_position( 123124 );

*/
class Paginator {
    constructor(args) {
        this.pagefun          = args.pagefun;

        this.fetch_chunk_size = args.fetch_chunk_size || 500;
        this.max_cached       = args.max_cached  || 0; // 0 is no limit
        this.window_size      = args.window_size || 10;

        this.cache = [];  //cached part of list
        this.window = []; // <--- *** the field to watch and show on the UI ***

        // these are all indexes
        this.cache_end     = undefined;
        this.cache_start   = undefined;
        this.window_start  = undefined;
        this.window_end    = undefined;
        
        this.list_last_idx = undefined; //last index of the actual list

        if( this.cache_size != 0 && this.max_cached < 3*this.fetch_chunk_size ) {
            throw new Exception( "Paginator constructed with a cache size less than three times the fetch size" );
        }

        // server returns 1, { segment => [part of list], list_size => @$list }
    }
    start(idx) {
        this.reset();
        return this.set_position(idx||0);
    }
    reset() {
        this.cache.splice(0);
        this.window.splice(0);
        this.window_start  = undefined;
        this.list_last_idx = undefined;
        this.cache_end     = undefined;
        this.cache_start   = undefined;
    }
    next_idx() {
        return this.window_start + this.window_size;
    }
    prev_idx() {
        let prev = this.window_start - this.window_size;
        return prev < 0 ? 0 : prev;
    }
    has_next() {
        return this.window_end < this.list_last_idx;
    }
    has_prev() {
        return this.window_start > 0;
    }
    next( more ) {
        if( !this.has_next() ) return new Promise( (res) => res(this.window) );
        more = more ? more : this.window_size;
        return this.set_position( this.window_start + more );
    }
    prev( more ) {
        if( !this.has_prev() ) return new Promise( (res) => res(this.window) );
        more = more ? more : this.window_size;
        let newpos = this.window_start - more;
        if( newpos < 0 ) newpos = 0;
        return this.set_position( newpos );
    }
    refresh() {
        return this.set_position( this.window_start );
    }
    set_position( start_point ) {
        let end_point = (start_point + this.window_size)-1;
                
        if( start_point < 0 )
            start_point = 0;

        if( this.list_last_idx !== undefined ) {
            if( start_point > this.list_last_idx )
                start_point = this.list_last_idx;
            if( end_point > this.list_last_idx )
                end_point = this.list_last_idx;
        }

        if( isNaN( this.list_last_idx ) || // if we dont know the range or it falls outside the range
            isNaN( this.cache_start ) ||
            isNaN( this.cache_end ) ||
            (start_point <= this.list_last_idx &&
             (end_point > this.cache_end || start_point < this.cache_start )))
        {
            let cache_start = start_point;
            if( ! isNaN( this.list_last_idx ) && start_point < this.cache_start ) {
                cache_start = start_point - this.fetch_chunk_size;
                if( cache_start < 0 )
                    cache_start = 0;
                if( (this.cache_end !== undefined && this.cache_end < (start_point + this.window_size)) )
                    cache_start = start_point;
                else if( (cache_start + this.fetch_chunk_size) < (start_point + this.window_size) )
                    cache_start = start_point;
            }
            return this._load_cache( cache_start, start_point );
        } else {
            return new Promise( (res,rej) => {
                this._adjust_window( start_point );
                res(this.window);
            } );
        }
    } //set_position
    _load_cache( cache_start, win_start ) {
        return new Promise( (resolve, reject) => {
            this.pagefun(cache_start, this.fetch_chunk_size).then( res => {
                
                this.list_last_idx = res.list_size - 1;

                let seg = res.segment;
                // if it hasn't been calculated yet, or the segment does not overlap with the cache
                if( this.cache_start === undefined
                    || cache_start > this.cache_end
                    || (this.cache_start - cache_start) > seg.length )
                {
                    this.cache_start = cache_start;
                    this.cache_end = ( cache_start + seg.length ) - 1;
                    this.cache.splice( 0, this.cache.length, ...seg );
                }
                // else overlap at the beginning
                else if ( cache_start < this.cache_start ) {
                    //  s 1 2 3  (cache_start 1, cache_start 2)
                    // c [ 2 3 4 ], then start at 1
                    // [ 1 2 3 4 ] overlap 0
                    //
                    //  s 2 3 4 (win start 2, cache start 4)
                    // c [ 4 5 6 ]
                    // [ 2 3 4 5 6 ] overlap of 1
                    let gap = this.cache_start - cache_start;
                    this.cache_start = cache_start;
                    this.cache.splice( 0, gap, ...seg );
                    if( this.cache.length > this.max_cached ) {
                        this.cache.length = this.max_cached;
                        this.cache_end = (this.cache_start+ this.cache.length)-1;
                    }
                }
                // else overlap at the end
                else {
                    // cache start = 2
                    // cache end = 6
                    // win start = 5
                    // [ 2 3 4 5 6 ]
                    // +     [ 5 6 7 8 ]
                    //
                    // win start - cache start = 3
                    // 
                    // splice( 3, 2, ...[ 5 6 7 8 ] )
                    this.cache.splice( cache_start - this.cache_start, seg.length, ...seg );
                    this.cache_end = (cache_start + seg.length)-1;
                    if( this.cache.length > this.max_cached ) {
                        let remove = this.cache.length - this.max_cached;
                        this.cache.splice( 0, remove );
                        this.cache_start = this.cache_start - remove;
                    }
                }
                this._adjust_window(win_start);
                resolve(this.window);
            } ).catch( err => reject(err) );
        } );
    } //_load_cache
    _adjust_window(win_start) {
        this.window_start = win_start;
        this.window_end = (this.window_start + this.window_size)-1;
        if( this.window_end > this.list_last_idx )
            this.window_end = this.list_last_idx;

        let slice_start = win_start - this.cache_start;
        let slice_end = slice_start + this.window_size ;
        let win = this.cache.slice( slice_start, slice_end );
        this.window.splice( 0, this.window.length, ...win ); // use splice rather than replace, but probably *could* be replace. try both ways
    } //_adjust_window
} //Paginator

if( false ) { //testy
    let l =[];
    for( let i=0; i<50; i++ ) l.push(i*2);
//    console.log( l, "STARTL" );
    let pf = (sidx,amt) => {
        return new Promise( reso => {
            reso( { segment : l.slice( sidx, sidx+amt ), list_size : l.length } );
        } );
    };

    let p = new Paginator( { pagefun : pf, fetch_chunk_size : 6, window_size : 3, max_cached : 20 } );
    let count = 0;
    let fail_count = 0;

    const is = ( a, b, msg ) => {
        if( a !== b) {
            console.log( "***************************FAILED",
                         a, b, msg );
            return;
        }
        console.log( "PASSED", a, msg );
    }

    const like = ( a, b, msg ) => {
        if( a.length != b.length ) {
            console.log( "***************************FAILED", '['+a.join(",")+']', '['+b.join(",")+']', msg );
            fail_count++;
            return;
        }
        for( let i=0; i<a.length; i++ ) {
            if( a[i] != b[i] ) {
                console.log( "***************************FAILED", '['+a.join(",")+']', '['+b.join(",")+']', msg );
                fail_count++;
                return;
            }
        }
        console.log( "PASSED", '['+a.join(",") +']', msg );
    }

    const showState = win_start => {
        console.log( "WIN " + win_start + ' --> ' + p.window_end );
        console.log( "CA " + p.cache_start + ' --> ' + p.cache_end );
        console.log( "WINDOW  [ " + '   *,'.repeat( win_start) + ' ' + win.map(n => n < 10 ? '0'+n : n ).join(',  ') + ',' + '   *,'.repeat(p.list_last_idx-p.window_end) + "  ]" );
        console.log( "CACHE   [ " + '   *,'.repeat( p.cache_start) + ' ' + p.cache.map(n => n < 10 ? '0'+n : n ).join(',  ') + ',' + '   *,'.repeat(p.list_last_idx-p.cache_end) + "  ]" );
        let str = "INDEX   [  ";
        for( let i=0; i<=p.list_last_idx; i++ ) str = str + ( i < 10 ? '0'+i : i ) + ',  ';
        console.log( str + ']');
        console.log( p.cache_start, p.cache_end, p.list_last_idx );
    };
    
    const doprev = (w) => {
        console.log( count+") CCACHE AT " + p.cache.join(",") );
        console.log( "WWIN AT " + p.window.join(",") );
        if( count > 0 ) {
            let effc = count - 3;
            if( effc === 16 )
                like( w || p.window, [96,98], "Iterationz " + count );
            else if( effc < 0 )
                like( w || p.window, [0,2,4], "Iterationz " + count );
            else
                like( w || p.window, [effc*6,effc*6+2,effc*6+4], "Iterationw " + count );
            --count;
//            D = count == 12 || count == 11;

            p.prev().then( w => {
                doprev(w);
            } );
        } else {
            p.set_position( 49 ).then( win => {
                like( w, [98], 'last position' );
                is( p.has_next(), false, "no more next" );
                p.set_position(0).then( win => {
                    like( w, [0,2,4], 'first position' );
                    is( p.has_prev(), false, "no prev" );
                    if( fail_count === 0 ) {
                        alert( "PASSED ALL TESTS" );
                    } else {
                        alert( "FAILED " + fail_count + " TESTS" );
                    }
                } );

            } );
        }
    }

    const donext = (w) => {
        console.log( count+") CACHE AT " + p.cache.join(",") );
        console.log( "WIN AT " + p.window.join(",") );
        if( count> 18 ) {
            doprev();
            return;
        } else {
            if( count > 15 ) {
                like( w || p.window, [96,98], "Iterationy " + count );
            } else
                like( w || p.window, [count*6,count*6+2,count*6+4], "Iteration " + count );
            p.next().then( w => {
                donext(w);
            });
        }
        ++count;
    }

    p.set_position(0).then( donext );
} //test

        // set the window to part of the segment

            //   0 1 2 3 4 5    ( 3, 9 )
            // [ 3 4 5 6 7 8 9 ] <--- cache  (3,9)
            //           [ 8 9 10 11 ] <---- result segment (win_start = 8 )
            //   want a splice( 5, 4, ...result ) = (win_start-cache_start), result segment.length )

            //               0 1 2 3 4 5    ( 3, 9 )
            //             [ 3 4 5 6 7 8 9 ] <--- cache  (3,9)
            //       [ 0 1 2 3 4 5 ] <---- result segment (win_start = 0, len = 6 )
            //   want a splice( 0, 3, ...result ) = (win_start-cache_start) < 0 ? 0 : (win_start-cache_start),
            //                                       result_segment.length - (), ...result segment )

            //   0 1 2 3 4 5 6
            // [ 3 4 5 6 7 8 9 ] <--- cache  (3,9)
            //     [ 5 6 7 ] <--- window     (5,7)
            // start the slice at cache 2 = window_start - cache_start
        //   end the slice at cache 4 = window_end - cache_start


export { act, Paginator, match, matches, byClass, byId, byTag };
