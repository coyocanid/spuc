/*
  Paginator

   The paginator provides a moveable window for a list, caching parts of the list.

   For example, there might be a list of 10M log entries on the server.
   The Paginator might cache 1000 items at a time, and provide 100 at a time and 
   be able to cache up to 100_000 items.

   Since the actual list may be remote, the paginator methods
   (except for has_next and has_prev) return Promise objects.

   Usage :
      // provided by you to contact the server. The server
      // must respond with { segment, list_size } where 
      // segment is part of the full array.
      let pagefun = (start_idx,amount_to_get) => { 
         return new Promise( resolve => {
           ... contact server or something ...
           resolve( { segment => [..part of big list], list_size => size of big list } );
         } );
      }; 

      let p = new Paginator( { pagefun, 
                               fetch_chunk_size : 1000, 
                               window_size: 100, 
                               max_cached : 100000 } );

      // optionally provided by you, something that watches for
      // changes in the pagination window.
      yourWatchFunction( p.window );

      // set to the start of the list
      p.start().then( win => {
          // win === p.window
          // provided here if you want to use this rather than
          / a watch function
      } );

      p.has_next();  // check if it is not at the end

      // promises
      p.next();      // next window_size entries
      p.next( 500 ); // like a fast forward


      p.has_prev();  // check if it is not at the beginning

      // promises
      p.prev();      // prev window_size entries
      p.prev( 500 ); // like a fast rewind
  
      // promise to set an arbitrary position    
      p.set_position( 123124 );

*/
class Paginator {
    constructor(args) {
        this.pagefun          = args.pagefun;

        this.fetch_chunk_size = args.fetch_chunk_size || 500;
        this.window_size      = args.window_size || 10;
        this.max_cached       = args.max_cached  || 0; // 0 is no limit
        let start_pos         = args.start_pos   || 0;

        // both of these positions are relative
        // to the full list (on the server end)
        this.cache_end     = 0;
        this.cache_start   = start_pos; 
        this.window_start  = start_pos;
        
        this.list_last_idx = undefined;

        this.cache = []; //cached part of list
        this.window = []; // <--- *** the field to watch and show on the UI ***

        if( this.cache_size != 0 && this.max_cached < 3*this.fetch_chunk_size ) {
            throw new Exception( "Paginator constructed with a cache size less than three times the fetch size" );
        }
        
        // server returns 1, { segment => [part of list], list_size => @$list }
    }
    start(idx) {
        this.cache.splice(0);
        this.window.splice(0);
        this.list_last_idx = undefined;
        this.cache_end = 0;
        this.cache_start = idx||0;
        return this.set_position(idx||0);
    }
    has_next() {
        return this.window_end <= this.list_last_idx;
    }
    has_prev() {
        return this.window_start > 0;
    }
    next( more ) {
        if( !this.has_next() ) return new Promise( (res) => res(this.window) );
        more = more ? more : this.window_size;
        return this.set_position( this.window_start + more );
    }
    prev( more ) {
        if( !this.has_prev() ) return new Promise( (res) => res(this.window) );
        more = more ? more : this.window_size;
        return this.set_position( this.window_start - more );
    }
    refresh() {
        this.set_position( this.window_start );
    }
    set_position( start_point ) {
        let end_point = start_point + this.window_size;
        if( end_point > this.list_last_idx ) {
            end_point = this.list_last_idx;
        }
        if( start_point < 0 )
            start_point = 0;
        
        if( this.list_last_idx !== undefined && start_point > this.list_last_idx )
            start_point = this.list_last_idx;
        console.log( this, "SET_POS " + start_point + " - " + end_point );    
        if( this.list_last_idx === undefined ||
            (start_point < this.list_last_idx &&
             (end_point > this.cache_end || start_point < this.cache_start )))
        {
            return new Promise( (res,rej) => {
                this._load( start_point, end_point > this.cache_end )
                    .then( res )
                    .catch( err => rej(err) );
            } );
        } else {
            return new Promise( (res,rej) => {
                this._apply( start_point );
                res(this.window);
            } );
        }
    }
    _load( win_start, append_to_end ) {
        return new Promise( (resolve, reject) => {
            this.pagefun(win_start, this.fetch_chunk_size).then( res => {
                this.list_last_idx = res.list_size - 1;

                let delta = win_start - this.cache_start;
                let splice_start = delta > 0 ? delta : 0;
                let splice_len   = delta > 0 ? res.segment.length : res.segment.length - delta;
                this.cache.splice( splice_start, splice_len, ...res.segment );

                this.cache_end = this.cache_end + res.segment.length;
                if( win_start < this.cache_start )
                    this.cache_start = win_start;
                if( this.max_cached > 0 && this.cache.length > this.max_cached ) {
                    if( append_to_end ) {
                        let new_cache_start = this.cache_end - this.max_cached;
                        this.cache.splice( 0, new_cache_start - this.cache_start );
                        this.cache_start = new_cache_start;
                    } else { // snip from the end
                        let new_cache_end = this.cache_start + this.max_cached;
                        this.cache.splice( new_cache_end );
                        this.cache_end = new_cache_end;
                    }
                }
                this._apply(win_start);
                resolve(this.window);
            } ).catch( err => reject(err) );
        } );
    } //_load
    _apply(win_start) {
        this.window_start = win_start;
        this.window_end = this.window_start + this.window_size;
        if( this.window_end >= this.list_last_idx )
            this.window_end = this.list_last_idx+1;
        let win = this.cache.slice( this.window_start - this.cache_start, this.window_end - this.cache_start );
        this.window.splice( 0, this.window.length, ...win ); // use splice rather than replace, but probably *could* be replace. try both ways
        console.log( this.window_size, this.window.length ,"WINDOW SET" );
    } //_apply
} //Paginator

if( false ) { //testy
    let l =[];
    for( let i=0; i<50; i++ ) l.push(i*2);
    console.log( l, "STARTL" );
    let pf = (sidx,amt) => {
        return new Promise( reso => {
            reso( { segment : l.slice( sidx, sidx+amt ), list_size : l.length } );
        } );
    };

    let p = new Paginator( { pagefun : pf, fetch_chunk_size : 6, window_size : 3, max_cached : 20 } );
    let count = 0;
    let fail_count = 0;

    const is = ( a, b, msg ) => {
        if( a !== b) {
            console.log( "***************************FAILED",
                         a, b, msg );
            return;
        }
        console.log( "PASSED", a, msg );
    }
    
    const like = ( a, b, msg ) => {
        if( a.length != b.length ) {
            console.log( "***************************FAILED", '['+a.join(",")+']', '['+b.join(",")+']', msg );
            fail_count++;
            return;
        }
        for( let i=0; i<a.length; i++ ) {
            if( a[i] != b[i] ) {
                console.log( "***************************FAILED", '['+a.join(",")+']', '['+b.join(",")+']', msg );
                fail_count++;
                return;
            }
        }
        console.log( "PASSED", '['+a.join(",") +']', msg );
    }
    
    const doprev = (w) => {
        console.log( count+") CCACHE AT " + p.cache.join(",") );
        console.log( "WWIN AT " + p.window.join(",") );
        if( count > 0 ) {
            let effc = count - 3;
            if( effc === 16 )
                like( w || p.window, [96,98], "Iterationz " + count );
            else if( effc < 0 )
                like( w || p.window, [0,2,4], "Iterationz " + count );
            else
                like( w || p.window, [effc*6,effc*6+2,effc*6+4], "Iterationw " + count );
            --count;
//            D = count == 12 || count == 11;

            p.prev().then( w => {
                doprev(w);
            } );
        } else {
            p.set_position( 49 ).then( win => {
                like( w, [98], 'last position' );
                is( p.has_next(), false, "no more next" );
                p.set_position(0).then( win => {
                    like( w, [0,2,4], 'first position' );
                    is( p.has_prev(), false, "no prev" );
                    if( fail_count === 0 ) {
                        console.log( "PASSED ALL TESTS" );
                    } else {
                        console.log( "FAILED " + fail_count + " TESTS" );
                    }
                } );
                  
            } );
        }
    }
    
    const donext = (w) => {
        console.log( count+") CACHE AT " + p.cache.join(",") );
        console.log( "WIN AT " + p.window.join(",") );
        if( count> 18 ) {
            doprev();
            return;
        } else {
            if( count > 15 ) {
                like( w || p.window, [96,98], "Iterationy " + count );
            } else
                like( w || p.window, [count*6,count*6+2,count*6+4], "Iteration " + count );
            p.next().then( w => {
                donext(w);
            });
        }
        ++count;
    }

    p.set_position(0).then( donext );
} //test
