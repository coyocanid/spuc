import { act, Paginator, match, matches, byClass, byId } from "/spuc/js/tools.js";

let P2 = 2*Math.PI;

Vue.component('color', {
    props : [ 'buttons', 'color' ],
    data : function() {
        let rgba  = [100,100,100,255];
        return {
            ctx : undefined,
            R : undefined,
            G : undefined,
            B : undefined,
            rgba,
            parts : ['background-color:rgba('+rgba[0]+',0,0,255)',
                     'background-color:rgba(0,'+rgba[1]+',0,255)',
                     'background-color:rgba(0,0,'+rgba[2]+',255)'],
            cellcolor : 'background-color:rgba('+rgba.join(',')+')',
        };
    },
    mounted : function() {
        this.ctx = this.$el.children[1].children[0].children[0].children[1].getContext("2d");
        this.R = this.$el.children[1].children[1].children[0].children[0];
        this.G = this.$el.children[1].children[1].children[1].children[0];
        this.B = this.$el.children[1].children[1].children[2].children[0];
        this.$watch( 'color', function() {
            this.setcanvas( this.color );
        }, { immediate : true } );
    },
    methods : {
        setcanvas : function( color ) {
            this.ctx.fillStyle = color;
            this.ctx.fillRect( 0, 0, 1, 1 );
            this.rgba = this.ctx.getImageData( 0, 0, 1, 1 ).data;
            this.parts = ['background-color:rgba('+this.rgba[0]+',0,0,255)',
                          'background-color:rgba(0,'+this.rgba[1]+',0,255)',
                          'background-color:rgba(0,0,'+this.rgba[2]+',255)'];
            this.cellcolor = 'background-color:rgba('+this.rgba.join(',')+')';
            this.R.value = this.rgba[0];
            this.G.value = this.rgba[1];
            this.B.value = this.rgba[2];
        },
        setcolor : function( idx, val ) {
            this.rgba[idx] = val;
            this.cellcolor = 'background-color:rgba('+this.rgba.join(',')+')';
            let blank = [0,0,0];
            blank[idx] = val;
            this.parts[idx] = 'background-color:rgba('+blank.join(',')+')';
        },
        activate : function( butt ) {
            this.$emit( 'press', butt, 'rgba('+this.rgba.join(',')+')');
        },
    },
    template : `<div class="col" style="text-align:center">
                    <p>Color Mixer</p>

                    <div class="row" style="">

                       <div class="col" style="">
                          <div class="row" style="">
                             <div class="col squish" v-bind:style="cellcolor+';margin:auto;width:100px;height:100px;padding:20px;'">
                                     {{ rgba.join(",") }} 
                             </div>
                             <canvas width="1" height="1"></canvas>
                          </div>

                          <div class="row squish wrap" style="margin:10px 0">
                            <button  v-for="b in buttons"
                                     v-on:click="activate(b)" type="button">{{b}}</button>
                          </div>
                       </div>

                       <div class="row spread">
                         <div class="col" v-for="idx in [0,1,2]"
                              v-bind:style="parts[idx]"
                             >
                            <input style="-webkit-appearance:slider-vertical;max-width:40px;height:256px;"
                                v-on:input="setcolor(idx,$event.target.value)"
                                min="0"
                                max="255"
                                type="range">
                          </div>
                       </div>
                   </div>
                </div>`,
} );

Vue.component('imed', {
    name : 'imed',
    props : [ 'width', 'height', 'img_data', 'img_src', 'cscale', 'buttons', 'startdirty', 'command' ],
    data : function() {
        let aspect_ratio = this.width/this.height;
        return {
            inFullScreen : false,
            loading : false,

            aspect_ratio,
            orig_width :undefined,
            orig_height :undefined,

            editor : undefined,
            canvas : undefined,
            ctx : undefined,

            lastsave : undefined,
            needsSave : this.startdirty ? true : false,
            needsAutoSave : false,
            isDrawing : false,
            undos : [],
            redos : [],


            colors : ['black','white','red','orange','yellow','tan','brown','green','blue','violet', 'purple', 'indigo'],
            color_cols : 1,
            color : 'black',
            color_idx : 0,
            color_el : undefined,

            brush_size : 8,
            brush_el : undefined,

            lastX : undefined,
            lastY : undefined,
            ratx : undefined,
            raty : undefined,

            picture_x : undefined,
            picture_y : undefined,

            offset_x : undefined,
            offset_y : undefined,

            imgdata : undefined,
        }
    }, //data
    
    mounted : function() {
        // called when the element is on the page

        this.editor = this.$el;
        this.colormix = this.editor.children[1].children[1].children[0];
        this.canvas = this.editor.children[1].children[1].children[1];
        this.canvas.addEventListener( 'touchstart', ev => this.starttouchdraw(ev) );
        this.canvas.addEventListener( 'touchend', ev => this.enddraw() );
        this.canvas.addEventListener( 'touchmove', ev => this.touchmove(ev) );
        this.ctx    = this.canvas.getContext("2d");

        this.orig_width = this.canvas.offsetWidth;
        this.orig_height = this.canvas.offsetHeight;

        this.ratx   = this.width/this.canvas.offsetWidth;
        this.raty   = this.height/this.canvas.offsetHeight;

        if( this.img_data ) {
            this.ctx.clearRect( 0, 0, this.width, this.height );
            this.ctx.drawImage( this.img_data, 0, 0, this.width, this.height );
            this.imgdata = this.ctx.getImageData( 0, 0, this.width, this.height );
        }
        else if( this.img_src ) {
            this.apply_img_src();
        }

        this.$watch( 'img_src', function(val, oldval) {
            if( val !== oldval )
                this.apply_img_src();
        } ); //watch

        this.$watch( 'command', function(val, oldval) {
            if( val !== oldval ) {
                if( val == 'clear' ) {
                    this.clear('noconirm');
                }
            }
        } ); //watch command
        
        
        setInterval( () =>  {
            // check to see if this needs saving
            if( this.needsAutoSave ) {
                let imgdata = this.canvas.toDataURL( 'image/png' ).replace(/^data:image\/(png|jpg);base64,/, "");
                this.needsAutoSave = false;

                this.$emit('ready', 'auto-save', imgdata );

            }
        }, 10000 );
        window.addEventListener('resize',ev => setTimeout(this.scale,10) );
        this.scale();
    }, //mounted

    methods : {
        check_color_cols : function() {
            let max_colors_in_col = Math.round((50+this.orig_height) / 32); // 32 is the width of a color
            this.color_cols = this.colors.length == max_colors_in_col ? 1 : 1 + Math.floor(this.colors.length / max_colors_in_col);
        },
        color_control : function( type, color ) {
            // returns color index
            if( type === 'add' ) {
                this.color_idx = this.colors.length;
                this.colors.push( color );
                // ntoshow <= orig_height/32
                this.check_color_cols();
            }
            else if( type === 'done' ) {
                this.toggle_palette();
            }
        },
        toggle_palette : function() {
            // show palette, hide canvas
            this.$el.classList.toggle( 'show-palette' );
        },
        scale : function() {
            if( this.inFullScreen || document.fullscreen || document.fullScreenElement || this.editor.fullScreenElmenet ) {
                this.inFullScreen = true;
                let pel = this.canvas.parentElement;
                let par = pel.offsetWidth / pel.offsetHeight;
                if( par > this.aspect_ratio ) { // wider than taller
                    // use height to scale
                    this.canvas.style.height = (pel.offsetHeight - 2) + 'px';
                    this.canvas.style.width = Math.round(this.canvas.offsetHeight*this.aspect_ratio) + 'px';
                } else { //taller than wider
                    this.canvas.style.width = (pel.offsetWidth - 2) + 'px';
                    this.canvas.style.height = Math.round(this.canvas.offsetWidth/this.aspect_ratio) + 'px';
                }
            } else {
                this.inFullScreen = false;
                this.canvas.style.width = this.orig_width + 'px';
                this.canvas.style.height = this.orig_height + 'px';
            }
        },
        gofull : function() {
	    if( this.editor.requestFullScreen ) {
		this.editor.requestFullScreen();
		this.setFullScreen(true);
	    }
	    else if( this.editor.webkitRequestFullScreen ) {
		this.editor.webkitRequestFullScreen();
		this.setFullScreen(true);
	    }
	    else if( this.editor.mozRequestFullScreen ) {
		this.editor.mozRequestFullScreen();
		this.setFullScreen(true);
	    }
        },
        gonormal : function() {
            if( document.closeFullScreen )
                document.closeFullScreen();
            else if( document.webkitCancelFullScreen() )
                document.webkitCancelFullScreen();
            this.setFullScreen(false);
        },
        setFullScreen: function( fs) {
            this.inFullScreen = fs;
            window.addEventListener('resize',ev => setTimeout(this.scale,10) );
        },

        apply_img_src: function() {
            return new Promise( (resolve,reject) => {
                let img = new Image();
                img.onload = () => {
                    this.loading = false;
                    this.ctx.clearRect( 0, 0, this.width, this.height );
                    this.ctx.drawImage( img, 0, 0, this.width, this.height );
                    this.imgdata = this.ctx.getImageData( 0, 0, this.width, this.height );
                    resolve( this.imgdata );
                };
                this.loading = true;
                img.src = this.img_src;
            } );
        },

        move : function( x2, y2 ) {
            let x1 = this.lastX;
            let y1 = this.lastY;

            this.lastX = x2;
            this.lastY = y2;

            let delx = Math.abs(x2-x1);
            let dely = Math.abs(y2-y1);

            let del = delx + dely;
            let A = 0, B = 0, a = 0, b = 0;
            for( let i=0; i<del; i++ ) {
                if( A > B ) {
                    b++;
                    B = B + delx;
                } else {
                    a++;
                    A = A + dely;
                }
                if( y1 > y2 ) {
                    if( x1 > x2 ) {
                        this.draw_point(x1 - a,y1 - b);
                    } else {
                        this.draw_point(x1 + a,y1 - b);
                    }
                } else {
                    if( x1 > x2 ) {
                        this.draw_point(x1 - a,y1 + b);
                    } else {
                        this.draw_point(x1 + a,y1 + b);
                    }
                }
            }
        }, //move
        draw_point : function(x,y) {
            this.picture_x = x;
            this.picture_y = y;
            this.ctx.beginPath();
            this.ctx.arc( x, y, this.brush_size, 0, P2, true );
            this.ctx.fill();
        },
        
        startdraw : function(x, y) {
            this.isDrawing = true;
            this.lastX = x;
            this.lastY = y;
            this.draw_point( x, y );
        },
        enddraw : function() {
            this.isDrawing = false;
            this.fix();
        },

        dirty : function() {
            this.needsAutoSave = true;
            this.needsSave = true;
        },
        
        // saves the image
        fix : function() {
            this.redos.length = 0;
            this.undos.push(this.imgdata);

            this.imgdata = this.ctx.getImageData( 0, 0, this.width, this.height );
            this.dirty();
        },

        undo : function() {
            let und = this.undos.pop();
            if( und ) {
                let redo = this.ctx.getImageData( 0, 0, this.width, this.height );
                this.redos.push( redo );
                this.ctx.putImageData( und, 0, 0 );
                this.dirty();
            }
        },

        redo : function() {
            let redo = this.redos.pop();
            if( redo ) {
                let undo = this.ctx.getImageData( 0, 0, this.width, this.height );
                this.undos.push( undo );
                this.ctx.putImageData( redo, 0, 0 );
                this.dirty();
            }
        },
        clear: function(noconfirm) {
            if( noconfirm || confirm( "really clear this picture?" ) ) {
                let undo = this.ctx.getImageData( 0, 0, this.width, this.height );
                this.undos.push( undo );
                this.ctx.clearRect( 0, 0, this.width, this.height );
                this.dirty();
            }
        },
        touchmove : function( tev ) {
            if( tev.touches.length == 1 ) {
                let ev = tev.touches[0];
                tev.preventDefault();
                tev.stopPropagation();

                let evx = this.inFullScreen ? ev.clientX : ev.pageX;
                let evy = this.inFullScreen ? ev.clientY : ev.pageY;
                
                let x = Math.round(this.ratx*(evx - this.canvas.offsetLeft));
                let y = Math.round(this.raty*(evy - this.canvas.offsetTop));

                
                this.offset_x = ev.offsetX;
                this.offset_y = ev.offsetY;

                this.picture_x = x;
                this.picture_y = y;

                if( this.isDrawing ) {
                    this.move( x, y );
                }
            }
        },
        mousemove : function( ev ) {
            let x = Math.round(ev.offsetX * this.ratx);
            let y = Math.round(ev.offsetY * this.raty);
            this.offset_x = ev.offsetX;
            this.offset_y = ev.offsetY;

            this.picture_x = x;
            this.picture_y = y;

            if( this.isDrawing ) {
                this.move( x, y );
            }
        },
        starttouchdraw : function(tev) {
            if( tev.touches.length == 1 ) {
                let ev = tev.touches[0];
                tev.preventDefault();
                tev.stopPropagation();
                this.ratx   = this.width/this.canvas.offsetWidth;
                this.raty   = this.height/this.canvas.offsetHeight;
                let evx = this.inFullScreen ? ev.clientX : ev.pageX;
                let evy = this.inFullScreen ? ev.clientY : ev.pageY;
                this.startdraw( Math.round(this.ratx*(evx - this.canvas.offsetLeft)),
                                Math.round(this.raty*(evy - this.canvas.offsetTop)) );
            }
        },
        startmousedraw : function(ev) {
            this.ratx   = this.width/this.canvas.offsetWidth;
            this.raty   = this.height/this.canvas.offsetHeight;
            this.startdraw( Math.round(this.ratx*ev.offsetX), Math.round(this.raty*ev.offsetY) );
        },
        save : function(target) {
            let imgdata = this.canvas.toDataURL( 'image/png' ).replace(/^data:image\/(png|jpg);base64,/, "");
            this.lastsave = new Date().getTime();
            this.needsAutoSave = false;
            this.needsSave = false;
            this.$emit( 'ready', target, imgdata );
        },
        setsize : function( ev, sz ) {
            if( this.brush_el ) this.brush_el.classList.remove('chosen');
            this.brush_size = sz;
            this.brush_el = ev.target;
            ev.target.classList.add( 'chosen' );
        },
        removecolor: function( idx ) {
            this.colors.splice( idx, 1 );
            if( this.colors.length >= this.color_idx ) {
                this.color_idx--;
            }
            this.check_color_cols();
        },
        setcolor : function( ev, c, idx ) {
            this.color = c;
            this.color_idx = idx;
            this.ctx.fillStyle = c;
            if( this.color_el ) this.color_el.classList.remove('chosen');
            this.color_el = ev.target;
            this.colormix.setcolor = c;
            ev.target.classList.add( 'chosen' );
        },
    },
    template : `<div class="col"
                     @keyup.escape="setFullScreen(false)"
                     style="background:yellow">
                   <div class="row squish">
                         <button  v-for="b in buttons"  class="large"
                                  v-bind:style="needsSave ? 'background:lightgreen' : ''"
                                  v-on:click="save(b)" type="button">{{b}}</button>
                   </div>

                   <div class="row grow" style="background-color:grey;border:solid 1px;">
                     <div class="col">
                      <div class="grow" v-bind:style="'display:grid;background-color:#DDD;grid-template-columns:'+'auto '.repeat(color_cols)">
                        <div class="row color-control" 
                             v-for="(c,idx) in colors">
                           <div 
                             v-on:click="setcolor($event,c,idx)"
                             v-bind:class="(color === c ? 'chosen color' : 'color') + ' ' + c"
                             v-bind:style="'background-color:'+c"/>
                           <div v-on:click="removecolor(idx)" style="cursor:pointer;margin: auto 0"
                                class="on-palette">&timesb;</div>
                        </div>
                     </div>
                     <div v-on:click="toggle_palette" style="cursor:pointer;background:white;padding:4px;border:solid 3px black;border-radius:20px;">Palette</div>
                   </div>
                      <div class="row grow">
                        <color class="on-palette"
                               v-bind:color="color"
                               v-on:press="color_control"
                               v-bind:buttons="['add','done']"/>
                        <canvas id="canvas"
                            class="no-palette"
                            v-bind:width="width"
                            v-bind:height="height"
                            v-bind:style="'outline:solid 2px black;background:white;margin:auto;height:'+(height*cscale)+'px;width:'+(width*cscale)+'px;z-index:'+cscale"
                            v-on:mousemove="mousemove($event)"
                            v-on:mousedown="startmousedraw($event)"
                            v-on:mouseup="enddraw()"
                          ></canvas>
                      </div>
                      <div class="col spread">
                        <div class="col" style="min-width:60;background-color:#DDD;">
                         <div class="col" style="justify-content:none" v-for="sz in [1,4,8,13,21,34,50]"
                              v-on:click="setsize($event,sz)"
                              v-bind:class="(brush_size === sz ? 'chosen brush-container col' : 'brush-container col')"
                             >
                             <div class="brush"
                               v-bind:style="(brush_size === sz ? 'background-color:'+color+';': '' ) + 'border-radius:'+Math.round(sz/2)+'px;width:'+sz+'px;height:'+sz+'px'"
                             />
                         </div>
                       </div>

                       <div class="col">


                         <button v-on:click="undo()"
                                 v-bind:disabled="undos.length ? null : 'true'"
                                 type="button">undo</button>
                         <button v-on:click="redo()"
                                 v-bind:disabled="redos.length ? null : 'true'"
                                 type="button">redo</button>
                       </div>

                         <button v-on:click="clear()"
                                 type="button">clear</button>


                       <button v-if="inFullScreen" v-on:click="gonormal()" type="button">Exit Full Screen</button>
                       <button v-else v-on:click="gofull()" type="button">Full Screen</button>
                      </div>

                    </div>
                    <div class="row spread" v-if="false">
                       <span style="font-size:small;min-width:15em;">
                            Picture {{ this.picture_x }}, {{ this.picture_y }}
                            Offset  {{ this.offset_x }}, {{ this.offset_y }}
                       </span>
                    </div>
                  </div>`,
} );

const attach_color = (el) => {
    new Vue({
        el,
        template : `<div class="col">
                      <color
                         v-bind:initial="'wheat'"
                         v-bind:buttons="[['add to palette','add'],['copy','copy'],['remove','remove'],['done','close']]"
                          ></color>
                    </div>`,
    } );
};


export {attach_color};
