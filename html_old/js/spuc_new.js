import { act, Paginator, match, matches } from "/js/tools.js";

class ComicsPaginator extends Paginator {
    constructor( args ) {
        super( args );
        this.sort = 'recent';
        this.show = 'finished';
        this.artist = undefined;
        this.spuc   = args.spuc;
        this.detail = 0;
        this.pagefun = (start,chunksize) => {
            return act( 'App::SPUC::Public', 'comics', {
                start, length : chunksize,
                artist : this.artist ? this.artist.display_name : undefined,
                sort : this.sort,
                show : this.show,
            }, spuc.sess_id );
        };
    }
    set_show( show ) {
        if( this.show !== show ) {
            this.show = show;
            this.reset();
        }
    }
    set_sort( sort ) {
        if( this.sort !== sort ) {
            this.sort = sort;
            this.reset();
        }
    }
    set_artist( art ) {
        if( this.artist !== art ) {
            this.artist = art;
            this.reset();
        }
    }
    url( args ) {
        let start  = (args.window_start !== undefined ? args.window_start : this.window_start) || 0;
        let artist = args.artist !== undefined ? args.artist : this.artist ? this.artist.display_name : undefined;
        let sort   = ( args.sort !== undefined ? args.sort : this.sort ) || 'recent';
        let amt    = ( args.amt !== undefined ? args.amt : this.window_size ) || 4;
        let detail = ( args.detail !== undefined ? args.detail : this.detail ) || 0;
        let dest   = args.dest || 'comics'
        return '#' + dest +
            ';start='+start+
            ( artist ? ';artist=' + artist : '' ) +
            ';sort=' + sort +
            ';detail=' + detail +
            ';amt=' + amt;
    }
    next_url() {
        return this.url({ window_start : this.next_idx() });
    }
    prev_url() {
        return this.url({ window_start : this.prev_idx() });
    }
    sort_url(sort) {
        return this.url({ sort, window_start: 0 });
    }
    back_url() {
        return this.url({ detail : 0, amt : 4 });
    }
    detail_url(idx) {
        return this.url({ window_start : idx, detail : 1, amt : 1 });
    }
    artist_url(name) {
        return this.url({window_start:0,detail:0, amt: 4, dest:'artist', artist:name } );
    }

} //ComicsPaginator

class SPUC {
    constructor() {
        this.app = 'App::SPUC::Public';

        // session and logged in artist 
        this.sess_id = window.localStorage.getItem( 'sess_id' );
        this.login   = undefined;

        // what the main ui is showing
        this.showing = undefined;
        this.err     = '';
        this.msg     = '';

        // context of what this is showing, like
        // the site itself, the logged in user or a particular user
        this.context = this;
        this.doing   = undefined;

        // a cache of sorts
        this.artists = {};
        this.chats = [];

        this.comics_paginator = new ComicsPaginator( {
            fetch_chunk_size : 20,
            max_cached       : 400,
            window_size      : 4,
            spuc             : this,
        } );

        this.chat_paginator = new Paginator( {
            fetch_chunk_size : 100,
            max_cached       : 400,
            window_size      : 25,
            pagefun : (start,chunksize) => {
                return new Promise( (resolve,reject) => {
                    this.act( 'recent_chats', {
                        last_seen : this.last_seen,
                    } ).then( res => {
                        this.last_seen = res[res.length-1].idx;
                        resolve( { segment : res.slice(0,chunksize).map( r => r.chat ), list_size : res.length } );
                    } ).catch( e => reject(e) );
                } );
            },
        } );

        this.public_nav_list = [ ['comics'],
                                 ['artists'],
                               ];
        this.logged_in_nav_list = [ ['comics'],
                                    ['artists'],
                                    ['log out',()=>{this.logout().then( r => {
                                        spuc.msg = 'logged out';
                                        window.location.hash = '#comics';
                                    } )}],
                                    ['unfinished comics', 'comics;unfinished=1;' ],
                                    ['edit profile','profile'],
                                  ];
        this.nav_list = this.public_nav_list;
    } //constructor

    init() {
        // check if there is a sess_id to login with

    } //init
    _start_nav() {
        // get recent comics and chats
//        this.comics_paginator.start().then( e => {
            this.nav( match( window.location.href, /\#(.*)/ ) ? matches[1] : '' );
//            this.refresh_chat();
        console.log( 'uncomment setInterval for chat********************************************' );
//        setInterval( this.refresh_chat.bind(this), 10000 );
        //        } );

        // listens for a change in the hash of the url and
        // adjusts accordingly

    }

    // -------------------- main view changes ------------------------


    show_artists() {
        this.load_artists()
            .then( arts => this.showing = 'artists' );
    }
    show_artist(args) {
        this.load_artists()
            .then( arts => {
                let artist = this.artists[args.artist];
                if( artist ) {
                    this.comics_paginator.detail = args.detail || 0;
                    this.comics_paginator.window_size = 4;
                    this.comics_paginator.artist = artist;
                    this.comics_paginator.start().then( e => {
                        this.showing = 'artist'
                    } );
                } else {
                    this.showing = 'comics';
                }
            } );
    } //show_artist
    show_comics(args) {
        this.comics_paginator.window_size = parseInt(args.amt)||4;
        this.comics_paginator.set_sort( args.sort || 'recent' );
        this.comics_paginator.detail = args.detail || 0;
        if( args.artist ) {
            this.load_artists()
                .then( artists => {
                    this.comics_paginator.artist = this.artists[args.artist];
                    let start = args && args.start ? parseInt(args.start) : 0;
                    return this.comics_paginator.set_position(start).then( e => {
                        this.showing = 'artist';
                    });
                } );
        } else {
            this.comics_paginator.set_artist( undefined );
            let start = args.start ? parseInt(args.start) : 0;
            return this.comics_paginator.set_position(start).then( e => {
                this.showing = 'comics';
            });
        }
    } //show_comics
    show_reset_request(recover_idx) {
        this.showing = 'reset-request';
    }
    show_profile() {
        if( this.login )
            this.showing = 'profile';
        else {
            this.err = 'need to be logged in to edit profile';
            this.showing = 'comics';
        }
    }
    show_recover( args ) {
        if( args.tok ) {
            this.act( 'activate_reset', args.tok )
                .then( sess => {
                    this.login    = sess.account;
                    this.token    = args.tok;
                    this.sess_id  = sess.sess_id;
                    window.localStorage.setItem( 'sess_id', this.sess_id );
                    this.nav_list = this.logged_in_nav_list;
                    this.msg = 'You are now logged in and may now reset your password';
                    this.showing  = 'recover';
                } )
                .catch(e => {
                    this.showing = 'recover-err';
                } );
        } else {
            this.showing = 'recover-err';
        }
    } //show_recover
    show_signup() {
        this.showing = 'signup';
    }

    // ---------- actions ----------------

    // calls the other actions
    act( action, args, files ) {
        return act(this.app, action, args, this.sess_id, files );
    }


    add_chat(message) {
        return this.act( 'add_chat', { message } );
    }
    load_artists() {
        if( Object.keys(this.artists).length ) {
            return new Promise( (res,rej) => {
                res(this.artists);
            } );
        }
        return spuc.act('artists')
            .then( arts => {
                arts.forEach( a => {
                    this.artists[a.display_name] = a;
                } );
            } );
    } //load_artists
    logout() {
        this.nav_list = this.public_nav_list;
        window.localStorage.removeItem( 'sess_id' );
        return this.act('logout')
            .finally( r => {
                this.login = undefined;
                this.sess_id = undefined;
                this.msg = 'logged out';
                this.showing = 'comics';
            } );
    } //logout
    refresh_chat() {
        return this.act( 'recent_chat_length' )
            .then( cl => {
                let start_point = cl - (this.chat_paginator.window_size-1);
                if( start_point < 1 ) start_point = 1;
                this.last_seen = start_point - 1;
                this.chat_paginator.start(start_point-1).then( e => {
                    this.chats.splice( 0, this.chats.length, ...e.reverse() );
                } );
            } );
    } //refresh_chat
    reset_password(newpassword,token,oldpassword) {
        return this.act( 'reset_password', {newpassword,token,oldpassword} );
    }
    send_reset_request( email ) {
        return this.act( 'send_reset_request', email );
    }
    try_login(user,password) {
        return this.act( 'login', { user, password } )
            .then( ans => {
                let s = ans.session;
                this.login   = s.account;
                this.login.avatars = ans.avatars;
                this.sess_id = s.sess_id;
                window.localStorage.setItem( 'sess_id', this.sess_id );
                this.nav_list = this.logged_in_nav_list;
            });
    } //try_login
    signup(email,login,password) {
        return this.act( 'signup', { email, login, password } );
    }

    update_account(fields) {
        return this.act( 'update_account', fields ).then( res => {
            res.artist.avatars = res.avatars;
            this.login = res.artist;
        } );
    }
    upload_avatar(file) {
        return this.act( 'upload_avatar', undefined, [file] )
            .then( avs => this.login.avatars = avs );
    }
    // --------------- utility ------------------------
    format_time(time) {
        let d = new Date(1000*time);
        return d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
    }
    dump() {
        console.log( this,"SPUCDUMP" );
    }
} //class SPC

class Navagator {
    constructor() {
        this.context = undefined;
        this.showing = undefined;
        this.login   = undefined;
        this.artist  = undefined;
    }
    init() {
        if( this.sess_id ) {
            this.act( 'session_login', { sess_id : this.sess_id } )
                .then( res => {
                    let sess = res.session;
                    this.login = sess.account;
                    this.login.avatars = res.avatars;
                    this.nav_list = this.logged_in_nav_list;
                    this._start_nav();
                } )
                .catch(e => {
                    this.sess_id = undefined;
                    spuc.msg = 'logged out';
                    window.location.hash = '#comics';
                    window.localStorage.removeItem( 'sess_id' );
                    this._start_nav();
                } );
        } else {
            this._start_nav();
        }
    }
    _start_nav() {
        window.addEventListener( 'hashchange', ev => {
            let hash = match( ev.newURL, /\#(.*)/ ) ? matches[1] : '';
            console.log( 'hashchange to', hash );
            this.nav( hash );
        } );
    }
    
    nav( hash ) {
        this.err = '';
        let parts = hash.split( ';' );
        let page = parts[0];
        let args = {};
        for( let i=1; i<parts.length; i++ ) {
            if( match( parts[i], /(.*)=(.*)/ ) ) {
                args[matches[1]] = matches[2];
            }
        }
        let shower = (this['show_'+page] || this.show_comics ).bind(this);
        console.log( "NAV TO " + page, args );
                    
        shower( args );
        this.msg = undefined;
    } //nav
    
} //class Navagator

// --------- THE ACTIVE STUFF

let gator = new Navagator();

//let spuc = new SPUC();
//spuc.init();


// ---------- UI -----------

Vue.component('artist', {
    data : function() {
        return { spuc };
    },
    template : `<div class="col">
                  <img class="avatar" v-bind:titlex="spuc.comics_paginator.artist.display_name" v-bind:src="spuc.comics_paginator.artist.avatar.path">
                  <span>{{ spuc.comics_paginator.artist.display_name }}</span>
                  <h2>Recent Comics</h2>
                  <comics/>
                </div>`,
} );

Vue.component('artist-blurb', {
    data : function() { return { spuc, artist : this.$attrs.artist } },
    template : `<a v-bind:href="'#artist;artist='+artist.display_name" style="text-decoration:none;border:solid 2px black; margin: 5px 0;font-size:larger; cursor:pointer">
                 <div class="row">
                   <div class="col"><img class="avatar" v-bind:src="artist.avatar.path"></div>
                   <div class="col">
                    <span>{{ artist.display_name }}</span>
                    <span>{{ artist.bio }}</span>
                   </div>
                 </div>
               </a>
               `,
} ); //artist-blurb

Vue.component('artists', {
    data : function() { return { spuc } },
    template : `<div class="row">
                  <div class="col">
                    <h2>The artists of SPUC</h2>
                    <artist-blurb v-for="art in Object.keys(spuc.artists).sort().map( aname => spuc.artists[aname] )" v-bind:artist="art"/>
                  </div>
                  <chat></chat>
               </div>`,
} );

Vue.component('chat', {
    data : function() { return { spuc } },
    methods : {
        add_chat( ev ) {
            let msg = ev.target.value;
            if( msg.match(/\S/) ) {
                ev.target.value = '';
                this.spuc.add_chat( msg )
                    .then( () => spuc.refresh_chat() );
            }
        }
    },
    template : `<div class="col shrink">
                    <h2>Chatter</h2>
                    <div v-if="spuc.login">
                      <input type="text"
                             v-on:change="add_chat($event)">
                    </div>
                    <div v-for="c in spuc.chats" class="chat-entry">
                      <img class="tiny-avatar" v-bind:titley="c.artist.display_name" v-bind:src="c.artist.avatar.path">
                      {{ c.comment }}
                    </div>
                </div>`,
} ); //chat


Vue.component('comic', {
    data : function() { return { spuc, comic : this.$attrs.comic, comic_idx : this.$attrs.comic_idx, new_comment : '' } },
    methods : {
        add_comment : function() {
            if( this.new_comment.match(/\S/) ) {
                this.spuc.act( 'add_comment', { comic : this.comic, comment : this.new_comment } )
                    .then( comic => {
                        this.new_comment = '';
                        this.comic = comic;
                    } )
                    .catch( e => {
                        spuc.err = e;
                    } );
            }
        },
        kudo : function(panel,idx) {
            spuc.act( 'kudo', { panel } )
                .then( comic => this.comic = comic );
        }
    },
    template : `<div v-if="spuc.comics_paginator.detail==1" class="col strip" style="border:solid 1px black; margin:3px;">
                   <div v-for="(p,idx) in comic.panels" class="col">
                      <div class="row" style="margin-bottom:3px">
                         <div class="col" style="cursor:pointer" v-on:click="kudo(p,idx);"><span>&#x2764;</span><span>{{ p.kudos }}</span></div>
                         <div v-if="p.type === 'caption'" class="detail panel caption" style="flex-grow:1">{{p.caption}}</div>
                         <div class="detail panel" v-else><img class="detail picture" v-bind:src="p.picture.path"></div>
                         <panel-artist-detail v-bind:panel="p"></panel-artist-detail>
                      </div>
                   </div>
                   <div class="col">
                      <h3 style="text-align:center">Comments</h3>
                      <div v-if="spuc.login" class="row">
                        <div class="col"><img class="tiny-avatar" v-bind:src="spuc.login.avatar.path" style="margin:0 auto"></div>
                        <input type="text" v-bind:value="new_comment" v-on:input="new_comment=$event.target.value" v-on:change="add_comment()" placeholder="say something">
                        <button type="button" v-on:click="add_comment()">add comment</button>
                      </div>
                      <div v-for="c in comic.comments" class="row">
                         <div class="col" v-bind:href="'#artist='+c.artist.display_name">
                            <img class="tiny-avatar" v-bind:src="c.artist.avatar.path" style="margin:0 auto">
                            <span style="margin:0 auto"> {{ c.artist.display_name }}</span>
                         </div>
                         {{ c.comment }}
                      </div>
                   </div>
                </div>
                <div v-else class="col strip" style="border:solid 1px black; margin:3px;">
                   <a v-bind:href="spuc.comics_paginator.detail_url(comic_idx)" style="text-decoration:none">
                     <span style="cursor:pointer;text-size:larger; padding-bottom:10px;">&#x1F50D;</span>
                     comments {{ comic.comment_count }}  kudos {{ comic.kudo_count }}
                     <div v-for="p in comic.panels" class="col">
                        <div class="row" style="margin-bottom:3px">
                           <div v-if="p.type === 'caption'" class="panel caption" style="flex-grow:1">{{p.caption}}</div>
                           <div class="panel" v-else><img class="picture" v-bind:src="p.picture.path"></div>
                        </div>
                     </div>
                   </a>
                </div>`,
} ); //comic

Vue.component('comics', { //sort most kudos, etc
    data : function() { return { spuc } },
    computed : {
        blurb : function() {
            let bl = 'Showing ';
            let comics = 1 + spuc.comics_paginator.list_last_idx;
            let comic = (1+spuc.comics_paginator.window_start);
            if( spuc.comics_paginator.window_size > 1 ) {
                let comic_to = comic + spuc.comics_paginator.window_size - 1;
                let winend = spuc.comics_paginator.list_last_idx + 1;
                if( comic_to > winend ) comic_to = winend;
                bl = 'comics ' + comic + " to " + comic_to + ' of ' + comics;
            }
            else {
                bl = 'comic ' + comic + ' of ' + comics;
            }
            return bl;
        }
    },
    template : `<div class="grow row">
                  <div class="col">

                     <div class="row">
                       {{ blurb }}
                       <span v-if="spuc.comics_paginator.sort == 'recent'" class="bold">Showing Most Recent</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('recent')">Most Recent</a>

                       <span v-if="spuc.comics_paginator.sort == 'comments'" class="bold">Showing Most Commented</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('comments')">Most Commented</a>

                       <span v-if="spuc.comics_paginator.sort == 'kudos'" class="bold">Showing Most Kudoed</span>
                       <a v-else v-bind:href="spuc.comics_paginator.sort_url('kudos')">Most Kudoed</a>
                     </div>

                     <div class="row wide">
                         <a v-if="spuc.comics_paginator.has_prev()" v-bind:href="spuc.comics_paginator.prev_url()">&lt; prev</a>
                         <span v-else >&lt; prev</span>

                         <a v-if="spuc.comics_paginator.detail==1" v-bind:href="spuc.comics_paginator.back_url()"> ^back^ </a>

                         <a v-if="spuc.comics_paginator.has_next()" v-bind:href="spuc.comics_paginator.next_url()">next &gt;</a>
                         <span v-else>next &gt;</span>
                     </div>

                     <div class="row">
                        <comic v-for="(c,idx) in spuc.comics_paginator.window"
                               v-bind:comic="c"
                               v-bind:key="c.id"
                               v-bind:comic_idx="idx+spuc.comics_paginator.window_start"
                              ></comic>
                     </div>
                  </div>
                  <chat></chat>
                </div>`,
} );

// input with a delay before it sends the change message
Vue.component('d-input', {
    data : function() { return { timer : false } },
    methods : {
        delay : function(event) {
            this.timer && clearTimeout( this.timer );
            this.timer = setTimeout( this.$emit.bind(this,'change',event),900 );
        }
    },
    template : `<input v-on:keyup="delay($event)">`,
} ); //d-input

Vue.component('err', {
    data : function() { return { err : this.$attrs.err, thing : this.$attrs.thing } },
    template : `<div v-if="thing.err" class="col" style="border:4px solid red;background:pink">
                     {{ thing.err }}
                </div>`
} ); //err

Vue.component('logged-in', {
    data : function() { return { spuc } },
    template : `<div class="col">
                  <span style="font-size:large"> {{ spuc.login.display_name }}</span>
                  <a href="#profile"><img class="big-avatar" v-bind:src="spuc.login.avatar.path"></a>
               </div>`,
} );


Vue.component('login', {
    data : function() { return { spuc, ready : false, login : '', password : '', err : '' } },
    methods : {
        update : function( fld, val ) {
            this[fld] = val;
            this.ready = this.login.match(/\S/) && this.password.match(/\S/);
        },
        try_login : function( fld, val ) {
            if( this.ready )
                spuc.try_login(this.login,this.password)
                .then( li => {
                    this.err = '';
                    this.spuc.msg = 'logged in';
                } )
                .catch( err => this.err = err );
            return this.ready;
        },
        blur : function(ev) {
            ev.target.blur();
        }
    },
    template : `<div class="col spread">
                   <div class="col">
                    <div v-if="this.err" style="border:solid 3px red">
                        {{ this.err }}
                        <a href="#reset_request">recover password</a>
                    </div>
                    <input type="text" required="1"
                             v-on:input="update('login',$event.target.value)"
                             v-on:change="try_login() || blur($event)"
                             placeholder="username or email" />
                    <input type="password" required="1"
                             v-on:input="update('password',$event.target.value)"
                             v-on:change="try_login()"
                             placeholder="password" />
                    <button v-if="ready" v-on:click="try_login()" type="button">Log In</button>
                    <button v-else type="button" disabled>Log In</button>
                  </div>
                  <a href="#signup">Sign Up</a>
                </div>`,
} ); //login

Vue.component('msg', {
    data : function() { return { thing : this.$attrs.thing } },
    template : `<div v-if="thing.msg" class="col" style="border:4px solid brown">
                     {{ thing.msg }}
                </div>`
} ); //msg


Vue.component('panel-artist-detail', {
    data : function() { return { spuc, panel : this.$attrs.panel, artist : this.$attrs.panel.artist } },
    template : `<a class="col" v-bind:href="spuc.comics_paginator.artist_url(artist.display_name)">
                  <img class="avatar" v-bind:titleo="artist.display_name" v-bind:src="artist.avatar.path">
                </a>`,
} ); //panel-artist-detail

Vue.component('profile', {
    data : function() { return { spuc, bio : spuc.login.bio,
                                 opw: '', pw : '', pw2 : '',
                                 deleted_avatars : 0,
                                 passwords_ready : false,
                                 selected_file : '',
                                 msg : '',
                                 err : '' } },
    methods : {
        check_passwords : function(fld,val) {
            this[fld] = val;
            this.err = '';
            this.passwords_ready = false;
            if( (this.pw || this.pw2) && this.pw !== this.pw2 ) {
                this.err = 'passwords do not match';
            } else if( !this.pw || this.pw.length < 7 ) {
                this.err = 'password too short';
            } else if( this.pw === this.pw2 ) {
                this.passwords_ready = true;
            }
        },
        reset_password : function() {
            if( this.passwords_ready )
                spuc.reset_password( this.pw, undefined, this.opw )
                .then( m => this.msg = 'updated password' );
        },
        update_account( ev, args ) {
            ev.preventDefault();
            ev.stopPropagation();
            spuc.update_account( args );
        },
        delete_avatar( ev, idx ) {
            ev.preventDefault();
            ev.stopPropagation();
            if( confirm('really delete this avatar?') ) {
                spuc.update_account( { delete_avatar : idx } )
                    .then( e => this.deleted_avatars++ );
            }
        },
        recover_avatar(ev) {
            ev.preventDefault();
            ev.stopPropagation();
            spuc.update_account( { recover_avatar : 1 } )
                .then( e => this.deleted_avatars-- );
        },
        ready_file(ev) {
            this.selected_file = ev.target.files[0];
        },
    },
    template : `<div>
                  <h3>Avatars</h3>
                  <div class="col">
                      <div v-if="deleted_avatars > 0">
                        Avatar was deleted. 
                        <a href="#" class="click" v-on:click="recover_avatar($event)">Recover Avatar</a>
                      </div>
                      <div class="row">
                        <div class="col" style="margin-right:1em;border:solid gold 3px;padding:3px;">
                           <img class="big-avatar" style="border:solid black 1px;" title="default avatar" v-bind:src="spuc.login.avatar.path">
                           Default Avatar
                        </div>
                        <div class="col" style="text-align:center;border:solid black 3px;padding: 3px;">
                           <div class="row">
                              <div class="col" v-for="(a,idx) in spuc.login.avatars" v-if="a.path != spuc.login.avatar.path">
                                <img class="big-avatar"  v-bind:src="a.path" style="border:solid black 1px;margin:0 5px;" v-on:click="spuc.update_account( { avatar : idx } )">
                                <div class="row squish">
                                  <a href="#" v-on:click="update_account( $event, { avatar : idx } )">default</a>
                                  <a href="#" v-on:click="delete_avatar( $event, idx )">delete</a>
                                </div>
                              </div>
                           </div>
                           Click to pick default avatar
                        </div>
                      </div>
                      
                      <a href="#" v-on:click="edit_avatars()">Create new avatar</a>
                      <div>
                        <h2>Upload Avatar</h2>
                        <input type="file" v-on:change="ready_file($event)">
                        <button type="button" v-bind:disabled="selected_file?null:'true'"
                                v-on:click="spuc.upload_avatar(selected_file)">upload</button>
                      </div>
                  </div>

                  <h3>Bio</h3>

                  <textarea cols="50" rows="7"
                         v-bind:value="bio"
                         v-on:input="bio = $event.target.value"
                         v-bind:style="bio===spuc.login.bio ? '' : 'background-color:pink'">{{ bio }}</textarea>
                  <button type="button"
                          v-on:click="spuc.update_account( { bio } )"
                          v-bind:disabled="bio===spuc.login.bio?'true':null">update bio</button>
                  <button type="button" v-on:click="bio=spuc.login.bio">reset</button>

                  <h3>Update Password</h3>
                  <msg v-bind:thing="this"/>
                  <err v-bind:thing="this"/>
                  <div class="row">
                     <div class="col">
                       <input id="pw" type="password" required="1"
                              placeholder="old password"
                              v-bind:class="{ err : this.err.match(/wrong/) }"
                              v-on:input="check_passwords('opw',$event.target.value)"
                              />

                       <input id="pw" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password (at least 7 characters)"
                              v-bind:class="{ err : this.err.match(/too/) }"
                              v-on:input="check_passwords('pw',$event.target.value)"
                              v-on:change="reset_password()" />

                       <input id="pw2" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password repeat"
                              v-bind:class="{ err : this.err.match(/too/) }"
                              v-on:input="check_passwords('pw2',$event.target.value)"
                              v-on:change="reset_password()" />
                       <button v-bind:disabled="passwords_ready?null:'true'" v-on:click="reset_password()" type="button">Reset</button>
                      </div>
                   </div>
                </div>`
} ); //profile


Vue.component('recover-err', {
    data : function() { return { spuc, err : 'bad or expired token' } },
    template : `<div class="col">
                  <h2>Recover Account</h2>
                  <div>
                     <err v-bind:thing="this"/>
                     <a href="#comics">return to site</a>
                  </div>
                </div>`,
} );

Vue.component('recover', {
    data : function() { return { spuc, pw : '', pw2 : '', ready : false, err : '', complete : false } },
    methods : {
        check_passwords : function(fld,val) {
            this[fld] = val;
            this.ready = false;
            this.err = '';
            if( this.pw != this.pw2 && (this.pw && this.pw2) ) {
                this.err = "passwords don't match";
            }
            else if( this.pw && ! this.pw.match( /\S{7}\S*/ ) ) {
                this.err = "password too short";
            }
            else if( this.pw && this.pw2) {
                this.ready = true;
            }
        },
        reset_password() {
            if( this.ready )
                this.spuc.reset_password( this.pw, this.spuc.token )
                .then( e => {
                    this.complete = true;
                    this.err = '';
                } )
                .catch( e => this.err = e );
        },
    },
    template : `<div class="col">
                  <h2>Recover Account</h2>
                  <div>
                    <h3>Reset Password</h3>
                    <div v-if="complete" class="col">
                       Your password is reset and you are logged in. <a href="#comics">Look at some comics</a>.
                    </div>
                    <div v-else class="col">
                       <div v-if="err">
                          <err v-bind:thing="this"/>
                          <a href="#comics">return to site</a>
                       </div>
                       <input id="pw" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password (at least 7 characters)"
                              v-bind:class="{ err : this.err.match(/password/) }"
                              v-on:input="check_passwords('pw',$event.target.value)"
                              v-on:change="reset_password()" />

                       <input id="pw2" type="password" required="1" pattern="\\S{7}\\S*"
                              placeholder="password repeat"
                              v-bind:class="{ err : this.err.match(/password/) }"
                              v-on:input="check_passwords('pw2',$event.target.value)"
                              v-on:change="reset_password()" />
                       <button v-bind:disabled="ready?null:'true'" v-
                               on:click="reset_password()" type="button">Reset</button>
                  </div>
                </div>
              </div>`,
} ); //recover

Vue.component('reset-request', {
    data : function() { return { spuc, ready : false, email : '', msg : '' } },
    methods : {
        checklink : function(txt) {
            this.email = txt.trim();
            this.ready = txt.match( /^\S+\@\S+\.\S+$/ ) ? true : false;
        },
        send_reset : function() {
            spuc.send_reset_request( this.email ).then( msg => {
                this.msg = 'sent';
            } );
        }
    },
    template : `<div class="col">
                    <h2>Request Password Reset Link</h2>
                    <div v-if="msg" class="row">
                      Request reset sent. Be sure to check your spam folder.
                      <a href="#comics">back to SPUC</a>
                    </div>
                    <div v-else class="row">
                        <input type="text"
                               v-on:input="checklink($event.target.value)"
                               v-on:change="send_reset()"
                               placeholder="email">
                        <button v-bind:disabled="!ready" v-on:click="send_reset()"
                                type="button">
                            send request
                        </button>
                    </div>
                </div>`,
} ); //reset-request

Vue.component('signup', {
    data : function() { return { spuc,
                                 ready : false,
                                 err : '',
                                 login : '',
                                 email : '',
                                 password : '',
                                 p2 : '' }
                      },
    methods : {
        set_signup : function( k, v ) {
            this[k] = v;
            this.check_signup();
        },
        check_signup : function() {
            this.ready = false;
            this.err = '';
            if( this.password != this.p2 && (this.password && this.p2) ) {
                this.err = "passwords don't match";
            }
            else if( this.email && ! this.email.match( /^[^@]+@[a-zA-Z]+\.[a-zA-Z.]*[a-zA-Z]+$/ ) ) {
                this.err = "invalid looking email";
            }
            else if( this.password && ! this.password.match( /\S{7}\S*/ ) ) {
                this.err = "password too short";
            }
            else if( this.login && ! this.login.match( /\S/ ) ) {
                this.err = "login too short";
            }
            else if( this.login || this.email ) {
                spuc.act('check_login_and_email',{ login: this.login,
                                                   email: this.email } )
                    .then( res => this.ready = true )
                    .catch( msg => this.err = msg );
            } else {
                this.err = 'no err';
            }
        },
        signup() {
            return spuc.signup( this.email, this.login, this.password )
                .then( msg => {
                    spuc.msg = "Created and logged into account.";
                    window.location.hash = '#setup';
                } )
                .catch( e => this.err = e );
        },
    },
    template : `<div id="signup" class="row squish">
                  <div class="col">
                   <h2>Make an Artist Account</h2>
                   <err v-bind:thing="this"/>
                   <div class="col">
                    <d-input type="email" required="1"
                           pattern="^[^@]+@[a-zA-Z]+\\.[a-zA-Z.]*[a-zA-Z]+$"
                           placeholder="email"
                           v-bind:class="{ err : this.err.match(/email/) }"
                           v-on:change="set_signup('email',$event.target.value)" />

                    <d-input type="text" required="1"  placeholder="username"
                           v-bind:class="{ err : this.err.match(/login/) }"
                           v-on:change="set_signup('login',$event.target.value)" />

                    <d-input id="pw" type="password" required="1" pattern="\\S{7}\\S*"
                           placeholder="password (at least 7 characters)"
                           v-bind:class="{ err : this.err.match(/password/) }"
                           v-on:change="set_signup('password',$event.target.value)" />

                    <d-input id="pw2" type="password" required="1" pattern="\\S{7}\\S*"
                           placeholder="password repeat"
                           v-bind:class="{ err : this.err.match(/password/) }"
                           v-on:change="set_signup('p2',$event.target.value)" />
                  </div>
                    <button type="button"
                            v-on:click="signup"
                            v-bind:class="this.ready?'valid' : ''"
                            v-bind:disabled="this.ready?null:'true'">Sign Up</button>
                    <a href="#comics">cancel</a>
                 </div>
                </div>`,
} ); //signup

// --------------------- TOP LEVEL UI

new Vue({
    el : '#main',
    data : function() { return { gator } },
    template : `<div>
                   <err v-bind:thing="gator"/>
                   <msg v-bind:thing="gator"/>
                   <artist v-if="gator.showing==='artist'"></artist>
                   <signup v-else-if="gator.showing==='signup'"></signup>
                   <artists v-else-if="gator.showing==='artists'"></artists>
                   <reset-request v-else-if="gator.showing==='reset-request'"></reset-request>
                   <recover v-else-if="gator.showing==='recover'"></recover>
                   <recover-err v-else-if="gator.showing==='recover-err'"></recover-err>
                   <profile v-else-if="gator.showing==='profile'"></profile>
                   <comics v-else></comics>
                </div>
                `,
});

new Vue({
    el : '#header',
    data : function() { return { gator } },
    template : `<header class="col">
                  <div class="row spread">
                    <a class="col grow splash" href="#comics">
                     <p>Scarf Poutine You Clone</p>
                     <p>Collaborative Commics</p>
                    </a>
                    <logged-in v-if="gator.login"></logged-in>
                    <login v-else-if="! gator.sess_id && gator.showing != 'signup'"></login>
                  </div>
                  <div class="row">
                    <div class="navbut" v-for="n in gator.context.nav_list">
                      <div v-if="typeof n[1] === 'function'" v-on:click="n[1]()">{{ n[0] }}</div>
                      <a v-else-if="typeof n[1] === 'string'"
                         v-bind:href="'#'+n[1]"
                         >{{ n[0] }}</a>
                      <a v-else
                         v-bind:href="'#'+n[0]"
                         >{{ n[0] }}</a>
                    </div>
                  </div>
                </header>
               `,
});
