

alert('hi');
/*
const yote_root = '/yote/js/';

import { loadYote } from '/js/yote.js';
import { Paginator } from '/js/tools.js';
import { Component } from '/js/backdraft.js';

const hashToParams = () => {
    let hash = {};
    if (window.location.hash.length > 1) {
        window.location.hash
            .substring(1)
            .split( /\&/ )
            .forEach( pair => {
                let [key,val] = pair.split( /=/, 2 );
                if (key in hash) {
                    hash[key] = [ hash[key], val ];
                } else {
                    hash[key] = val;
                }
            } );
    }
    return hash;    
} //hashToParams


class ParamManager {
    constructor(defaults) {
        this.current  = {...defaults};
        this.defaults = {...defaults};
        this.fixed    = {...defaults};
        this.updated  = {
            isUpdated : false,
            param     : {},
            count     : 0,
        };
    }
    fix() {
        Object.keys(this.fixed)
            .filter( k => ! ( k in this.current ) )
            .forEach( k => delete this.fixed[k] );
        Object.keys(this.current)
            .forEach( k => this.fixed[k] = this.current[k] );
        Object.keys( this.updated.param ).forEach( k => delete this.updated.param[k] );
        this.updated.count = 0;
        this.updated.isUpdated = false;
    }
    updateFromHash() {
        const hash = hashToParams();
        this.updated.isUpdated = false;
        this.updated.count = 0;
        Object.keys( hash )
            .filter( k => ! (k in this.fixed) )
            .forEach( newkey => {
                this.current[ newkey ] = hash[ newkey ];
                this.updated.param[ newkey ] = true;
                this.updated.count++;
            } );
        Object.keys( this.current )
            .filter( k => hash[k] !== this.current[ k ] )
            .forEach( changedKey => {
                this.current[ changedKey ] = hash[ changedKey ];
                this.updated.param[ changedKey ] = true;
                this.updated.count++;
            } );

        this.updated.isUpdated = this.updated.count > 0;
        return this.updated.isUpdated ? this.updated.param : false;
    } //updateFromHash
    updateToHash() {
        this.fix();
        window.location.hash = Object.keys( this.current )
            .filter( k => this.current[k] !== undefined )
            .map( k => `${k}=${escape(this.current[k])}` )
            .join( '&' );
    } //updateToHash
    update( key, val ) {
        this.current[key] = val;
    }
    remove( key ) {
        delete this.current[key];
    }
    set( updates ) {
        Object.keys( updates )
            .forEach( k => this.update( k, updates[k] ) );
        this.fix();
        this.updateToHash();
    }
    makeHashString( updates, removals ) {
        const remove = {};
        removals && removals.forEach( rem => remove[rem] = true );
        let items = {...this.current};
        updates && Object.keys( updates )
            .forEach( k => items[k] = updates[k] );
        return '#' + Object.keys( items )
            .filter( k => ! remove[k] )
            .filter( k => items[k] !== undefined )
            .map( k => `${k}=${escape(items[k])}` )
            .join( '&' );
    }
    
} //ParamManager

const $ = (sel,from) => (from||document).querySelector(sel);
const $$ = (sel,from) => (from||document).querySelectorAll(sel);


let spuc;
const paramManager = new ParamManager( {
    mode     : 'comics',
    position : '0',
} );

paramManager.updateFromHash();

const comics_paginator = new Paginator( {
    fetch_chunk_size : 50,
    max_cached       : 2000,
    window_size      : 5,
    window_start     : paramManager.current.comic_position,
    to_fill          : (start, chunk_size) => {
        const args = { start, amount : chunk_size, sort : paramManager.current.sort };
        if ( paramManager.current.mode === 'artists' && paramManager.current.artist_detail !== undefined ) {
            args.artist = GLOBAL.artists[ parseInt(paramManager.current.artist_position) ];
            args.artist_position = paramManager.current.artist_position;
        }
        return spuc.paginate_finished_comics( args );
    }
} );

const artists_paginator = new Paginator( {
    fetch_chunk_size : 50,
    max_cached       : 2000,
    window_size      : 9,
    window_start     : paramManager.current.artist_position,
    to_fill          : (start, chunk_size) => spuc.paginate_artists( { start, amount : chunk_size } ),
} );

const GLOBAL = { paramManager,
                 comics_paginator,
                 artists_paginator,
                 comics  : [],
                 artists : [],
               };

console.warn( 'set up js paths for spuc and yote' );
console.warn( 'set up js pagination' );

Vue.component('artist', {
    props : [ 'artist', 'idx' ],
    data : function() { return { GLOBAL, PM : GLOBAL.paramManager } },
    template : `<div class="row shrink artist">
                   <a v-bind:href="PM.makeHashString({ artist_idx : idx, artist_detail : 1 })">
                     <img v-bind:src="artist.get_avatar().get_url_path()">
                   </a>
                   <div class="col">
                     <div class="bold large">{{ artist.get_name() }} </div>
                     <div>{{ artist.get_bio() }} </div>
                   </div>
                </div>`,
} ); //artist

Vue.component('artists', {
    data : function() { return { GLOBAL, paginator : GLOBAL.artists_paginator, PM : GLOBAL.paramManager } },
    template : `<div class="artists col"
                 v-bind:data-last_updated="paginator.meta.last_updated">
                  <div class="row">
                    <a v-if="paginator.has_prev()"
                       v-bind:href="PM.makeHashString( { artist_position : paginator.prev_idx() } )">&lt;</a>
                    <span v-else>&lt;</span>
                    <a v-if="paginator.has_next()"
                       v-bind:href="PM.makeHashString( { artist_position : paginator.next_idx() } )">&gt;</a>
                    <span v-else>&gt;</span>
                  </div>
                  <div v-if="PM.current.artist_detail">
                    OTTY
                    <artist v-bind:artist="GLOBAL.artists[0]"
                            v-bind:idx="paginator.curr_idx()"
                            v-bind:data-last_updated="paginator.meta.last_updated">
                    </artist>
                    <comics></comics>
                  </div>
                  <div class="row wrap" v-else>
                      <artist v-for="(artist,idx) in GLOBAL.artists"
                            v-bind:idx="paginator.curr_idx() + idx"
                            v-bind:data-last_updated="paginator.meta.last_updated"
                        v-bind:artist="artist"></artist>
                  </div>
                </div>`,
} ); //artists


Vue.component('thumbcomic', {
    props    : [ 'comic', 'idx' ],
    data     : function() { return { GLOBAL, PM : GLOBAL.paramManager } },
    template : `<a class="comic thumb no-decoration" 
                   v-bind:href="PM.makeHashString( { comic_detail : '1', comic_position : idx } )">
                   <div class="row">🔍</div>
                   <div v-for="panel in comic.get_panels()" class="col panel">
                   <div v-if="panel.get_type() === 'caption'" class="caption">
                      {{ panel.get_caption() }}
                   </div>
                   <img v-else v-bind:src="panel.get_picture().get_url_path()" class="picture">
                  </div>
                </a>`,
} ); //thumbcomic

Vue.component('detailcomic', {
    props    : [ 'comic', 'idx' ],
    data     : function() { return { GLOBAL, PM : GLOBAL.paramManager } },
    template : `<div class="comic detail col">
                     <div v-for="panel in comic.get_panels()" class="row">
                       <div class="panel">
                         <a v-bind:href="PM.makeHashString( { mode : 'artists', artist_handle : panel.get_artist_handle().get_name(), artist_detail : 1 }, [ 'artist_position' ] )">
                           <img class="tiny-avatar" v-bind:src="panel.get_artist_handle().get_avatar().get_url_path()">
                         </a>
                         <div class="row squish">
                           <span>❤</span>
                           <span>{{ panel.get_kudos() }}</span>
                         </div>
                       </div>
                       <div v-if="panel.get_type() === 'caption'" class="caption grow panel">
                         {{ panel.get_caption() }}
                       </div>
                       <img v-else v-bind:src="panel.get_picture().get_url_path()" class="picture panel">
                  </div>
                </div>`,
} ); //detailcomic


Vue.component('comics', {
    data : function() { return { GLOBAL, paginator : GLOBAL.comics_paginator, PM : GLOBAL.paramManager } },
    template : `<div class="comics col center"
                 v-bind:data-last_updated="paginator.meta.last_updated">
                  <div class="row spread">
                    <span class="x-large bold">Comics</span>
                    <span v-if="! PM.current.sort || PM.current.sort==='recent'">
                      Showing most recent
                    </span>
                    <a v-else v-bind:href="PM.makeHashString( { sort : 'recent', position : '0' } )">most recent</a>

                    <span v-if="PM.current.sort==='commented'">
                      Showing most commented
                    </span>
                    <a v-else v-bind:href="PM.makeHashString( { sort : 'commented', position : '0' } )">most commented</a>

                    <span v-if="PM.current.sort==='kudoed'">
                      Showing most kudoed
                    </span>
                    <a v-else v-bind:href="PM.makeHashString( { sort : 'kudoed', position : '0' } )">most kudoed</a>

                    <span v-if="PM.current.sort==='rated'">
                      Showing highest rated
                    </span>
                    <a v-else v-bind:href="PM.makeHashString( { sort : 'rated', position : '0' } )">highest rated</a>
                    
                  </div>
                  <div class="row squish">
                    <div class="row box">
                      <a v-if="paginator.has_prev()"
                         class="x-large"
                         v-bind:href="PM.makeHashString( { comic_position : paginator.prev_idx() } )">&lt; prev</a>
                      <span v-else class="disabled x-large">&lt; prev</span>
                      <div v-if="PM.current.comic_detail==='1'" class="row squish x-large">
                        <a v-bind:href="PM.makeHashString( { comic_detail : undefined } )">back</a>
                      </div>
                      <a v-if="paginator.has_next()"
                         class="x-large"
                         v-bind:href="PM.makeHashString( { comic_position : paginator.next_idx() } )">next &gt;</a>
                      <span v-else class="disabled x-large">next &gt;</span>
                    </div>
                  </div>
                  <div v-if="PM.current.comic_detail==='1'" class="row squish">
                    <detailcomic v-for="(comic,idx) in GLOBAL.comics"
                           v-bind:data-last_updated="paginator.meta.last_updated"
                           v-bind:idx="idx + paginator.curr_idx()"
                           v-bind:comic="comic"></detailcomic>
                  </div>
                  <div class="row squish" v-else>
                    <thumbcomic v-for="(comic,idx) in GLOBAL.comics"
                           v-bind:data-last_updated="paginator.meta.last_updated"
                           v-bind:idx="idx + paginator.curr_idx()"
                           v-bind:comic="comic"></thumbcomic>
                  </div>
                </div>`,
} ); //comics

Vue.component('spucheader', {
    data : function() { return { GLOBAL } },
    template : `<header class="row">
                  <div class="grow xxx-large">masthead</div>
                  <div>Login</div>
                </header>`
} );

Vue.component('spucheader', {
    data : function() { return { GLOBAL } },
    template : `<header class="row">
                  <div class="grow xxx-large">masthead</div>
                  <div>Login</div>
                </header>`,
} );

Vue.component('options', {
    data : function() { return { GLOBAL, PM : GLOBAL.paramManager } },
    template : `<div class="row squish options">
                   <div v-for="(title,idx) in ['comics','artists']">
                     <span class="showing option selected" v-if="PM.current.mode === title || (!PM.current.mode&&idx===0)">{{ title }}</span>
                     <a v-else class="option" v-bind:href="PM.makeHashString({mode:title,comic_detail:undefined})" >{{ title }}</a>
                   </div>
                </div>`,
} );

                  // <span class="showing" v-if="PM.current.mode === 'comics' || (!PM.current.mode)">Comics</span>
                  // <a v-else v-bind:href="PM.makeHashString({mode:'comics',comic_detail:undefined})" >Comics</a>
                  // <span class="showing" v-if="PM.current.mode === 'artists'">Artists</span>
                  // <a v-else v-bind:href="PM.makeHashString({mode:'artists',comic_detail:undefined})" >Artists</a>

Vue.component('mainspuc', {
    data : function() { return { GLOBAL } },
    template : `<div>
                  <artists v-if="GLOBAL.paramManager.current.mode=='artists'"></artists>
                  <comics v-else></comics>
                </div>`,
} );

const vm = new Vue({
    el : '#spuc',
    data : function() { return { GLOBAL } },
    template : `<div>
                  <spucheader></spucheader>
                  <hr>
                  <options></options>
                  <hr>
                  <mainspuc></mainspuc>
                </div>`,
});

async function update() {
    let PM = GLOBAL.paramManager,
        apag = GLOBAL.artists_paginator,
        cpag = GLOBAL.comics_paginator;
    const updates = PM.updateFromHash();
    if( ! updates ) {
        PM.current.comic_position = 0;
        PM.current.artist_position = 0;
    }
    
    apag.set_position( PM.current.artist_position || 0 )
        .then( artists => GLOBAL.artists = artists );

    // ARTIST DETAIL VS NOT
    
    if (PM.current.artist_detail==='1') {
        apag.set_window_size(1);
        if (PM.current.artist_handle) {
            try {
                GLOBAL.artist = await spuc.lookup_artist(PM.current.artist_handle);
            } catch( err ) {
                alert(err);
            }
        } else {
            GLOBAL.artist = GLOBAL.artists[0];
        }
    } else {
        apag.set_window_size(9);
    }

    // COMICS DETAIL VS NOT
    if (PM.current.comic_detail==='1') {
        cpag.set_window_size(1);
    } else {
        cpag.set_window_size(4);
    }

    //        console.log( `set pos ${PM.current.comic_position}` );

    // IF SORT IS UPDATED, NEED TO RESTART THE COMIC PAGINATOR
    if (updates.sort || updates.artist_position) {
        cpag.start( PM.current.comic_position || 0 )
            .then( comics => GLOBAL.comics = comics );
    } else {
        cpag.set_position( PM.current.comic_position || 0 )
            .then( comics => GLOBAL.comics = comics );
    }

    PM.fix();
}

loadYote().then( yote => {
    spuc = yote.apps.spuc;
    window.addEventListener( 'hashchange', ev => update() );
    update();
} );
*/

