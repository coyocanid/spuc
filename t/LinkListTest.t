#!/usr/bin/perl

use strict;
use warnings;

use Carp;
use Data::Dumper;
use File::Temp qw/ :mktemp tempdir /;
use Test::More;
$SIG{ __DIE__ } = sub { Carp::confess( @_ ) };

BEGIN {
    use_ok( "SPUC::LinkedListNode" ) 
        || BAIL_OUT( "Unable to load 'SPUC::LinkedListNode'" );
}

my $dir = tempdir( CLEANUP => 1 );


my $store = Data::ObjectStore::open_store( $dir );

sub newnode {
    my( $rank, $pretty ) = @_;
    $store->create_container( 'SPUC::LinkedListNode', {
        item => $store->create_container( {
            ranking => $rank,
            pretty  => $pretty,
                                           } ),
                                    } );
}


my $head = newnode( 12, 42 );
my $first = $head;
# [ 12, ]
is( $first->getnext, undef, "no next yet" );
is( $first->getprev, undef, "no prev yet" );

$head = $head->insert_next( newnode( 13, 24 ) );
my $second = $head;
# [ 12, 13 ]

is( $first->getnext->get_item->get_ranking, 13, "has a next" );
is( $first->getprev, undef, "no prev yet" );


is( $head->getnext, undef, "inserted no next yet" );
is( $head->getprev->get_item->get_ranking, 12, 'inserted as first as prev' );


# insert_prev( $node, $list )
$head = $head->insert_prev( newnode( 14, 42 ) );
is( $head->get_item->get_ranking, 14, 'insert_prev' );
# [ 12, 14, 13 ]

is( $first->getnext->get_item->get_ranking, 14, 'insert_prev order' );
my $third = $head;

# head( $list )
is( $head->head->get_ranking, $first->get_ranking, "head gets correct item" );
is( $first->head->get_ranking, $first->get_ranking, "head gets correct item" );
is( $second->head->get_ranking, $first->get_ranking, "head gets correct item" );
is( $third->head->get_ranking, $first->get_ranking, "head gets correct item" );

# tolist( $list )
my $list = $head->tolist;
is( @$list, 3, "3 entries in list" );
is( $list->[0]->get_ranking, 12, "first entry" );
is( $list->[1]->get_ranking, 14, "second entry" );
is( $list->[2]->get_ranking, 13, "third entry" );


# next( $count, $list )

# find_prev

# nth

# sortinto

done_testing;
