#!/usr/bin/perl

use strict;
use warnings;

# /apps/SPUC/Public/_all_comics
# /apps/SPUC/Public/_artists (a list)

# artist
#   display_name
#   _playing

# comic
#   _player
#  
#


use Data::ObjectStore;

my $os = Data::ObjectStore->open_store( "/var/www/data/spuc" );
my $root = $os->load_root_container;
my $spuc = $root->get_apps->{SPUC}{Public};

my $comics = $spuc->get__all_comics;
my $players = $spuc->get__artists;

check();
exit;

sub check {

    for my $comic (@$comics) {
	my $panels = $comic->get_panels();
	if( $comic->get_finished && @$panels < 9 ) {
	    print STDERR "$comic, panels : ".scalar(@$panels)." finished ? ".($comic->get_finished ? "YES" : "NO" )."\n";
	    print STDERR "    MARKED FINISHED BUT IS NOT\n";
	}
	if (@$panels > 8 && ! $comic->get_finished) {
	    print STDERR "$comic, panels : ".scalar(@$panels)." finished ? ".($comic->get_finished ? "YES" : "NO" )."\n";
	    print STDERR "    NOT MARKED FINISHED BUT IS\n";
	}
	my $player = $comic->get__player;
	if ($player) {
	    my $playing = $player->get__playing;
	    if( $player != $playing ) {
		print STDERR "$comic marked as being played by ".$player->get_display_name." but isn't. They are playing comic $playing\n";
		$comic->set__player(undef);
	    }
	}
	my $path = $spuc->get__image_path."/comics";
	if( $comic->get_finished && @$panels == 9 ) {
	    my $dir = "$path/$comic";
	    if ( -e "$dir/finished.png" ) {

	    } else {
		$comic->make_comic_image;
	    }
	}
    }

    print STDERR "\n\nCHECK ARTISTS\n";
    
    for my $player (@$players) {
	my $playing = $player->get__playing;
	if ( $playing ) {
	    my $played_by = $playing->get__player;
	    if ( $player != $played_by) {
		print STDERR $player->get_display_name. " playing $playing but that is marked as being played by ".($played_by ? $played_by->get_display_name : ' noone ' )."\n";
		$player->set__playing(undef);
	    }
	}
    }
    $os->save;

}
