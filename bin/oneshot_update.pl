#!/usr/bin/perl

print "oneshot_update.pl\n";

__END__
use strict;
use warnings;
no warnings 'numeric';
no warnings 'uninitialized';
use Data::ObjectStore;

use Data::Dumper;
use Image::Magick;

use lib '/home/coyo/proj/spuc.new/lib';

my $store = Data::ObjectStore->open_store( "/var/www/data/spuc.vue" );
#$store->quick_purge; exit;
my $root = $store->load_root_container;
my $OLD = $root->get_SPUC;

my $artists = [values %{$OLD->get__users}];

my $NEW = $store->create_container( 'App::SPUC::Public',
                                    { _login    => {},
                                      _email    => {},
                                      _session  => {},
                                      _site     => $OLD->get__site,
                                      _app_path => '/spuc',
                                      _file_path => '/var/www',
                                      _image_path => '/var/www/html/spuc.vue/images',
                                    } );
$NEW->set__default_avatar( newpic($OLD->get__default_avatar) );
my $new_login = $NEW->get__login;
my $new_email = $NEW->get__email;
$root->set_apps( { SPUC => { Public => $NEW } } );


for my $artist (@$artists) {
    my $NART = $store->create_container( 'App::SPUC::Part::Artist' );
    $NEW->add_to__artists( $NART );
    for my $fld (qw( 
                 _active_time 
                 _created 
                 _email 
                 _enc_pw 
                 _login_name 
                 _login_time 
                 _reset_token_good_until
                 bio
                 display_name
                 ) ) {
        $NART->set( $fld, $artist->get( $fld ) );
    }
    $new_login->{lc($NART->get__login_name)} = $NART;
    $new_email->{lc($NART->get__email)} = $NART;
} #each artist

my( %OLDID2COMIC, %OLDID2PANEL, %OLDID2PIC );
sub transart {
    my( $OLDA ) = @_;
    return undef unless $OLDA;
    return $new_email->{lc($OLDA->get__email)};
}
sub transcomic {
    my( $OLDC ) = @_;
    return $OLDID2COMIC{ $OLDC };
}
sub transpan {
    my( $OLDP ) = @_;
    return $OLDID2PANEL{ $OLDP };
}
sub newpic {
    my( $OLDPIC ) = @_;
    return undef unless $OLDPIC;
    my $newp = $OLDID2PIC{$OLDPIC};
    unless( $newp ) {
        $newp = $store->create_container( 'App::SPUC::Part::Picture',
                                          {
                                              map { $_ => $OLDPIC->get($_) } qw( extension _original_name _origin_file )
                                          });
        my $path = $newp->get__origin_file;
        $path =~ s!/var/www/html!!;
        $newp->set_path( $path );
        $OLDID2PIC{$OLDPIC} = $newp;
    }
    return $newp;
}

my $chats = $OLD->get__chat;
for my $chat (reverse @$chats) {
    $NEW->add_to__chat( $store->create_container( {
        artist  => transart($chat->get_artist),
        comment => $chat->get_comment,
        time    => $chat->get_time,
                                                  } ) );
}


# now new_email can map to the new artist
my $all_comics = $OLD->get__all_comics;
for my $comic (@$all_comics) {
    my $NEWC = $store->create_container( 'App::SPUC::Part::Comic' );
    $NEWC->set_rating(0);
    $NEWC->set__ratings( {} ); # artist -> rating
    $NEWC->set__app( $NEW );
    $NEW->add_to__all_comics( $NEWC );

    # general fields
    $OLDID2COMIC{$comic} = $NEWC;
    for my $fld (qw( last_updated _hold_expires started needs )) {
        $NEWC->set( $fld, $comic->get( $fld ) );
    }

    # artists in comic
    $NEWC->set__creator( transart($comic->get_creator) );
    $NEWC->set__player( transart($comic->get__player) );
    my @oldarts = values %{$comic->get_artists};
    $NEWC->set__artists( { map { my $na = transart($_); $na => $na } @oldarts } );

    # comments
    my $comments = $comic->get_comments;
    $NEWC->set_comments( [ map { $store->create_container({time    => $_->get_time,
                                                           artist  => transart($_->get_artist),
                                                           comment => $_->get_comment}) } @$comments ] );
    $NEWC->set_comment_count( scalar(@$comments) );
    
    # panels
    my $panels = $comic->get_panels;

    my( $image, $img_file, $montage_pairs );
    my $is_finished = @$panels == $comic->get_needs;

    if( $is_finished ) {
        $NEWC->set_is_finished;
        $NEWC->make_comic_image;
    }
    
    my $kcount = 0;
    for my $panel (@$panels) {
        my $NEWP = $store->create_container( 'App::SPUC::Part::Panel' );
        $NEWC->add_to_panels( $NEWP );
        $OLDID2PANEL{$panel} = $NEWP;
        for my $fld (qw( created kudos type )) {
            $NEWP->set( $fld, $panel->get( $fld ) );
        }
        $kcount += $NEWP->get_kudos;

        $NEWP->set_artist( transart( $panel->get_artist ) );
        $NEWP->set__comic( $NEWC );
        if( $panel->get_type eq 'picture' ) {
            my $oldpic = $panel->get_picture;
            $NEWP->set_picture( newpic( $oldpic ) );
            $img_file = $NEWP->get_picture->get__origin_file;
        } else {
            $NEWP->set_caption( $panel->get_caption );
            push @$montage_pairs, [ $NEWP->get_caption, $img_file ];
            
        }
    } #each panel

    if( $is_finished ) {
        $NEWC->set_finished( 1 );
    }
    
    $NEWC->set_kudo_count( $kcount );
    
} #each comic

# dip again into the artists to fill out the comic links they have
for my $artist (@$artists) {
    
    my $NEWA = transart( $artist );
    $NEWA->set__playing( transcomic( $artist->get__playing ) );
    $NEWA->set__saved_comic( transcomic( $artist->get__saved_comic ) );
    $NEWA->set__saved_pic( newpic( $artist->get__saved_panel ) );
    $NEWA->set__unfinished_comics( [ map { transcomic($_) } @{$artist->get__unfinished_comics([])} ] );
    $NEWA->set__all_comics( [ map { transcomic($_) } @{$artist->get__all_comics([])} ] );
    $NEWA->set__finished_comics( [ map { transcomic($_) } @{$artist->get_finished_comics([])} ] );
    $NEWA->set__avatars( [ map { newpic($_) } @{$artist->get__avatars([])} ] );
    $NEWA->set_avatar( newpic( $artist->get_avatar ) );
    $NEWA->set__updates( [ map { { type => $_->{type}, msg => $_->{msg}, comic => transcomic($_->{comic}) } } @{$artist->get__updates([])}] );
    my $kuds = $NEWA->set__kudos( { map { my $pan = transpan($_); $pan => $pan } values %{$artist->get__kudos({})} } );
    for my $panel (values %$kuds) {
        $panel->get__kudo_givers({})->{$NEWA} = $NEWA;
    }
    my $bookd = $NEWA->set__bookmarks( [ map { transcomic($_) } @{$artist->get__bookmarks||[]} ] );
    $NEWA->set__bookmark_hash( { map { $_ => $_ } @$bookd } );
    $NEWA->set__ratings( {} );
}

$NEW->set__finished_comics( [ map { transcomic( $_ ) } @{$OLD->get_finished_comics} ] );
$NEW->set__unfinished_comics( [ map { transcomic( $_ ) } @{$OLD->get__unfinished_comics} ] );

$store->save;

