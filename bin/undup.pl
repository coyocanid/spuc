#!/usr/bin/perl

use strict;
use warnings;

use Data::ObjectStore;

my $os = Data::ObjectStore->open_store( "/var/www/data/spuc" );
my $root = $os->load_root_container;
my $spuc = $root->get_apps->{SPUC}{Public};

my $comics = $spuc->get__all_comics;

for my $comic (@$comics) {
    print "CHECKING $comic ".$comic->get_panels->[0]->get_caption."\n";
    print STDERR Data::Dumper->Dump([$comic->[1],"COMIC"]);
    my( @matches ) = grep { "$_" eq "$comic" } @$comics;
    if ( @matches > 1 ) {
	print " ***  HAS DUPs \n";
    }
    my $pans = $comic->get_panels;
    if ("$comic" eq 23166 ) {
	print STDERR "Found\n";
	print STDERR Data::Dumper->Dump([$pans->[$#$pans]->[1],"XX"]);
	if ("$pans->[$#$pans]" == "$pans->[$#$pans-1]") {
	    print STDERR "Dup panel found\n";
	}
	exit;
    }
}

exit;
